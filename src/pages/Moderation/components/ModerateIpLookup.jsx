import React from 'react';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import NavbarResponsive from './NavbarResponsive';
import { Panels } from './IpLookup/style';
import IpLookupPanel from './IpLookup/IpLookupPanel';
import UsernameLookupPanel from './IpLookup/UsernameLookupPanel';
import { navItems } from './NavItems';
import { PageWrapper } from './style';

const ModerateIpLookup = () => (
  <UserGroupRestricted userGroupIds={[3, 4, 5]}>
    <PageWrapper>
      <NavbarResponsive items={navItems} />
      <Panels>
        <IpLookupPanel />
        <UsernameLookupPanel />
      </Panels>
    </PageWrapper>
  </UserGroupRestricted>
);

export default ModerateIpLookup;
