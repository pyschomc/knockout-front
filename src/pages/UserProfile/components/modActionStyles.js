import styled from 'styled-components';
import { ThemeVerticalPadding, ThemeHorizontalPadding } from '../../../Theme';

export const BansContainer = styled.div`
  @media (max-width: 960px) {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    justify-content: space-evenly;
  }
`;
export const BasicInfoWrapper = styled.div``;
export const BanReason = styled.div``;
export const BanItem = styled.div`
  width: 100%;
  background: #dc4035;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  margin-bottom: 10px;
`;
export const BigInfo = styled.h1`
  margin-top: 0;
`;
