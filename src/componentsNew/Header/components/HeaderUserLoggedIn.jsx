import React, { useRef, useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { HashLink as Link } from 'react-router-hash-link';
import styled from 'styled-components';
import config from '../../../../config';
import {
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeTextColor,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import MentionsContainer from './MentionsContainer';
import { DesktopMediaQuery } from '../../../components/SharedStyles';

const UserAvatar = ({ src, alt }) => {
  if (!src || src === 'none.webp') {
    return null;
  }

  let url = `${config.cdnHost}/image/${src}`;
  if (!src || src.length === 0) {
    url = '/static/default-avatar.png';
  }
  return <img className="user-menu-avatar" src={url} alt={`${alt}'s Avatar`} />;
};

UserAvatar.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
};

const HeaderUserLoggedIn = (props) => {
  const { userId, avatarUrl, username, usergroup, unreadSubscriptions } = props;

  const [open, setOpen] = useState(false);

  const userMenuRef = useRef();
  const userButtonRef = useRef();

  const numberOfMentions = useSelector((state) => state.mentions.mentions.length || 0);

  const toggleOpen = () => {
    setOpen((value) => !value);
  };

  useEffect(() => {
    function handleClickOutside(event) {
      if (
        userMenuRef.current &&
        !userMenuRef.current.contains(event.target) &&
        !userButtonRef.current.contains(event.target)
      ) {
        toggleOpen();
      }
    }

    if (open) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [open, userMenuRef]);

  return (
    <UserMenuButton onClick={toggleOpen} ref={userButtonRef} className="user-menu-button" left>
      {numberOfMentions > 0 && <div className="link-notification left">{numberOfMentions}</div>}

      <UserRoleWrapper user={{ usergroup }}>
        <span className="user-menu-username">{username}</span>
      </UserRoleWrapper>
      <UserAvatar src={avatarUrl} alt={username} />

      {open && (
        <>
          <div className="user-menu-opened" ref={userMenuRef}>
            <Link to={`/user/${userId}`} onClick={toggleOpen} className="user-menu-link">
              <i className="fas fa-user">&nbsp;</i>
              Profile
            </Link>
            <Link to="/usersettings" onClick={toggleOpen} className="user-menu-link">
              <i className="fas fa-user-cog">&nbsp;</i>
              Settings
            </Link>
            <Link to="/alerts/list" onClick={toggleOpen} className="user-menu-link">
              <i className="fas fa-newspaper">&nbsp;</i>
              Subscriptions
              {unreadSubscriptions > 0 && (
                <span className="subscriptions">{unreadSubscriptions}</span>
              )}
            </Link>
            <Link to="/logout" onClick={toggleOpen} className="user-menu-link">
              <i className="fas fa-sign-out-alt">&nbsp;</i>
              Log out
            </Link>

            <MentionsContainer toggleOpen={toggleOpen} />
          </div>
        </>
      )}
    </UserMenuButton>
  );
};

HeaderUserLoggedIn.propTypes = {
  userId: PropTypes.number.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  usergroup: PropTypes.number.isRequired,
  unreadSubscriptions: PropTypes.number,
};

HeaderUserLoggedIn.defaultProps = {
  unreadSubscriptions: 0,
};

const UserMenuButton = styled.div`
  display: flex;
  height: 45px;
  padding: 0;
  align-items: center;
  justify-content: center;
  background: ${ThemeBackgroundDarker};
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  position: relative;
  cursor: pointer;
  z-index: 51;

  ${DesktopMediaQuery} {
    &:hover {
      filter: brightness(1.1);
    }
  }

  ${(props) =>
    props.left &&
    `
    right: unset;
    left: 0;
    transform: unset;
  `}

  span.user-menu-username {
    margin: 0 ${ThemeHorizontalPadding};
    line-height: 1.3;
    font-size: ${ThemeFontSizeMedium};
  }

  div.user-menu-opened {
    top: 45px;
    right: 0;
    z-index: 51;
    color: ${ThemeTextColor};

    width: 300px;
    max-height: 650px;
    position: absolute;

    margin-top: ${ThemeVerticalPadding};
    background: ${ThemeBackgroundLighter};
    border: none;
    border-radius: 5px;
    box-shadow: 0 6px 14px rgba(0, 0, 0, 0.33);

    display: flex;
    flex-direction: column;
    overflow: hidden;

    font-size: ${ThemeFontSizeMedium};

    @media (max-width: 900px) {
      width: calc(100vw - 45px);
      position: fixed;
      top: 70px;
      left: 50%;
      transform: translateX(-50%);
    }
  }

  img.user-menu-avatar {
    display: block;
    float: right;
    height: 47px;
    margin: 0;
    margin-right: -1px;
    background: rgba(0, 0, 0, 0.25);
  }

  a.user-menu-link {
    display: block;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    box-sizing: border-box;
    color: ${ThemeTextColor};

    i {
      min-width: 25px;
    }

    &:hover {
      background-color: ${ThemeBackgroundDarker};
    }
  }

  span.subscriptions {
    background: #ec3737;
    padding: 1px 4px;
    margin-left: 10px;
    height: 13px;
    border-radius: 7px;
    font-weight: bold;
    font-size: 10px;
    text-align: center;
    line-height: 13px;
    vertical-align: middle;
  }
`;

export default HeaderUserLoggedIn;
