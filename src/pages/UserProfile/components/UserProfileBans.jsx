/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';

import { BansContainer } from './modActionStyles';
import { makeBanInvalid } from '../../../services/moderation';
import { pushSmartNotification } from '../../../utils/notification';
import { usergroupCheck } from '../../../components/UserGroupRestricted';
import BanItem from './BanItem';

const setBanInvalid = async ({ banId, username, banStillValid }) => {
  if (!banStillValid) {
    alert('This ban doesnt seem to be active. Let the devs know and they will try to help.');
    return;
  }

  if (
    window.confirm(
      `Are you sure you want to invalidate ${username}'s ban by setting its year to 2001?`
    )
  ) {
    try {
      const banResponse = await makeBanInvalid(banId);

      pushSmartNotification(banResponse);
    } catch (error) {
      console.error(error);
    }
  }
};

const durationInDays = (a, b) => {
  const date1 = new Date(a);
  const date2 = new Date(b);
  const days = parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10);

  return `${days} days`;
};
const durationInHours = (a, b) => {
  const date1 = new Date(a);
  const date2 = new Date(b);

  const hours = parseInt((date2 - date1) / (1000 * 60 * 60), 10);

  if (hours > 24) {
    return durationInDays(a, b);
  }

  return `${hours} hours`;
};

const msToTime = msString => {
  const ms = msString % 1000;
  // eslint-disable-next-line no-param-reassign
  msString = (msString - ms) / 1000;
  const secs = msString % 60;
  // eslint-disable-next-line no-param-reassign
  msString = (msString - secs) / 60;
  const mins = msString % 60;
  const hrs = (msString - mins) / 60;

  return {
    hrs,
    mins,
    secs,
    ms
  };
};

const getBanIcon = (a, b) => {
  const date1 = new Date(a);
  const date2 = new Date(b);

  const dateInMs = date2 - date1;

  const durationObject = msToTime(dateInMs);

  if (durationObject.hrs > 10000) return 'static/icons/jail-door.png';
  if (durationObject.hrs > 72) return 'static/icons/handcuffs.png';
  if (durationObject.hrs >= 48) return 'static/icons/sparring.png';
  if (durationObject.hrs >= 24) return 'static/icons/nappy.png';
  if (durationObject.hrs === 12) return 'static/icons/12.png';
  if (durationObject.hrs < 12) return 'static/icons/light-off.png';
};

const UserProfileBans = ({ user, bans }) => {
  const isMod = usergroupCheck([3, 4, 5]);

  return (
    <BansContainer>
      {bans.map(ban => {
        const { id, banReason, thread, createdAt, expiresAt, bannedBy, post } = ban;
        const banStillValid = dayjs(expiresAt).isAfter(dayjs());

        return (
          <BanItem
            key={id}
            banReason={banReason}
            createdAt={createdAt}
            expiresAt={expiresAt}
            duration={durationInHours(createdAt, expiresAt)}
            bannedByName={bannedBy.username}
            thread={thread}
            post={post}
            threadTitle={thread.title}
            banStillValid={banStillValid}
            isMod={isMod}
            banIcon={getBanIcon(createdAt, expiresAt)}
            invalidateBan={() => {
              setBanInvalid({ banId: id, username: user.username, banStillValid });
            }}
          />
        );
      })}
    </BansContainer>
  );
};

UserProfileBans.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string.isRequired
  }).isRequired,
  bans: PropTypes.array.isRequired
};

export default UserProfileBans;
