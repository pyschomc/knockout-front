import React from 'react';
import { Link } from 'react-router-dom';

import PrivacyPolicy from './components/PrivacyPolicy';

const PrivacyPolicyPage = () => (
  <div>
    <div>
      See <Link to="/rules">this page</Link> for the rules.
    </div>
    <PrivacyPolicy />
  </div>
);

export default PrivacyPolicyPage;
