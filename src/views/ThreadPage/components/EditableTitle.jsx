import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import LoggedInOnly from '../../../components/LoggedInOnly';
import { usergroupCheck } from '../../../components/UserGroupRestricted';
import { updateThread } from '../../../services/threads';
import { pushSmartNotification, pushNotification } from '../../../utils/notification';
import Tooltip from '../../../components/Tooltip';
import ForumIcon from '../../../components/ForumIcon';
import { ThemeHugeTextSize } from '../../../Theme';
import {
  ThemeTextColor,
  ThemeHorizontalPadding,
  ThemeBackgroundDarker,
} from '../../../utils/ThemeNew';
import { SubHeaderButton } from '../../../componentsNew/Buttons';

const EditableTitle = ({ title, iconId, threadId, byCurrentUser }) => {
  const [currentTitle, setCurrentTitle] = useState('Thread');
  const [isEditing, setIsEditing] = useState(false);
  const [titleInput, setTitleInput] = useState('');

  const titleField = useRef();

  useEffect(() => {
    setCurrentTitle(title);
  }, [title]);

  useEffect(() => {
    if (isEditing && titleField.current) {
      titleField.current.focus();
    }
  }, [isEditing]);

  const edit = () => {
    setIsEditing((value) => !value);
    setTitleInput(currentTitle);
  };

  const save = async () => {
    try {
      if (titleInput.length > 140) {
        pushSmartNotification({ error: 'Title is too long. Must be 140 characters or less.' });
        throw new Error({ error: 'Title too long' });
      }

      await updateThread({ title: titleInput, id: threadId });

      pushNotification({ message: 'Thread title updated.' });
      setCurrentTitle(titleInput);
    } catch (err) {
      pushNotification({ message: 'An error occured.' });
    }
    setIsEditing(false);
  };

  return (
    <StyledEditableTitle>
      <ForumIcon iconId={iconId} />
      {isEditing ? (
        <input
          className="thread-page-title-input"
          title="Title edit input"
          ref={titleField}
          value={titleInput}
          onChange={(e) => setTitleInput(e.target.value)}
        />
      ) : (
        <span>{title ? currentTitle : 'Loading thread...'}</span>
      )}
      <LoggedInOnly>
        {(usergroupCheck([3, 4, 5]) || byCurrentUser) &&
          (isEditing ? (
            <span>
              <div className="thread-page-title-button">
                <Tooltip text="Save title" top={false}>
                  <SubHeaderButton title="Save title" onClick={() => save()}>
                    <i className="fas fa-check" />
                    <span>Save</span>
                  </SubHeaderButton>
                </Tooltip>
              </div>
              <div className="thread-page-title-button">
                <Tooltip text="Cancel" top={false}>
                  <SubHeaderButton title="Cancel" onClick={() => edit()}>
                    <i className="fas fa-times" />
                    <span>Cancel</span>
                  </SubHeaderButton>
                </Tooltip>
              </div>
            </span>
          ) : (
            <div className="thread-page-title-button">
              <Tooltip text="Edit title" top={false}>
                <SubHeaderButton
                  title="Edit title"
                  className="title-edit-button"
                  onClick={() => edit()}
                >
                  <i className="fas fa-pencil-alt" />
                  <span>Rename</span>
                </SubHeaderButton>
              </Tooltip>
            </div>
          ))}
      </LoggedInOnly>
    </StyledEditableTitle>
  );
};

EditableTitle.propTypes = {
  byCurrentUser: PropTypes.bool,
  threadId: PropTypes.number,
  title: PropTypes.string,
  iconId: PropTypes.number,
};
EditableTitle.defaultProps = {
  byCurrentUser: false,
  threadId: 0,
  iconId: 0,
  title: '',
};

const StyledEditableTitle = styled.div`
  display: flex;
  align-items: center;

  .thread-page-title-button {
    display: inline-block;
    font-weight: normal;
    margin-left: 9px;
  }

  .thread-page-title-input {
    padding: 1px ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    font-size: ${ThemeHugeTextSize};
    font-family: 'Open Sans', sans-serif;
    font-weight: 700;
    line-height: 1.1;
    overflow-wrap: break-word;
    vertical-align: middle;
    border: none;
    background: ${ThemeBackgroundDarker};
    flex-grow: 1;
  }
`;

export default EditableTitle;
