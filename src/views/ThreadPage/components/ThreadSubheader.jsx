import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import LoggedInOnly from '../../../components/LoggedInOnly';
import EditableBackground from './EditableBackground';
import Tooltip from '../../../components/Tooltip';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import { scrollToTop } from '../../../utils/pageScroll';
import TagButton from './TagButton';
import {
  ThemeSecondaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemeMediumTextSize,
} from '../../../Theme';
import {
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
} from '../../../utils/ThemeNew';
import { MobileMediaQuery, DesktopMediaQuery } from '../../../components/SharedStyles';
import { SubHeaderButton, SubheaderLink } from '../../../componentsNew/Buttons';
import Pagination from '../../../componentsNew/Pagination';

const ThreadSubheader = ({
  thread,
  params,
  currentPage,
  togglePinned,
  toggleDeleted,
  toggleLocked,
  currentUserId,
  showMoveModal,
  createAlert,
  deleteAlert,
}) => (
  <StyledThreadSubheader>
    <SubheaderLink to={`/subforum/${thread.subforumId}`} onClick={scrollToTop}>
      &#8249;
      <span>{thread.subforumName || 'Subforum'}</span>
    </SubheaderLink>
    <div className="subheader-buttons">
      <div className="subheader-pagination">
        <Pagination
          totalPosts={thread.totalPosts}
          pagePath={`/thread/${params.id}/`}
          currentPage={currentPage}
          showNext
        />
      </div>
      <LoggedInOnly>
        <div className="subheader-dropdown">
          <i className="subheader-dropdown-arrow fa fa-angle-down" />
          <ul className="subheader-dropdown-list">
            <UserGroupRestricted userGroupIds={[3, 4, 5]}>
              <li className="subheader-dropdown-list-item">
                <Tooltip text={thread.pinned ? 'Unpin Thread' : 'Pin Thread'}>
                  <SubHeaderButton status onClick={togglePinned} type="button">
                    {thread.pinned ? (
                      <i className="fas fa-sticky-note" />
                    ) : (
                      <i className="far fa-sticky-note" />
                    )}
                    <span>{thread.pinned ? 'Unpin Thread' : 'Pin Thread'}</span>
                  </SubHeaderButton>
                </Tooltip>
              </li>
              <li className="subheader-dropdown-list-item">
                <Tooltip text={thread.locked ? 'Unlock Thread' : 'Lock Thread'}>
                  <SubHeaderButton status onClick={toggleLocked} type="button">
                    {thread.locked ? (
                      <i className="fas fa-lock-open" />
                    ) : (
                      <i className="fas fa-lock" />
                    )}
                    <span>{thread.locked ? 'Unlock Thread' : 'Lock Thread'}</span>
                  </SubHeaderButton>
                </Tooltip>
              </li>
              <li className="subheader-dropdown-list-item">
                <Tooltip text={thread.deleted ? 'Restore Thread' : 'Delete Thread'}>
                  <SubHeaderButton status onClick={toggleDeleted} type="button">
                    {thread.deleted ? (
                      <i className="fas fa-trash-restore" />
                    ) : (
                      <i className="fas fa-trash" />
                    )}
                    <span>{thread.deleted ? 'Restore Thread' : 'Delete Thread'}</span>
                  </SubHeaderButton>
                </Tooltip>
              </li>
              <li className="subheader-dropdown-list-item">
                <Tooltip text="Move thread">
                  <SubHeaderButton status onClick={showMoveModal} type="button">
                    <i className="fas fa-people-carry" />
                    <span>Move thread</span>
                  </SubHeaderButton>
                </Tooltip>
              </li>
              <TagButton thread={thread} />
            </UserGroupRestricted>
            <EditableBackground
              backgroundUrl={thread.threadBackgroundUrl}
              byCurrentUser={currentUserId === thread.userId}
              threadId={thread.id}
            />
            <div>
              {thread.isSubscribedTo ? (
                <li className="subheader-dropdown-list-item">
                  <Tooltip text="Unsubscribe">
                    <SubHeaderButton onClick={deleteAlert}>
                      <i className="fas fa-eye-slash" />
                      <span>Unsubscribe</span>
                    </SubHeaderButton>
                  </Tooltip>
                </li>
              ) : (
                <li className="subheader-dropdown-list-item">
                  <Tooltip text="Subscribe">
                    <SubHeaderButton onClick={createAlert}>
                      <i className="fas fa-eye" />
                      <span>Subscribe</span>
                    </SubHeaderButton>
                  </Tooltip>
                </li>
              )}
            </div>
          </ul>
        </div>
      </LoggedInOnly>
    </div>
  </StyledThreadSubheader>
);

ThreadSubheader.propTypes = {
  thread: PropTypes.shape({
    id: PropTypes.number,
    subforumId: PropTypes.number,
    subforumName: PropTypes.string,
    totalPosts: PropTypes.number,
    pinned: PropTypes.bool,
    deleted: PropTypes.bool,
    locked: PropTypes.bool,
    threadBackgroundUrl: PropTypes.string,
    userId: PropTypes.number,
    isSubscribedTo: PropTypes.bool,
  }).isRequired,
  params: PropTypes.shape({
    id: PropTypes.number,
  }).isRequired,
  currentPage: PropTypes.number.isRequired,
  togglePinned: PropTypes.func.isRequired,
  toggleLocked: PropTypes.func.isRequired,
  toggleDeleted: PropTypes.func.isRequired,
  showMoveModal: PropTypes.func.isRequired,
  deleteAlert: PropTypes.func.isRequired,
  createAlert: PropTypes.func.isRequired,
  currentUserId: PropTypes.number.isRequired,
};

export const StyledThreadSubheader = styled.header`
  box-sizing: border-box;
  max-width: 100vw;
  padding-top: ${ThemeVerticalPadding};
  padding-bottom: calc(${ThemeVerticalPadding} / 2);
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  display: flex;
  flex-direction: row;

  .subheader-back {
    display: flex;
    height: 30px;
    border-radius: 5px;
    overflow: hidden;
    align-items: stretch;
    margin-right: ${ThemeHorizontalPadding};
    background: ${ThemeSecondaryBackgroundColor};
    text-decoration: none;
  }

  .subheader-back-link {
    display: block;
    margin: 0;
    font-size: 24px;
    height: 30px;
    padding: 0 ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    background: ${ThemeQuaternaryBackgroundColor};
    text-decoration: none;
    cursor: pointer;

    span {
      font-size: ${ThemeFontSizeSmall};
      white-space: nowrap;
      vertical-align: middle;
      padding-left: calc(${ThemeHorizontalPadding} / 2);
      ${MobileMediaQuery} {
        display: none;
      }
    }
  }

  .subheader-buttons {
    display: flex;
    flex-grow: 1;
  }

  .subheader-pagination {
    display: flex;
    justify-content: flex-end;
    flex-grow: 1;
    font-size: ${ThemeFontSizeSmall};
  }

  .subheader-dropdown {
    position: relative;
    width: 30px;
    height: 30px;
    border-radius: 5px;
    margin-left: ${ThemeHorizontalPadding};
    overflow: hidden;

    &:active,
    &:focus,
    &:hover {
      overflow: visible;
    }

    ${DesktopMediaQuery} {
      width: auto;
      margin-left: 0;
      overflow: visible !important;
    }
  }

  .subheader-dropdown-arrow {
    position: relative;
    display: block;
    width: 100%;
    height: 30px;
    line-height: 30px;
    text-align: center;
    font-size: ${ThemeMediumTextSize};
    color: ${ThemeTextColor};
    background: ${ThemeQuaternaryBackgroundColor};
    border-radius: 5px;
    cursor: pointer;
    z-index: 41;

    ${DesktopMediaQuery} {
      display: none;
    }
  }

  .subheader-dropdown-list {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    list-style: none;
    padding: 32px 0 0 0;
    background: ${ThemeQuaternaryBackgroundColor};
    box-shadow: 0 0 0 2000px rgba(0, 0, 0, 0.5);
    border-radius: 5px;
    z-index: 40;

    ${DesktopMediaQuery} {
      position: relative;
      width: auto;
      padding: 0;
      background: transparent;
      box-shadow: none;
      display: flex;
      flex-direction: row;
    }
  }

  .subheader-dropdown-list-item {
    position: relative;
    list-style: none;
    padding: 0;
    margin: calc(${ThemeVerticalPadding} / 2) calc(${ThemeVerticalPadding} / 2);

    ${DesktopMediaQuery} {
      margin: 0;
      margin-left: ${ThemeHorizontalPadding};
    }
  }
`;

export default ThreadSubheader;
