import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';

import { SubHeaderDropdownListItem } from '../../../components/SubHeader/style';
import Tooltip from '../../../components/Tooltip';
import { getTags, updateThreadTags } from '../../../services/tags';
import { pushSmartNotification } from '../../../utils/notification';
import Modal, { ModalFieldLabel } from '../../../componentsNew/Modals/Modal';
import ModalSelect from '../../../componentsNew/Modals/ModalSelect';
import { SubHeaderButton, Tag } from '../../../componentsNew/Buttons';

const TagButton = ({ thread }) => {
  const [isVisible, toggleIsVisible] = useState(false);
  const [availableTags, setAvailableTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);
  const tagsMap = useRef({});

  const defaultText = 'Select a tag...';

  const toggleVisible = async () => {
    toggleIsVisible((value) => !value);

    if (availableTags.length === 0) {
      const tags = await getTags();
      setAvailableTags(
        tags.map((tag) => ({
          value: tag.id,
          text: tag.name,
        }))
      );
      tags.forEach((tag) => {
        tagsMap.current[tag.id] = tag.name;
      });
    }
  };

  const handleTagAdd = (e) => {
    if (selectedTags.length === 3) {
      return;
    }
    const { value } = e.target;
    if (value !== defaultText) {
      setSelectedTags([...selectedTags, Number(value)]);
    }
  };

  const handleTagRemove = (index) => {
    const newTags = [...selectedTags];
    newTags.splice(index, 1);

    setSelectedTags(newTags);
  };

  const handleSubmit = async () => {
    const payload = {
      threadId: thread.id,
      tags: selectedTags,
    };

    try {
      await updateThreadTags(payload);
      pushSmartNotification({ message: 'Tags updated.' });
      toggleIsVisible();
    } catch (error) {
      pushSmartNotification({ error: 'Could not update tags.' });
    }
  };

  return (
    <>
      <SubHeaderDropdownListItem>
        <Tooltip text="Edit tags">
          <SubHeaderButton status active title="Edit tags" onClick={toggleVisible}>
            <i className="fas fa-tag" />
            <span>Edit tags</span>
          </SubHeaderButton>
        </Tooltip>
      </SubHeaderDropdownListItem>
      <Modal
        iconUrl="/static/icons/tag.png"
        title="Edit tags"
        cancelFn={toggleVisible}
        submitFn={handleSubmit}
        isOpen={isVisible}
      >
        <ModalSelect
          defaultText={defaultText}
          options={availableTags}
          selectedOptions={selectedTags}
          onChange={handleTagAdd}
        />
        <ModalFieldLabel>Selected tags</ModalFieldLabel>
        <div className="selected-tags">
          {selectedTags.map((tag, index) => (
            <Tag
              key={tag}
              type="button"
              className="tag-item"
              data-testid="selected-tag"
              onClick={() => handleTagRemove(index)}
            >
              <i className="fas fa-times-circle" />
              &nbsp;
              {tagsMap.current[tag]}
            </Tag>
          ))}
        </div>
      </Modal>
    </>
  );
};

export default TagButton;

TagButton.propTypes = {
  thread: PropTypes.shape({
    id: PropTypes.number,
  }).isRequired,
};
