/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';

import { SubforumItem, SubforumItemWrapper, SubforumListWrapper } from './components/style';
import PopularThreads from '../../components/PopularThreads';
import SubforumInfo from './components/SubforumInfo';
import SubforumStats from './components/SubforumStats';
import { scrollToTop } from '../../utils/pageScroll';
import { MobileMediaQuery } from '../../components/SharedStyles';

const defaultColors = [
  ['#ff9e39', '#f16d0f'], // general
  ['#80d8ff', '#b53fb1'], // videos
  ['#1bffb1', '#2dbbff'], // gaming
  ['#18caff', '#1aa0ff'], // game generals
  ['#3cff18', '#ffce1a'], // progress report
  ['#4cafe8', '#3b5592'], // developers
  ['#ff7c55', '#f15a2a'], // news
  ['#af5c2d', '#753b24'], // politics
  ['#fff94a', '#669529'], // h&s
  ['#19d0a6', '#2599a5'], // film tv
  ['#fb2e24', '#ae1c1c'], // meta
  ['#da46ff', '#da2361'], // creativity
  ['#ab7977', '#bd4045'] // facepunch
];
const defaultColor = index => defaultColors[index % defaultColors.length];

const SubforumListComponent = ({ subforums, defaultIcon }) => (
  <SubforumListWrapper>
    <Helmet>
      <title>Knockout!</title>
    </Helmet>

    <SubforumItemWrapper>
      {subforums.map((subforum, index) => (
        <SubforumItem
          key={subforum.id + subforum.name}
          id={`subforum-${subforum.id}`}
          icon={(subforum.icon && subforum.icon.replace('/96/', '/150/')) || defaultIcon}
          color={defaultColor(index)}
          onClick={scrollToTop}
        >
          <Link
            title={`${subforum.name}: ${subforum.description}`}
            className="sfitem-header"
            to={`/subforum/${subforum.id}`}
          >
            <p className="sfitem-name">{subforum.name}</p>
            {/* <p className="sfitem-description">{subforum.description}</p> */}
            <SubforumStats subforum={subforum} />
          </Link>
          <SubforumInfo subforum={subforum} />
        </SubforumItem>
      ))}
    </SubforumItemWrapper>

    <PopularThreads />
  </SubforumListWrapper>
);

SubforumListComponent.propTypes = {
  subforums: PropTypes.array.isRequired,
  defaultIcon: PropTypes.string.isRequired
};

export default SubforumListComponent;
