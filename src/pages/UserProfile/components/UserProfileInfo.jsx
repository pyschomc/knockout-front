/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';

import {
  UserInfo,
  UserInfoWrapper,
  UserInfoOverWrapper,
  UserName,
  UserJoinDate,
  UserInfoBackgroundImage,
  BackgroundBottomOverlay,
  BackgroundTopOverlay
} from '../../../components/Post/style';
import { DefaultBlueHollowButton } from '../../../components/SharedStyles';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import config from '../../../../config';
import UserAvatar from '../../../components/UserAvatar';
import UserProfileTopRating from './UserProfileTopRating';
import UserGroupRestricted from '../../../components/UserGroupRestricted';

import { removeUserImage, removeUserProfile } from '../../../services/moderation';

const UserProfileInfo = ({ user, topRatings }) => {
  const userJoinDateShort = dayjs(user.createdAt).format('MMM YYYY');
  const userJoinDateLong = dayjs(user.createdAt).format('DD/MM/YYYY');

  /* Converter because Inacio is gay */
  // This shouldn't be here forever
  // eslint-disable-next-line no-param-reassign
  if (!user.avatar_url) user.avatar_url = user.avatarUrl;
  // eslint-disable-next-line no-param-reassign
  if (!user.isBanned) user.isBanned = user.banned;
  /* End */

  return (
    <UserInfoOverWrapper>
      <UserInfoWrapper style={{ height: '460px', borderRadius: '5px' }}>
        <UserInfo to="#">
          <UserAvatar user={user} />

          <UserRoleWrapper user={user}>
            <UserName>{user.username}</UserName>
          </UserRoleWrapper>

          <UserJoinDate title={`Joined ${userJoinDateLong}`}>
            {userJoinDateShort}

            {userJoinDateLong === dayjs().format('DD/MM/YYYY') && ' 🍰'}
          </UserJoinDate>
          <UserProfileTopRating topRatings={topRatings} />
        </UserInfo>
        <UserInfoBackgroundImage backgroundUrl={`${config.cdnHost}/image/${user.backgroundUrl}`}>
          <BackgroundTopOverlay />
          <BackgroundBottomOverlay />
        </UserInfoBackgroundImage>
      </UserInfoWrapper>
      <UserGroupRestricted userGroupIds={[3, 4, 5]}>
        <DefaultBlueHollowButton
          style={{ marginRight: 'unset' }}
          onClick={() => removeUserImage({ userId: user.id, avatar: true })}
        >
          Remove Avatar
        </DefaultBlueHollowButton>
        <DefaultBlueHollowButton
          style={{ marginRight: 'unset' }}
          onClick={() => removeUserImage({ userId: user.id, background: true })}
        >
          Remove Background
        </DefaultBlueHollowButton>
        <DefaultBlueHollowButton
          style={{ marginRight: 'unset' }}
          onClick={() => removeUserProfile(user.id)}
        >
          Remove profile customizations
        </DefaultBlueHollowButton>
      </UserGroupRestricted>
    </UserInfoOverWrapper>
  );
};

UserProfileInfo.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    usergroup: PropTypes.number.isRequired,
    backgroundUrl: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired
  }).isRequired,
  topRatings: PropTypes.array
};
UserProfileInfo.defaultProps = {
  topRatings: []
};

export default UserProfileInfo;
