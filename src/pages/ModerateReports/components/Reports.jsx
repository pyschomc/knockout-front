/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/button-has-type */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Post from '../../../components/Post';
import { closeReport } from '../../../services/moderation';

import { ThemeVerticalPadding, ThemeHorizontalPadding } from '../../../Theme';

const ReportItem = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: ${ThemeVerticalPadding};
  margin-bottom: ${ThemeVerticalPadding};
`;

const ReportInfo = styled.div`
  background: #dc4035;
  border-radius: 5px;
  ${props => props.backgroundColor && `background: ${props.backgroundColor};`}
  color: white;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  margin-bottom: ${ThemeVerticalPadding};

  button {
    float: right;
    background: transparent;
    border: none;
    color: white;
    line-height: 22px;
    cursor: pointer;
  }
`;

const Reports = ({ reports, getOpenReports }) => (
  <div>
    {reports.map(item => {
      const {
        postUsername: username,
        avatarUrl,
        userJoinDate,
        postContent: postBody,
        postDate,
        postId,
        ratings,
        byCurrentUser,
        bans,
        postUserId,
        postThreadId,
        postPage,
        subforumName
      } = item;

      const user = {
        username,
        avatar_url: avatarUrl,
        userJoinDate,
        id: postUserId
      };

      return (
        <ReportItem key={`${item.reportId}-${item.postId}`}>
          <ReportInfo>
            &quot;{item.reportReason}&quot; - reported by {item.reportUsername} on{' '}
            <Link to={`/thread/${postThreadId}/${item.postPage}#post-${item.postId}`}>
              <b>this thread</b> in {subforumName}
            </Link>
            <button
              onClick={async () => {
                await closeReport(item.reportId);
                await getOpenReports();
              }}
            >
              Mark as handled
            </button>
          </ReportInfo>
          <Post
            username={username}
            avatarUrl={avatarUrl}
            userJoinDate={userJoinDate}
            postBody={postBody}
            postDate={postDate}
            postId={postId}
            ratings={ratings}
            user={user}
            byCurrentUser={byCurrentUser}
            bans={bans}
            thread={postThreadId}
            postPage={postPage}
          />
        </ReportItem>
      );
    })}
  </div>
);

Reports.propTypes = {
  reports: PropTypes.array.isRequired,
  getOpenReports: PropTypes.func.isRequired
};
export default Reports;
