import styled from 'styled-components';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeTextColor,
} from '../../../utils/ThemeNew';

export const HomePageWrapper = styled.section`
  display: grid;
  grid-template-columns: 300px auto;
  grid-template-rows: 0;
  grid-gap: ${ThemeHorizontalPadding};
  font-size: ${ThemeFontSizeMedium};
  min-height: 900px;
  height: max-content;
  padding: 0 ${ThemeHorizontalPadding};
  color: ${ThemeTextColor};

  @media (max-width: 900px) {
    display: flex;
    flex-direction: column-reverse;
  }
`;

export const HomePageSubforumContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  align-content: baseline;

  @media (max-width: 900px) {
    grid-template-columns: 1fr;
  }
`;
