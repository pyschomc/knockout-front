import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { HashLink as Link } from 'react-router-hash-link';
import styled from 'styled-components';
import { updateMention, getMentions } from '../../../services/mentions';
import { updateMentions } from '../../../state/mentions';
import {
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { buttonHoverBrightness } from '../../../components/SharedStyles';

async function handleMentionDelete(postId, dispatch) {
  await updateMention(postId);
  dispatch(updateMentions(await getMentions()));
}

function parseMentionContent(mention) {
  return JSON.parse(mention.content)[0];
}

const MentionsContainer = ({ toggleOpen }) => {
  const dispatch = useDispatch();
  const mentions = useSelector((state) => state.mentions.mentions || []);

  const handleMentionClick = (postId) => {
    toggleOpen();
    handleMentionDelete(postId, dispatch);
  };

  return (
    <StyledMentionsContainer mentions={mentions}>
      {mentions.length === 0 && <MentionEmpty>You have no new mentions!</MentionEmpty>}

      {mentions.map((mention) => (
        <MentionItemWrapper key={mention.mentionId}>
          <MentionItem
            to={`/thread/${mention.threadId}/${mention.threadPage}#post-${mention.postId}`}
            onClick={() => handleMentionClick(mention.postId)}
          >
            {parseMentionContent(mention)}
          </MentionItem>
          <DeleteMentionButton onClick={() => handleMentionDelete(mention.postId, dispatch)}>
            <i className="fas fa-eraser" />
          </DeleteMentionButton>
        </MentionItemWrapper>
      ))}
    </StyledMentionsContainer>
  );
};

MentionsContainer.propTypes = {
  toggleOpen: PropTypes.func.isRequired,
};

export default MentionsContainer;

const MentionItemWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  background: ${ThemeBackgroundDarker};
  box-sizing: border-box;
  color: ${ThemeTextColor};
`;

const MentionEmpty = styled.div`
  display: block;
  margin: ${ThemeVerticalPadding} auto;
  background: ${ThemeBackgroundDarker};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  box-sizing: border-box;
  color: ${ThemeTextColor};
`;

const MentionItem = styled(Link)`
  display: block;
  flex-grow: 1;
  line-height: 1.3;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

const StyledMentionsContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 50vh;
  border-top: solid 1px #666;
  background: ${ThemeBackgroundDarker};

  scrollbar-width: thin;
  scrollbar-color: ${ThemeTextColor} ${ThemeBackgroundLighter};

  ::-webkit-scrollbar {
    width: 5px;
  }

  ::-webkit-scrollbar-thumb {
    background: #ddd;
  }

  ::-webkit-scrollbar-track {
    background: #666;
  }
`;

const DeleteMentionButton = styled.button`
  cursor: pointer;
  color: white;
  min-height: 25px;
  background: rgb(187, 37, 37);
  border: none;
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  ${buttonHoverBrightness}
`;
