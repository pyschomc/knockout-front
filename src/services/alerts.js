import { authPost, authDelete } from './common';

export const createAlertRequest = async ({ threadId, lastSeen, previousLastSeen }) => {
  if (!threadId || !lastSeen) throw Error('Invalid alert');

  if (new Date(lastSeen) > new Date(previousLastSeen)) {
    const requestBody = {
      lastSeen,
      threadId,
    };

    const response = await authPost({ url: '/alert', data: requestBody });

    return response;
  }

  return null;
};

export const deleteAlertRequest = async ({ threadId }) => {
  if (!threadId) return { error: 'Invalid thread id' };

  const requestBody = {
    threadId,
  };

  const response = await authDelete({ url: '/alert', data: requestBody });

  return response;
};

export const getAlerts = async () => {
  const user = localStorage.getItem('currentUser');

  if (!user) {
    return [];
  }
  try {
    const results = await authPost({ url: '/alert/list', data: {} });

    return results.data;
  } catch (err) {
    return [];
  }
};
