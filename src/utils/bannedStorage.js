export const loadBannedMessageFromStorage = () => {
  const banInformation = JSON.parse(localStorage.getItem('banInformation'));

  if (banInformation) {
    return banInformation;
  }
  return null;
};

export const saveBannedMessageToStorage = data => {
  const { banMessage, threadId } = data;
  const banInformation = JSON.stringify({ banMessage, threadId });

  localStorage.setItem('banInformation', banInformation);
};

export const clearBannedMessageFromStorage = () => {
  localStorage.removeItem('banInformation');
};
