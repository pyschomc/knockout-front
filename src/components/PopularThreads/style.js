import { Link } from 'react-router-dom';
import styled, { keyframes } from 'styled-components';
import { TabletMediaQuery } from '../SharedStyles';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeLargeTextSize,
  ThemeMediumTextSize,
  ThemeSmallTextSize,
  ThemeSecondaryBackgroundColor,
  ThemeSecondaryTextColor,
  ThemeTertiaryTextColor,
  ThemeScrollbarHandleHover,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker
} from '../../Theme';

export const PopularThreadWrapper = styled.div`
  position: relative;
  display: flex;
  flex-shrink: 0;
  min-width: 300px;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalHalfPadding};

  ${TabletMediaQuery} {
    padding-right: ${ThemeHorizontalPadding};
  }
`;

export const PopularThreadList = styled.div`
  width: 100%;
  min-height: 75px;
  padding: 0;
  margin-bottom: 15px;
  box-sizing: border-box;
  font-size: 18px;
  color: white;
  text-decoration: none;
  position: relative;
  background: ${ThemeSecondaryBackgroundColor};
  transition: background 0.15s ease-in-out;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  border-radius: 5px;
`;

export const PopularThreadHeader = styled.div`
  position: relative;
  display: flex;
  margin: 0;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  background: ${ThemeBackgroundLighter};
  box-shadow: inset 0 1px rgba(0, 0, 0, 0.03), 0 1px rgba(0, 0, 0, 0.03);
  color: white;
  text-decoration: none;
  width: 100%;
  box-sizing: border-box;
  overflow: hidden;
  flex-direction: column;
  border-radius: 5px 5px 0 0;
`;

export const PopularThreadModeSwitch = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  top: ${ThemeVerticalPadding};
  right: ${ThemeHorizontalPadding};
  bottom: ${ThemeVerticalPadding};
  width: 40px;
  border-radius: 5px;
  background: ${ThemeBackgroundDarker};
  color: ${ThemeSecondaryTextColor};
  cursor: pointer;
`;

export const PopularThreadTitle = styled.h2`
  margin: 0;
  padding: 0;
  color: ${ThemeTertiaryTextColor};
  line-height: 1.4;
  font-size: ${ThemeLargeTextSize};
  font-weight: bold;
  white-space: nowrap;
`;

export const PopularThreadDescription = styled.h3`
  display: inline-block;
  margin: 0;
  padding: 0;
  line-height: 1.4;
  font-size: ${ThemeMediumTextSize};
  font-weight: 100;
  color: ${ThemeSecondaryTextColor};
  flex-grow: 1;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;

export const PopularThreadContent = styled.div`
  position: relative;
  display: block;
  flex-grow: 1;
  overflow: hidden;
`;

export const PopularThreadContentScoller = styled.div`
  position: absolute;
  display: block;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  overflow: hidden auto;

  /* width */
  &::-webkit-scrollbar {
    width: 15px;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: ${ThemeBackgroundLighter};
    border: 4px solid ${ThemeBackgroundDarker};
  }

  /* Handle on hover */
  &::-webkit-scrollbar-thumb:hover {
    background: ${ThemeScrollbarHandleHover};
  }

  /* Firefox scrollbar */
  scrollbar-width: thin;
  scrollbar-color: ${ThemeBackgroundLighter} ${ThemeBackgroundDarker};

  ${TabletMediaQuery} {
    position: relative;
  }
`;

export const PopularThreadContentShadow = styled.div`
  position: absolute;
  display: none;
  bottom: 0;
  left: 0;
  right: 0;
  height: 75px;
  pointer-events: none;
  background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5));
  border-radius: 0 0 5px 5px;

  ${TabletMediaQuery} {
    display: none;
  }
`;

export const PopularThreadItem = styled(Link)`
  position: relative;
  display: flex;
  flex-direction: row;
  border-radius: 5px;
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  margin-right: 0;
  text-decoration: none;

  &:hover {
    background: ${ThemeBackgroundLighter};
  }
`;

export const PopularThreadItemIcon = styled.div`
  width: 48px;
  height: 100%;
  flex-shrink: 0;
`;

export const PopularThreadItemContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex-grow: 1;
  flex-shrink: 1;
  padding-left: ${ThemeHorizontalHalfPadding};
  width: calc(100% - 48px);
`;

export const PopularThreadItemTitle = styled.h2`
  display: block;
  margin: 0;
  padding: 0;
  color: ${ThemeTertiaryTextColor};
  line-height: 1.3;
  font-weight: 100;
  font-size: ${ThemeSmallTextSize};
  white-space: pre-line;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const PopularThreadItemDescription = styled.h3`
  display: block;
  margin: 0;
  padding: 0;
  line-height: 1.3;
  font-size: ${ThemeSmallTextSize};
  font-weight: 100;
  color: ${ThemeSecondaryTextColor};
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;

const placeholderAnim = keyframes`
  0% {
    transform: translateX(0%);
  }
  100% {
    transform: translateX(-75%);
  }
`;

export const StyledItemPlaceholder = styled.div`
  width: calc(100% - ${ThemeHorizontalPadding} - ${ThemeHorizontalPadding});
  position: relative;
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  min-height: 50px;

  .fake-title,
  .fake-info,
  .fake-icon {
    position: absolute;
    border-radius: 4px;
    overflow: hidden;

    &:after {
      content: '';
      display: block;
      width: 400%;
      height: 100%;
      left: 0;
      position: absolute;
      background: linear-gradient(
        90deg,
        ${ThemeBackgroundLighter} 0%,
        ${ThemeBackgroundDarker} 25%,
        ${ThemeBackgroundLighter} 75%,
        ${ThemeBackgroundDarker} 100%
      );
      animation: ${placeholderAnim} 1s linear infinite;
    }
  }
  .fake-icon {
    width: 48px;
    height: 48px;
    top: 0;
    left: 0;
  }
  .fake-title {
    width: 40%;
    height: ${ThemeMediumTextSize};
    top: 50%;
    transform: translateY(-15px);
    left: calc(48px + ${ThemeHorizontalHalfPadding});
  }
  .fake-info {
    width: 70%;
    height: ${ThemeMediumTextSize};
    bottom: 50%;
    transform: translateY(15px);
    left: calc(48px + ${ThemeHorizontalHalfPadding});
  }
`;
