import React from 'react';
import LoginButton from '../../../components/LoginButton';

const PatreonContainer = () => (
  <div className="panel">
    <h2 title="Richboi Zone">Patreon Bonuses</h2>
    <span>Connects accounts permanently</span>
    <span>don&apos;t use if you&apos;re not a Patreon supporter!</span>
    <LoginButton color="white" background="#e85b46" route="/link/patreon">
      <i className="fab fa-patreon" />
      <span style={{ color: 'white' }}>Claim Bonuses</span>
    </LoginButton>
  </div>
);

export default PatreonContainer;
