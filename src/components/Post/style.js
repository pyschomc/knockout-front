import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import {
  ThemeMediumTextSize,
  ThemeSmallTextSize,
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeSecondaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeBackgroundImageGradient,
  ThemeBackgroundImageGradientHover,
} from '../../Theme';

export const PostWrapper = styled.li`
  position: relative;
  display: flex;
  margin: 0 0 ${ThemeVerticalHalfPadding} 0;
  border-radius: 5px;
  overflow: hidden;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  min-height: 200px;

  @media (max-width: 960px) {
    flex-direction: column;
  }
`;

const backgroundOverlayShared = css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-size: cover;
`;

export const BackgroundBottomOverlay = styled.div`
  ${backgroundOverlayShared}
  background-image: ${ThemeBackgroundImageGradientHover};
`;
export const BackgroundTopOverlay = styled.div`
  ${backgroundOverlayShared}
  opacity: 1;
  background-image: ${ThemeBackgroundImageGradient};

  transition: opacity ease-in-out 500ms;
  transition-delay: 200ms;
`;

export const UserInfoBackgroundImage = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 460px;
  z-index: 0;
  background-image: url(${(props) => props.backgroundUrl});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

export const UserInfo = styled(Link)`
  margin-bottom: auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: start;
  z-index: 2;
  position: relative;

  transition: opacity ease-in-out 500ms, min-height 250ms ease-in-out;
  transition-delay: 550ms;

  min-height: 0px;

  @media (min-width: 960px) {
    &:hover {
      min-height: 360px;
    }
  }

  @media (max-width: 960px) {
    flex-direction: row;
    justify-content: unset;

    & > span {
      flex-grow: 1;
    }

    img,
    source {
      max-width: 40px;
      height: 40px;
    }
  }
`;

export const UserInfoWrapper = styled.div`
  padding: 45px 0 60px;
  width: 230px;
  background: ${ThemeQuaternaryBackgroundColor};
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  border-left: 1px solid transparent;
  box-sizing: border-box;

  ${(props) => props.isUnread && `border-left: 4px solid #4368ad;`}
  ${(props) => props.byCurrentUser && `border-left: 4px solid #ffd817;`}
  ${(props) => props.isLinkedPost && `border-left: 4px dashed #ff661d;`}

  @media (max-width: 960px) {
    bottom: auto;
    width: auto;
    right: 0;
    padding: 10px;
  }

  &:hover {
    @media (min-width: 960px) {
      ${BackgroundTopOverlay} {
        opacity: 0;
      }
      ${UserInfo} {
        opacity: 0;
      }
    }
  }
`;

export const UserInfoOverWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const UserName = styled.span`
  font-weight: bold;
  text-align: center;
  font-size: ${ThemeMediumTextSize};
  margin-bottom: 10px;
`;

export const UserJoinDate = styled.span`
  font-size: ${ThemeSmallTextSize};
  margin-top: ${ThemeVerticalPadding};
  color: ${ThemePrimaryTextColor};

  @media (max-width: 960px) {
    margin-left: 15px;
    margin-top: 0;
    flex-grow: 0 !important;
  }
`;

export const PostBody = styled.div`
  width: 100%;
  min-height: 200px;
  display: flex;
  flex-direction: column;
  position: relative;
  line-height: 1.5;
  background: ${ThemeSecondaryBackgroundColor};
  box-shadow: -5px 0 5px rgba(0, 0, 0, 0.1);
  ${(props) => !props.canRate && `padding-bottom: calc(${ThemeVerticalPadding(props)} * 3);`}

  @media (min-width: 960px) {
    max-width: 100%;
  }
`;

export const PostDate = styled.div`
  display: flex;
  justify-content: space-between;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalHalfPadding};
  padding-left: ${ThemeHorizontalPadding};
  align-items: center;
  font-size: 12px;
  background: ${ThemeQuaternaryBackgroundColor};
`;

export const PostContent = styled.div`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  flex-grow: 1;
  font-size: ${ThemeMediumTextSize};
  color: ${ThemePrimaryTextColor};
  white-space: pre-wrap;
  overflow-wrap: break-word;
  min-height: 160px;

  /* bbcode styling */
  .bb-post {
    > ul {
      list-style-type: disc;
    }
    > li {
      list-style: none;
    }

    a {
      word-break: break-all;
    }
  }
`;

export const PostButtonContainer = styled.div`
  height: 100%;
  display: inline-flex;
`;

export const PostReplyToButton = styled.button`
  background: transparent;
  color: ${ThemePrimaryTextColor};
  border: none;
  height: 100%;
  font-size: ${ThemeMediumTextSize};
  cursor: pointer;
  opacity: 1;
  transition: opacity 85ms ease-in-out;
  ${(props) => !props.square && `border-top-right-radius: 5px;`}

  &:hover {
    opacity: 0.5;
  }
`;

export const PostBanUserButton = styled(PostReplyToButton)`
  border-top-right-radius: 0;
  opacity: 1;
  transition: opacity 85ms ease-in-out, color 85ms ease-in-out;

  &:hover {
    opacity: 0.5;
    color: #fcd743;
  }
`;

export const PostReplyToButtonIcon = styled.i`
  margin-right: 10px;
`;

export const BanInfoWrapper = styled.div`
  width: 100%;
  min-height: 50px;
  background: #dc4035;
  padding: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
`;

export const BanInfoIcon = styled.img`
  width: 50px;
  height: 50px;
  margin-right: 15px;
`;

export const BanInfoText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 100%;
  min-height: 40px;
`;

export const BanInfoStrong = styled.span`
  font-weight: bold;
`;

export const PostShowBBCodeButton = styled.button`
  background: transparent;
  color: ${ThemePrimaryTextColor};
  border: none;
  height: 100%;
  font-size: ${ThemeMediumTextSize};
  cursor: pointer;
  opacity: 1;
  transition: opacity 85ms ease-in-out;
  ${(props) => !props.square && `border-top-right-radius: 5px;`}

  &:hover {
    opacity: 0.5;
  }
`;

export const PostShowBBCodeIcon = styled.i`
  margin-right: 10px;
`;
