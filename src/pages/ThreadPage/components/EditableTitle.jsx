import React from 'react';
import PropTypes from 'prop-types';

import {
  SubHeaderDropdownListItem,
  SubHeaderRealButton,
} from '../../../components/SubHeader/style';
import LoggedInOnly from '../../../components/LoggedInOnly';
import { usergroupCheck } from '../../../components/UserGroupRestricted';
import { updateThread } from '../../../services/threads';
import { pushSmartNotification, pushNotification } from '../../../utils/notification';
import Tooltip from '../../../components/Tooltip';

class EditableTitle extends React.Component {
  constructor(props) {
    super(props);
    this.state = { title: 'Thread' };
  }

  static getDerivedStateFromProps(props, state) {
    return { ...state, title: props.title };
  }

  edit = async () => {
    const newTitle = prompt('Please enter your new title', this.state.title);
    const { threadId } = this.props;

    try {
      if (newTitle.length > 140) {
        pushSmartNotification({ error: 'Title too long. Must be 140 or less' });
        throw new Error({ error: 'Title too long' });
      }

      await updateThread({ title: newTitle, id: threadId });

      pushNotification({ message: 'Thread title updated. Refresh to see the new title! (wip)' });
    } catch (err) {
      this.setState({ editing: true });
    }
  };

  render() {
    const { byCurrentUser } = this.props;

    return (
      <LoggedInOnly>
        {(usergroupCheck([3, 4, 5]) || byCurrentUser) && (
          <SubHeaderDropdownListItem>
            <Tooltip text="Edit title">
              <SubHeaderRealButton onClick={() => this.edit()}>
                <i className="fas fa-pencil-alt" />
                <span>Rename</span>
              </SubHeaderRealButton>
            </Tooltip>
          </SubHeaderDropdownListItem>
        )}
      </LoggedInOnly>
    );
  }
}

EditableTitle.propTypes = {
  byCurrentUser: PropTypes.bool,
  threadId: PropTypes.number,
  title: PropTypes.string,
};
EditableTitle.defaultProps = {
  byCurrentUser: false,
  threadId: 0,
  title: '',
};

export default EditableTitle;
