export const MENTION_UPDATE = 'MENTION_UPDATE';

export function updateMentions(value) {
  return {
    type: MENTION_UPDATE,
    value
  };
}
