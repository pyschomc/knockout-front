import React, { useState } from 'react';
import styled from 'styled-components';
import Rules from './components/Rules';
import PrivacyPolicy from './components/PrivacyPolicy';
import ModeratorCoc from './components/ModeratorCoc';
import StyleWrapper from './components/StyleWrapper';
import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeTextColor,
  ThemeHighlightWeaker,
} from '../../utils/ThemeNew';

const RulesPage = () => {
  const [tab, setTab] = useState(0);

  const CurrentTab = () => {
    switch (tab) {
      case 0:
        return <Rules />;
      case 1:
        return <PrivacyPolicy />;
      case 2:
        return <ModeratorCoc />;
      default:
        return <Rules />;
    }
  };

  return (
    <StyleWrapper>
      <StyledRulesPage>
        <div className="tabs">
          <button onClick={() => setTab(0)} className={tab === 0 ? 'current' : ''} type="button">
            Rules
          </button>
          <button onClick={() => setTab(1)} className={tab === 1 ? 'current' : ''} type="button">
            Privacy
          </button>
          <button onClick={() => setTab(2)} className={tab === 2 ? 'current' : ''} type="button">
            Moderator Code of Conduct
          </button>
        </div>
        {CurrentTab()}
      </StyledRulesPage>
    </StyleWrapper>
  );
};

const tabHeight = '50px';

const StyledRulesPage = styled.div`
  padding: calc(${ThemeHorizontalPadding} * 3);
  padding-top: calc((${ThemeHorizontalPadding} * 3) + ${tabHeight});
  position: relative;

  .tabs {
    width: 100%;
    height: ${tabHeight};
    display: flex;
    justify-content: space-around;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    border-bottom: 2px solid rgba(0, 0, 0, 0.2);
    button {
      position: relative;
      text-transform: capitalize;
      background-color: transparent;
      cursor: pointer;
      border: none;
      color: ${ThemeTextColor};
      height: ${tabHeight};
      line-height: ${tabHeight};
      padding: 0 calc(${ThemeHorizontalPadding} * 2);
      &:before,
      &:after {
        content: ' ';
        position: absolute;
        height: 2px;
        width: 0%;
        background-color: ${ThemeHighlightWeaker};
        bottom: 0;
        transition: width 200ms ease-in-out;
      }
      &:before {
        right: 50%;
      }
      &:after {
        left: 50%;
      }
      &:focus,
      &:hover {
        outline: none;
        &:before,
        &:after {
          width: 25%;
        }
      }
      &.current {
        &:after,
        &:before {
          content: ' ';
          position: absolute;
          height: 2px;
          width: 50%;
        }
      }
    }
  }
  @media (max-width: 450px) {
    padding-top: calc(${ThemeHorizontalPadding} * 3);
    .tabs {
      margin-bottom: calc(${ThemeVerticalPadding} * 3);
      flex-direction: column;
      height: auto;
      position: relative;
      align-items: flex-start;
      button {
        display: block;
      }
    }
  }
`;

export default RulesPage;
