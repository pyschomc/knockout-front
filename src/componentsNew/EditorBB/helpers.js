/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */
import replaceBetween from '../../utils/replaceBetween';
import {
  isBoldHotkey,
  isCodeHotkey,
  isItalicHotkey,
  isStrikethroughHotkey,
  isSubmitHotkey,
  isUnderlinedHotkey,
} from './editorHotkeys';

import { isImage } from '../../components/KnockoutBB/components/ImageBB';
import { getStrawPollId } from '../../components/KnockoutBB/components/StrawPollBB';
import { getStreamableId } from '../../components/KnockoutBB/components/StreamableBB';
import { getTweetId } from '../../components/KnockoutBB/components/TweetBB';
import { isVideo } from '../../components/KnockoutBB/components/VideoBB';
import { getVimeoId } from '../../components/KnockoutBB/components/VimeoBB';
import { getYoutubeId } from '../../components/KnockoutBB/components/YoutubeBB';

export const httpRegex = new RegExp(
  '^' +
    '(?:(?:(?:https?|ftp):)?\\/\\/)' +
    '(?:\\S+(?::\\S*)?@)?' +
    '(?:' +
    '(?!(?:10|127)(?:\\.\\d{1,3}){3})' +
    '(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})' +
    '(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})' +
    '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
    '(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
    '(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
    '|' +
    '(?:' +
    '(?:' +
    '[a-z0-9\\u00a1-\\uffff]' +
    '[a-z0-9\\u00a1-\\uffff_-]{0,62}' +
    ')?' +
    '[a-z0-9\\u00a1-\\uffff]\\.' +
    ')+' +
    '(?:[a-z\\u00a1-\\uffff]{2,}\\.?)' +
    ')' +
    '(?::\\d{2,5})?' +
    '(?:[/?#]\\S*)?' +
    '$',
  'i'
);

export const handlePaste = (e, setContent) => {
  const clipboardData = e.clipboardData.getData('Text');

  if (isImage(clipboardData)) {
    if (confirm('Insert img?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[img]${clipboardData}[/img]`);
    }
  } else if (getYoutubeId(clipboardData)) {
    if (confirm('Insert youtube?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[youtube]${clipboardData}[/youtube]`);
    }
  } else if (getVimeoId(clipboardData)) {
    if (confirm('Insert Vimeo video?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[vimeo]${clipboardData}[/vimeo]`);
    }
  } else if (getStreamableId(clipboardData)) {
    if (confirm('Insert Streamable video?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[streamable]${clipboardData}[/streamable]`);
    }
  } else if (isVideo(clipboardData)) {
    if (confirm('Insert video?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[video]${clipboardData}[/video]`);
    }
  } else if (getStrawPollId(clipboardData)) {
    if (confirm('Insert strawpoll?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[strawpoll]${clipboardData}[/strawpoll]`);
    }
  } else if (getTweetId(clipboardData)) {
    if (confirm('Insert twitter?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[twitter]${clipboardData}[/twitter]`);
    }
  } else if (httpRegex.test(clipboardData)) {
    if (confirm('Insert url?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[url href="${clipboardData}"][/url]`);
    }
  }
};

function selectionRangeValid(selectionRange) {
  return (
    selectionRange[0] !== undefined &&
    selectionRange[1] !== undefined &&
    selectionRange[0] !== selectionRange[1]
  );
}

function modifySelectionRange(insertedString, content, input, setSelectionRange) {
  const { length } = insertedString;
  const contentLength = content.length;

  setSelectionRange([contentLength - length, contentLength - length]);

  setTimeout(() => {
    input.setSelectionRange(contentLength - length, contentLength - length);
    input.focus();
  }, 100);
}

export const insertTag = (type, content, setContent, selectionRange, setSelectionRange, input) => {
  if (type === 'li') {
    setContent((prevState) => `${prevState}[ul][li][/li][/ul]`);
    // } else if (type === 'quote') {
    //   const { threadPage, threadId, postId, username, postContent, mentionsUser } = content;
    //   setContent((prevState) =>
    //     `${prevState} [quote mentionsUser="${mentionsUser}" postId="${postId}" threadPage="${threadPage}" threadId="${threadId}" username="${username}"]${postContent}[/quote]`.trim()
    //   );
  } else if (selectionRangeValid(selectionRange)) {
    setContent((prevState) =>
      replaceBetween(prevState, selectionRange[0], selectionRange[1], [`[${type}]`, `[/${type}]`])
    );
    setSelectionRange([content.length, content.length]);
    input.focus();
  } else {
    const updatedContent = `${content}[${type}][/${type}]`;
    setContent((prevState) => `${prevState}[${type}][/${type}]`);
    modifySelectionRange(`[/${type}]`, updatedContent, input, setSelectionRange);
  }
};

export function checkForHotkeys(
  e,
  content,
  setContent,
  selectionRange,
  setSelectionRange,
  input,
  handleSubmit
) {
  if (isBoldHotkey(e)) {
    e.preventDefault();
    insertTag('b', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isItalicHotkey(e)) {
    e.preventDefault();
    insertTag('i', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isUnderlinedHotkey(e)) {
    e.preventDefault();
    insertTag('u', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isCodeHotkey(e)) {
    e.preventDefault();
    insertTag('code', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isStrikethroughHotkey(e)) {
    e.preventDefault();
    insertTag('s', content, setContent, selectionRange, setSelectionRange, input);
  } else if (handleSubmit !== undefined && isSubmitHotkey(e)) {
    handleSubmit();
  }
}
