import axios from 'axios';
import config from '../../config';
import { pushSmartNotification } from '../utils/notification';
import { authGet, authPost, authPut } from './common';

const APP_NAME = 'knockout.chat';

export const submitPost = ({ content, thread_id: threadId, sendCountryInfo }) => {
  // Separate the following code

  const contentSerialized = content;
  const contentLength = contentSerialized.length;

  const contentStringified = content;

  if (contentLength > 8000) {
    pushSmartNotification({ error: 'Post body too long.' });
    throw new Error('Post body too long.');
  }
  if (contentLength < 1) {
    pushSmartNotification({ error: `Post body too short. ${contentLength}/10` });
    throw new Error('Post body too short.');
  }

  const requestBody = {
    content: contentStringified,
    thread_id: threadId,
    displayCountryInfo: sendCountryInfo,
    appName: APP_NAME,
  };

  return authPost({ url: '/post', data: requestBody });
};

export const getPostList = async () => {
  const res = await axios.get(`${config.apiHost}/post`);

  const { data } = res;

  return data;
};

export const getPost = (postId) => authGet({ url: `/post/${postId}` });

export const updatePost = ({ content, id, threadId }) => {
  const contentSerialized = content;
  const contentLength = contentSerialized.length;

  const contentStringified = content;

  if (contentLength > 8000) {
    pushSmartNotification({ error: 'Post body too long.' });
    throw new Error('Post body too long.');
  }
  if (contentLength < 1) {
    pushSmartNotification({ error: `Post body too short. ${contentLength}/10` });
    throw new Error('Post body too short.');
  }

  const requestBody = {
    content: contentStringified,
    id,
    threadId,
    appName: APP_NAME,
  };

  return authPut({ url: '/post', data: requestBody });
};
