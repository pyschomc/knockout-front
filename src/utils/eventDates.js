import dayjs from 'dayjs';

export const getEventHeaderLogo = () => {
  const currentDate = dayjs();

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return '/static/logo_summer.svg';
  }

  return null;
};

export const getEventColor = () => {
  const currentDate = dayjs();

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return '#ff3b4e';
  }

  return null;
};

export const getEventQuotes = () => {
  const currentDate = dayjs();

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return [
      'Happy holidays!',
      "Summer's here!",
      "It's getting hot in here 🥵",
      'Ever wonder how Santa manages to wear those heavy clothes in December?',
      '☀️',
      '2020 edition',
      '🏄',
      '🎄',
      'This year, my gift to you is anime memes 🎁',
      'Brazilian Christmas Edition',
      "We didn't have enough money to put snow on the site this year, so we're going with a summer theme",
      'Cultural exchange edition',
      '"During summer temperatures can often reach 40 degrees Celsius (86 to 104 degrees Fahrenheit) in Rio de Janeiro and..."',
      "There... there's actually people in the other hemisphere?"
    ];
  }

  return null;
};
