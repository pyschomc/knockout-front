/* eslint-disable no-alert */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import LoggedInOnly from '../../LoggedInOnly';
import UserGroupRestricted, { usergroupCheck } from '../../UserGroupRestricted';
import submitBan from '../../../services/ban';
import submitReport from '../../../services/reports';
import { ThemeTextColor, ThemeHorizontalPadding } from '../../../utils/ThemeNew';

const banPrompt = ({ user, postId }) => {
  submitBan({
    userId: user.id,
    postId,
    banReason: window.prompt("What's the ban reason? (required)"),
    banLength: window.prompt(
      'How long should this user be banned for? | 24: 1 day | 72: 3 days | 168: 1 week | 720: 1 month | 0: PERMABAN'
    ),
  });
};

const reportPrompt = (postId) => {
  submitReport({
    postId,
    reportReason: window.prompt('What rule did this user break?'),
  });
};

const PostControls = ({
  hideControls,
  user,
  postId,
  showBBCode,
  toggleEditable,
  replyToPost,
  byCurrentUser,
  threadLocked,
}) => {
  return (
    !hideControls && (
      <LoggedInOnly>
        <StyledPostControls>
          <UserGroupRestricted userGroupIds={[3, 4, 5, 6]}>
            <button
              className="btn-control"
              type="button"
              onClick={() => banPrompt({ user, postId })}
              title="Ban User"
            >
              <i className="fas fa-gavel" />
            </button>
          </UserGroupRestricted>

          <button
            type="button"
            className="btn-control"
            onClick={() => reportPrompt(postId)}
            title="Report post"
          >
            <i className="fas fa-flag" />
          </button>

          {!byCurrentUser && (
            <button type="button" className="btn-control" onClick={showBBCode} title="Show BBCode">
              <i className="fas fa-code" />
            </button>
          )}
          {!threadLocked && (usergroupCheck([3, 4, 5]) || byCurrentUser) && (
            <button type="button" className="btn-control" onClick={toggleEditable} title="Edit">
              <i className="fas fa-pencil-alt" />
            </button>
          )}
          {!threadLocked && (
            <button
              type="button"
              className="btn-control"
              onClick={() => replyToPost()}
              title="Reply"
            >
              <i className="fas fa-reply" />
            </button>
          )}
        </StyledPostControls>
      </LoggedInOnly>
    )
  );
};
export default PostControls;
PostControls.propTypes = {
  hideControls: PropTypes.bool,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    isBanned: PropTypes.bool.isRequired,
    avatar_url: PropTypes.string.isRequired,
  }).isRequired,
  postId: PropTypes.number.isRequired,
  showBBCode: PropTypes.func,
  toggleEditable: PropTypes.func,
  replyToPost: PropTypes.func,
  byCurrentUser: PropTypes.bool,
  threadLocked: PropTypes.bool,
};
PostControls.defaultProps = {
  hideControls: false,
  showBBCode: undefined,
  toggleEditable: undefined,
  replyToPost: undefined,
  byCurrentUser: false,
  threadLocked: false,
};

const StyledPostControls = styled.div`
  display: flex;

  .btn-control {
    height: 100%;
    background: transparent;
    color: ${ThemeTextColor};
    opacity: 0.6;
    border: none;
    cursor: pointer;
    padding: 0 calc(${ThemeHorizontalPadding} * 1.5);
    transition: opacity 100ms ease-in-out;
    &:hover {
      opacity: 1;
    }
  }
`;
