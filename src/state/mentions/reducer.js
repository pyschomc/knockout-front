import { MENTION_UPDATE } from './actions';

const initialState = {
  mentions: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case MENTION_UPDATE:
      return { ...state, mentions: action.value };
    default:
      break;
  }
  return state;
}
