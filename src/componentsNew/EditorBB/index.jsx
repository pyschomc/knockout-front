/* eslint-disable react/jsx-curly-newline */
/* eslint-disable no-restricted-globals */
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import EditorToolbar from './components/EditorToolbar';
import EditorOptions from './components/EditorOptions';
import StyledEditorBB from './style';
import Tooltip from '../Tooltip';

import { handlePaste, checkForHotkeys } from './helpers';
import {
  loadExtraScriptingSettingFromStorage,
  saveExtraScriptingInfoSettingToStorage,
  loadDisplayCountrySettingFromStorage,
  saveDisplayCountryInfoSettingToStorage,
} from '../../utils/postOptionsStorage';

const EditorBB = ({ content, setContent, handleSubmit, children }) => {
  const [allowExtraScripting, setAllowExtraScripting] = useState(false);
  const [sendCountryInfo, setSendCountryInfo] = useState(false);
  const [selectionRange, setSelectionRange] = useState([]);
  const [showOptions, setShowOptions] = useState(false);
  const inputRef = useRef(undefined);

  const updateContent = (e) => {
    setContent(e.target.value);
  };

  const setSelection = (e) => {
    setSelectionRange([e.target.selectionStart, e.target.selectionEnd]);
  };

  const preActionCheck = (nextFn) => {
    if (allowExtraScripting) {
      nextFn();
    }
  };

  const resizeTextarea = () => {
    if (inputRef.current.scrollHeight > 120) {
      inputRef.current.style.height = 'auto';
      inputRef.current.style.height = `${inputRef.current.scrollHeight}px`;
    } else {
      inputRef.current.style.height = 'auto';
      inputRef.current.style.height = '120px';
    }
  };

  useEffect(() => {
    setAllowExtraScripting(loadExtraScriptingSettingFromStorage());
    setSendCountryInfo(loadDisplayCountrySettingFromStorage());
  }, []);

  useEffect(() => {
    saveExtraScriptingInfoSettingToStorage(allowExtraScripting);
  }, [allowExtraScripting]);

  useEffect(() => {
    saveDisplayCountryInfoSettingToStorage(sendCountryInfo);
  }, [sendCountryInfo]);

  useEffect(() => {
    inputRef.current.addEventListener('input', resizeTextarea, false);
    const oldInput = inputRef.current;
    resizeTextarea();

    return () => {
      oldInput.removeEventListener('input', resizeTextarea, false);
    };
  }, []);

  return (
    <StyledEditorBB>
      <textarea
        name="content"
        aria-label="Content"
        rows={1}
        ref={inputRef}
        value={content}
        onChange={updateContent}
        onKeyDown={(e) =>
          preActionCheck(() =>
            checkForHotkeys(
              e,
              content,
              setContent,
              selectionRange,
              setSelectionRange,
              inputRef.current,
              handleSubmit
            )
          )
        }
        onKeyUp={(e) => preActionCheck(() => setSelection(e))}
        onPaste={(e) => preActionCheck(() => handlePaste(e, setContent))}
        onClick={(e) => preActionCheck(() => setSelection(e))}
        style={{ height: '120px' }}
      />
      {showOptions && (
        <EditorOptions
          allowExtraScripting={allowExtraScripting}
          setAllowExtraScripting={setAllowExtraScripting}
          sendCountryInfo={sendCountryInfo}
          setSendCountryInfo={setSendCountryInfo}
          handleOptionsClose={() => setShowOptions(false)}
        />
      )}
      <div className="editor-footer">
        {allowExtraScripting ? (
          <EditorToolbar
            content={content}
            setContent={setContent}
            selectionRange={selectionRange}
            setSelectionRange={setSelectionRange}
            input={inputRef.current}
          />
        ) : (
          <div className="toolbar disabled">
            <button type="button">
              <Tooltip text="Extra scripting disabled!">
                <i className="fas fa-ghost" />
              </Tooltip>
            </button>
          </div>
        )}
        <div className="actions">
          <Link className="help-link" to="/knockoutbb" target="_blank">
            <i className="fas fa-question" />
            &nbsp;Formatting Help
          </Link>
          <button type="button" onClick={() => setShowOptions(!showOptions)}>
            <i className="fas fa-cog" />
            Settings
          </button>
          {children}
        </div>
      </div>
    </StyledEditorBB>
  );
};

EditorBB.propTypes = {
  content: PropTypes.string.isRequired,
  setContent: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func,
  children: PropTypes.element,
};

EditorBB.defaultProps = {
  handleSubmit: () => {},
  children: undefined,
};

export default EditorBB;
