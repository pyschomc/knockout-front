import styled from 'styled-components';
import { TabletMediaQuery } from '../SharedStyles';
import {
  ThemeMediumTextSize,
  ThemeSmallTextSize,
  ThemeVerticalHalfPadding,
  ThemeHorizontalHalfPadding,
  ThemePrimaryTextColor,
  ThemeSecondaryTextColor
} from '../../Theme';
import LinkBox from '../LinkBox';

export const ThreadTitle = styled.h3`
  color: ${ThemePrimaryTextColor};
  font-size: ${ThemeMediumTextSize};
  line-height: 1.4;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  padding: 0;
`;

export const ThreadSubtitle = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: ${props => (props.vertical ? 'column' : 'row')};
  text-align: ${props => props.textAlign || 'left'};
`;

export const ThreadAuthor = styled.span`
  color: ${ThemeSecondaryTextColor};
  font-size: ${ThemeSmallTextSize};
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  flex-shrink: 1;
  line-height: 1.4;
`;

export const ThreadPageCount = styled(ThreadAuthor)`
  font-size: ${ThemeSmallTextSize};
  padding-left: ${ThemeHorizontalHalfPadding};
  flex-shrink: 0;
`;

export const ThreadLink = styled(LinkBox)`
  flex-grow: 1;
  margin-top: ${ThemeVerticalHalfPadding};
  margin-bottom: ${ThemeVerticalHalfPadding};
  margin-left: ${ThemeHorizontalHalfPadding};
  margin-right: ${ThemeHorizontalHalfPadding};

  ${TabletMediaQuery} {
    margin-left: ${ThemeHorizontalHalfPadding};
    margin-right: ${ThemeHorizontalHalfPadding};
    margin-bottom: 0;
  }
`;
