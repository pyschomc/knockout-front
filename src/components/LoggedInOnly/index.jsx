import { connect } from 'react-redux';
import store from '../../state/configureStore';

const LoggedInOnly = props => {
  if (props.user.loggedIn && props.user.username) {
    return props.children;
  }
  return null;
};

export const isLoggedIn = () => store.getState().user.loggedIn;

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(LoggedInOnly);
