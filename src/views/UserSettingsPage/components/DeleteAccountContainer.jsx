import React from 'react';
import PropTypes from 'prop-types';
import { scrollToTop } from '../../../utils/pageScroll';
import { StyledDeleteAccountButton } from '../../../pages/UserSettings/components/DeleteAccount';

const DeleteAccountContainer = (props) => {
  return (
    <div className="panel">
      <h2 title="Delete Account">Delete Account</h2>
      <StyledDeleteAccountButton
        background="#ad1a1a"
        onClick={() => {
          props.showDeleteAccount(true);
          scrollToTop();
        }}
        type="button"
      >
        <i className="fas fa-bomb" />
        Delete Account
      </StyledDeleteAccountButton>
    </div>
  );
};

DeleteAccountContainer.propTypes = {
  showDeleteAccount: PropTypes.func,
};

DeleteAccountContainer.defaultProps = {
  showDeleteAccount: null,
};

export default DeleteAccountContainer;
