import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  ThemeKnockoutRed,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeBackgroundLighter,
} from '../../../utils/ThemeNew';

const PageSwitcher = ({ pageAmount, currentPage, nextPage, prevPage }) => {
  const pageList = [];
  for (let i = 1; i <= pageAmount; i += 1) {
    pageList.push(
      <li key={i} className={`${currentPage === i ? 'current' : ''}`}>
        ·
      </li>
    );
  }

  return (
    <StyledPageSwitcher>
      {currentPage !== 1 ? (
        <button onClick={prevPage} type="button">
          Back
        </button>
      ) : (
        <div className="spacer" />
      )}
      <ul>{pageList}</ul>
      {currentPage !== pageAmount ? (
        <button onClick={nextPage} type="button">
          Next
        </button>
      ) : (
        <div className="spacer" />
      )}
    </StyledPageSwitcher>
  );
};

PageSwitcher.propTypes = {
  pageAmount: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  nextPage: PropTypes.func.isRequired,
  prevPage: PropTypes.func.isRequired,
};

const StyledPageSwitcher = styled.div`
  margin-top: 80px;
  display: flex;
  justify-content: center;
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  ul {
    display: inline-block;
    list-style: none;
    margin: 0;
    padding: 0;
    li {
      display: inline-block;
      font-size: 100px;
      line-height: 0.5em;
      vertical-align: middle;
      color: ${ThemeBackgroundLighter};
      height: 56px;
      user-select: none;
      &.current {
        color: ${ThemeKnockoutRed};
      }
    }
  }
  button,
  div.spacer {
    width: 70px;
    margin: 0 ${ThemeHorizontalPadding};
  }
  button {
    position: relative;
    cursor: pointer;
    border: none;
    background-color: transparent;
    color: ${ThemeTextColor};
    &::before,
    &::after {
      content: '';
      position: absolute;
      bottom: 10px;
      width: 0px;
      height: 2px;
      background-color: ${ThemeKnockoutRed};
      transition: width 200ms;
    }
    &::before {
      right: 50%;
    }
    &::after {
      left: 50%;
    }
    &:hover,
    &:focus {
      &::before,
      &::after {
        width: 25%;
      }
    }
    &:focus {
      outline: none;
    }
  }
`;

export default PageSwitcher;
