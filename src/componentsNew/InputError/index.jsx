import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { transparentize } from 'polished';
import { ThemeKnockoutRed, ThemeFontSizeSmall, ThemeTextColor } from '../../utils/ThemeNew';

const StyledInputError = styled.div`
  box-sizing: border-box;
  width: 100%;
  padding: 0.4rem 0.8rem;
  border: 1px solid ${ThemeKnockoutRed};
  background: ${transparentize(0.9, ThemeKnockoutRed)};
  border-top: none;
  color: ${ThemeTextColor};
  font-size: ${ThemeFontSizeSmall};
`;

const InputError = ({ error }) => {
  if (error) {
    return <StyledInputError>{error}</StyledInputError>;
  }

  return null;
};

InputError.propTypes = {
  error: PropTypes.string,
};

InputError.defaultProps = {
  error: undefined,
};

export default InputError;
