/* eslint-disable react/display-name */
import React, { useState, forwardRef, useImperativeHandle } from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import EditorBB from '../EditorBB';

import { handleNewPostSubmit, handleEditPostSubmit } from './helpers';
import {
  loadPostContentsFromStorage,
  savePostContentsToStorage,
} from '../../utils/postEditorAutosave';

const PostEditor = forwardRef(({ threadId, initialContent, type, postId, postSubmitFn }, ref) => {
  const startingContent = initialContent || loadPostContentsFromStorage(threadId) || '';
  const [content, setContent] = useState(startingContent);
  const { id: subforumId } = useParams();

  const handleSubmit = () => {
    if (type === 'new') {
      handleNewPostSubmit(content, subforumId, threadId, postSubmitFn, setContent);
    } else if (type === 'edit') {
      handleEditPostSubmit(content, subforumId, threadId, postId, postSubmitFn);
    }
  };

  const handleSetContent = (value) => {
    setContent(value);
    if (type === 'new') {
      savePostContentsToStorage(threadId, value);
    }
  };

  useImperativeHandle(ref, () => ({
    appendToContent(text) {
      setContent(content + text);
    },
  }));

  return (
    <StyledPostEditor>
      <EditorBB content={content} setContent={handleSetContent} handleSubmit={handleSubmit}>
        <button type="submit" onClick={handleSubmit} title="Shortcut: Ctrl+Enter">
          <i className="fas fa-paper-plane" />
          Submit
        </button>
      </EditorBB>
    </StyledPostEditor>
  );
});

const StyledPostEditor = styled.div`
  position: relative;
`;

export default PostEditor;

PostEditor.propTypes = {
  threadId: PropTypes.number.isRequired,
  initialContent: PropTypes.string,
  type: PropTypes.string,
  postId: PropTypes.number,
  postSubmitFn: PropTypes.func,
};

PostEditor.defaultProps = {
  initialContent: undefined,
  type: 'new',
  postId: undefined,
  postSubmitFn: () => {},
};
