/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import {
  ThreadPageItem,
  ForumIconWrapper,
  ForumIconAlign,
  ThreadInfoWrapper,
  ThreadTitle,
  LockedIndicator,
  PinnedIndicator,
  ThreadPagination
} from '../../SubforumPage/components/style';
import ForumIcon from '../../../components/ForumIcon';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import PaginationNoLink from '../../../components/Pagination/PaginationNoLink';

class UserProfileLatestThreads extends React.Component {
  state = { open: false };

  handleToggleContent = key => {
    this.setState(prevState => ({ [key]: !prevState[key] }));
  };

  render() {
    const { threads, user, loading, pageChangeFn } = this.props;

    if (loading && threads.threads.length === 0) {
      return (
        <StyledLatestPosts>
          <i className="fas fa-truck-loading" />
          &nbsp;Loading...
        </StyledLatestPosts>
      );
    }

    return (
      <StyledLatestPosts>
        <PaginationNoLink
          pageChangeFn={pageChangeFn}
          totalPosts={threads.totalThreads}
          currentPage={threads.currentPage}
          pageSize={40}
          marginBottom
        />
        {threads.threads.map(thread => (
          <ThreadPageItem
            to={`/thread/${thread.id}/1`}
            key={thread.id + thread.title}
            locked={!!thread.locked}
            pinned={!!thread.pinned}
            deleted={!!thread.deleted_at}
          >
            <ForumIconWrapper>
              <Link to={`/thread/${thread.id}/1`}>
                <ForumIconAlign>
                  <ForumIcon iconId={thread.iconId} />
                </ForumIconAlign>
              </Link>
            </ForumIconWrapper>
            <ThreadInfoWrapper>
              <ThreadTitle to={`/thread/${thread.id}/1`}>
                {!!thread.locked && <LockedIndicator title="Locked!" className="fas fa-lock" />}
                {!!thread.pinned && (
                  <PinnedIndicator title="Pinned" className="fas fa-sticky-note" />
                )}
                <span>{thread.title}</span>
              </ThreadTitle>
              <ThreadPagination>
                <UserRoleWrapper user={user}>{user.username}</UserRoleWrapper>
              </ThreadPagination>
            </ThreadInfoWrapper>
          </ThreadPageItem>
        ))}
        <PaginationNoLink
          pageChangeFn={pageChangeFn}
          totalPosts={threads.totalThreads}
          currentPage={threads.currentPage}
          pageSize={40}
          marginBottom
        />
      </StyledLatestPosts>
    );
  }
}

UserProfileLatestThreads.propTypes = {
  threads: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  pageChangeFn: PropTypes.func.isRequired
};

export default UserProfileLatestThreads;

const StyledLatestPosts = styled.div``;
