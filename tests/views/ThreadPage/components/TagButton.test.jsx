import React from 'react';

import { render, fireEvent, act } from '@testing-library/react';
import axios from 'axios';
import TagButton from '../../../../src/views/ThreadPage/components/TagButton';

jest.mock('axios');

describe('TagButton component', () => {
  const tags = [
    { id: 1, name: 'NSFW' },
    { id: 2, name: 'Memes' },
    { id: 3, name: 'Politics' },
  ];
  const thread = { id: 0 };
  axios.get.mockResolvedValue({ data: tags });

  it('does not display the modal on render', () => {
    const { queryByText } = render(<TagButton thread={thread} />);
    expect(queryByText('Cancel')).toBeNull();
  });

  it('displays the modal', async () => {
    const { queryByText, queryByTitle } = render(<TagButton thread={thread} />);
    const tagButton = queryByTitle('Edit tags');
    expect(tagButton).not.toBeNull();
    await act(async () => {
      fireEvent.click(tagButton);
    });

    expect(queryByText('Cancel')).not.toBeNull();
  });

  it('correctly displays selected tags', async () => {
    const { queryByTitle, queryAllByTestId, getByTestId } = render(<TagButton thread={thread} />);
    const tagButton = queryByTitle('Edit tags');
    await act(async () => {
      fireEvent.click(tagButton);
    });

    const modalSelect = getByTestId('modal-select');
    await act(async () => {
      fireEvent.focus(modalSelect);
      fireEvent.keyDown(modalSelect, { key: 'ArrowDown', code: 40 });
      fireEvent.change(modalSelect, { target: { value: tags[0].id } });
    });

    let selectedTags = queryAllByTestId('selected-tag');
    expect(selectedTags).toHaveLength(1);
    await act(async () => {
      fireEvent.click(selectedTags[0]);
    });
    selectedTags = queryAllByTestId('selected-tag');
    expect(selectedTags).toHaveLength(0);
  });
});
