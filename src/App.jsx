import { loadProgressBar } from 'axios-progress-bar';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import { ThemeProvider } from 'styled-components';
import MainView from './MainView';
import MainViewNew from './views/MainView';
import {
  loadScaleFromStorage,
  loadThemeFromStorage,
  loadWidthFromStorage,
  loadPunchyLabsFromStorageBoolean,
} from './services/theme';
import store from './state/configureStore';

const labsEnabled = loadPunchyLabsFromStorageBoolean();

const MainViewComponent = labsEnabled ? MainViewNew : MainView;

// adds the axios progress bar to the global axios instance
loadProgressBar({
  parent: '#knockout-header',
});
toast.configure();

const App = () => (
  <Provider store={store}>
    <ThemeProvider
      theme={{
        mode: loadThemeFromStorage(),
        width: loadWidthFromStorage(),
        scale: loadScaleFromStorage(),
      }}
    >
      <BrowserRouter basename="/">
        <MainViewComponent />
      </BrowserRouter>
    </ThemeProvider>
  </Provider>
);

export default App;

ReactDOM.render(<App />, document.getElementById('app'));
