/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import ErrorBoundary from '../../components/ErrorBoundary/ErrorBoundary';
import LoggedInOnly from '../../components/LoggedInOnly';
import SelectModal from '../../components/Modals/SelectModal';
import Post from '../../components/Post';
import { PageTitle } from '../../components/SubHeader/style';
import { ThreadNewPost, ThreadPageWrapper, ThreadPostList } from './components/style';
import ThreadSubheader from './components/ThreadSubheader';
import PostEditorBB from '../../components/KnockoutBB/PostEditorBB';
import ForumIcon from '../../components/ForumIcon';

const ThreadPage = ({
  posts,
  thread,
  params,
  showingMoveModal,
  moveModalOptions,
  currentPage,
  togglePinned,
  toggleLocked,
  toggleDeleted,
  showMoveModal,
  deleteAlert,
  createAlert,
  quickReplyEditor,
  currentUserId,
  submitThreadMove,
  hideModal,
  refreshPosts,
  getEditor,
  goToPost,
  linkedPostId,
}) => (
  <>
    {showingMoveModal && (
      <SelectModal
        title="Move thread to subforum"
        options={moveModalOptions}
        submitFn={submitThreadMove}
        cancelFn={hideModal}
        iconUrl="https://img.icons8.com/color/96/000000/rearrange.png"
      />
    )}
    <div>
      <Helmet defer={false}>
        <title>
          {thread.title ? `${thread.title} - Knockout!` : 'Knockout - Loading thread...'}
        </title>
      </Helmet>
      <PageTitle>
        <ForumIcon iconId={thread.iconId} />
        {thread.title ? thread.title : 'Loading thread...'}
      </PageTitle>
      <ThreadSubheader
        thread={thread}
        params={params}
        currentPage={currentPage}
        togglePinned={togglePinned}
        toggleDeleted={toggleDeleted}
        toggleLocked={toggleLocked}
        currentUserId={currentUserId}
        showMoveModal={showMoveModal}
        createAlert={createAlert}
        deleteAlert={deleteAlert}
      />
      <ThreadPageWrapper>
        <ThreadPostList>
          {posts.map((post, index) => {
            return (
              <Post
                key={`${post.id}-${post.updatedAt}`}
                postDepth={index}
                totalPosts={posts.length}
                goToPost={goToPost}
                username={post.user.username}
                avatarUrl={post.user.avatar_url}
                userJoinDate={post.user.createdAt}
                threadPage={currentPage}
                threadId={thread.id}
                threadLocked={thread.locked}
                postId={post.id}
                postBody={post.content}
                postDate={post.createdAt}
                ratings={post.ratings}
                quickReplyEditor={quickReplyEditor}
                user={post.user}
                byCurrentUser={currentUserId === post.user.id}
                bans={post.bans}
                subforumName={thread.subforumName || 'Subforum'}
                isUnread={
                  new Date(thread.subscriptionLastSeen) < new Date(post.createdAt) ||
                  new Date(thread.readThreadLastSeen) < new Date(post.createdAt)
                }
                countryName={post.countryName}
                isLinkedPost={linkedPostId && Number(post.id) === Number(linkedPostId)}
              />
            );
          })}
        </ThreadPostList>
        {!thread.locked && !thread.deleted && (
          <LoggedInOnly>
            <ErrorBoundary errorMessage="The editor has crashed. Your post was saved a few seconds ago (hopefully). Reload the page.">
              <ThreadNewPost>
                <PostEditorBB
                  threadId={params.id}
                  subforumId={thread.subforumId}
                  callbackFn={refreshPosts}
                  getEditor={getEditor}
                  buttonLabel="Reply"
                  editable
                />
              </ThreadNewPost>
            </ErrorBoundary>
          </LoggedInOnly>
        )}
      </ThreadPageWrapper>
      <ThreadSubheader
        thread={thread}
        params={params}
        currentPage={currentPage}
        togglePinned={togglePinned}
        toggleDeleted={toggleDeleted}
        toggleLocked={toggleLocked}
        currentUserId={currentUserId}
        showMoveModal={showMoveModal}
        createAlert={createAlert}
        deleteAlert={deleteAlert}
      />
    </div>
  </>
);
ThreadPage.propTypes = {
  posts: PropTypes.array.isRequired,
  thread: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  showingMoveModal: PropTypes.bool.isRequired,
  moveModalOptions: PropTypes.array.isRequired,
  currentPage: PropTypes.number.isRequired,
  togglePinned: PropTypes.func.isRequired,
  toggleLocked: PropTypes.func.isRequired,
  toggleDeleted: PropTypes.func.isRequired,
  showMoveModal: PropTypes.func.isRequired,
  deleteAlert: PropTypes.func.isRequired,
  createAlert: PropTypes.func.isRequired,
  quickReplyEditor: PropTypes.any,
  currentUserId: PropTypes.number.isRequired,
  submitThreadMove: PropTypes.func.isRequired,
  hideModal: PropTypes.func.isRequired,
  refreshPosts: PropTypes.func.isRequired,
  getEditor: PropTypes.func.isRequired,
  goToPost: PropTypes.func.isRequired,
  linkedPostId: PropTypes.number.isRequired,
};
ThreadPage.defaultProps = {
  quickReplyEditor: undefined,
};

export default ThreadPage;
