import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import config from '../../../config';
import { getUser, getUserBans, getUserProfile, getUserTopRatings } from '../../services/user';
import { updateBackgroundRequest } from '../../state/background';
import UserProfileComponent from './UserProfileComponent';

class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.profileSwitch = React.createRef();

    this.state = {
      user: null,
      editingProfile: false,
    };
  }

  async componentDidMount() {
    if (!this.props.match.params.id) {
      return;
    }
    this.gatherData(this.props.match.params.id);
  }

  componentDidUpdate() {
    if (!this.props.match.params.id) {
      return;
    }

    // i wish we had typescript
    // one is a string, the other is a number.
    if (this.state.user && `${this.props.match.params.id}` !== `${this.state.user.id}`) {
      this.gatherData(this.props.match.params.id);
      this.profileSwitch.current.handleTabClick(-1);
    }
  }

  componentWillUnmount() {
    this.props.updateBackground(null);
  }

  async gatherData(id) {
    const user = await getUser(id);
    const bans = await getUserBans(id);
    const topRatings = await getUserTopRatings(id);
    const userProfile = await getUserProfile(id);

    this.setState({ user, bans, topRatings, userProfile }, () => {
      this.handleBackgroundUpdate(userProfile);
    });
  }

  handleBackgroundUpdate(userProfile) {
    if (userProfile.backgroundUrl) {
      const url = `${config.cdnHost}/image/${userProfile.backgroundUrl}`;
      return this.props.updateBackground(url, userProfile.backgroundType);
    }
    return this.props.updateBackground(null);
  }

  handleProfileEditorVisibility(newStatus) {
    this.setState({ editingProfile: newStatus });
  }

  render() {
    if (!this.state.user) {
      return '';
    }

    const { user, bans, userProfile, editingProfile, topRatings = [] } = this.state;
    const { currentUser } = this.props;

    const canEditProfile = user.id === currentUser.id && currentUser.usergroup !== 1;

    return (
      <UserProfileComponent
        user={user}
        bans={bans}
        userProfile={userProfile}
        topRatings={topRatings}
        canEditProfile={canEditProfile}
        showProfileEditor={() => this.handleProfileEditorVisibility(true)}
        hideProfileEditor={() => this.handleProfileEditorVisibility(false)}
        editingProfile={editingProfile}
        profileSwitch={this.profileSwitch}
      />
    );
  }
}

UserProfile.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  updateBackground: PropTypes.func.isRequired,
  currentUser: PropTypes.shape({
    id: PropTypes.number.isRequired,
    usergroup: PropTypes.number.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ user, background }) => ({
  currentUser: user,
  background,
});
const mapDispatchToProps = (dispatch) => ({
  updateBackground: (value, bgType) => dispatch(updateBackgroundRequest(value, bgType)),
});
export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
