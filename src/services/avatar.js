/* eslint-disable prettier/prettier */
import { authPost } from './common';

export const uploadAvatar = async file => {
  try {
    const formData = new FormData();
    const imageFile = file;
    formData.append('image', imageFile);

    const requestBody = formData;

    const response = await authPost({
      url: '/avatar',
      data: requestBody,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
    if (response.status !== 201 || !response.data.message) {
      return new Error('Upload failed.');
    }
    return response.data;
  } catch (err) {
    throw new Error(err);
  }
};

export const uploadBackground = async file => {
  try {
    const formData = new FormData();
    const imageFile = file;
    formData.append('image', imageFile);

    const requestBody = formData;

    const response = await authPost({
      url: '/background',
      data: requestBody,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
    if (response.status !== 201 || !response.data.message) {
      return new Error('Upload failed.');
    }
    return response.data;
  } catch (err) {
    throw new Error(err);
  }
};

export default uploadAvatar;
