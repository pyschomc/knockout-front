import { authPut } from './common';

const submitRating = ({ postId, rating }) => {
  const requestBody = { postId, rating };
  return authPut({ url: '/rating', data: requestBody });
};

export default submitRating;
