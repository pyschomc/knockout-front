import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import PropTypes from 'prop-types';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { filterNsfwThreads } from '../../../utils/filterNsfwThreads';

dayjs.extend(relativeTime);

const defaultColors = [
  ['#ff9e39', '#f16d0f'], // general
  ['#80d8ff', '#b53fb1'], // videos
  ['#1bffb1', '#2dbbff'], // gaming
  ['#18caff', '#1aa0ff'], // game generals
  ['#3cff18', '#ffce1a'], // progress report
  ['#4cafe8', '#3b5592'], // developers
  ['#ff7c55', '#f15a2a'], // news
  ['#af5c2d', '#753b24'], // politics
  ['#fff94a', '#669529'], // h&s
  ['#19d0a6', '#2599a5'], // film tv
  ['#fb2e24', '#ae1c1c'], // meta
  ['#da46ff', '#da2361'], // creativity
  ['#ab7977', '#bd4045'], // facepunch
];

const defaultColor = (index) => defaultColors[index % defaultColors.length];

const buildLastPost = (lastPost, threadTitle, subforumId, threadIsNsfw) => {
  if (threadIsNsfw) {
    return (
      <Link to={`/subforum/${subforumId}`} title="Go to subforum">
        <p className="thread-title">NSFW thread!</p>
        <p className="second-row">
          <span className="dim">Hidden due to your settings</span>

          <span className="go-to-page">Go to subforum ➤</span>
        </p>
      </Link>
    );
  }

  return (
    <Link to={`/thread/${lastPost.thread.id}/${lastPost.thread.lastPost.page}#post-${lastPost.id}`}>
      <p className="thread-title">{threadTitle}</p>
      <p className="second-row">
        <span className="dim">Last post by</span>
        <span> </span>
        <UserRoleWrapper user={lastPost.user}>{lastPost.user.username}</UserRoleWrapper>
        <span> </span>
        <span className="dim">{dayjs(lastPost.createdAt).fromNow()}</span>
        <span className="go-to-page">
          Go to page
          <span> </span>
          {lastPost.thread.lastPost.page}
          <span> ➤</span>
        </span>
      </p>
    </Link>
  );
};

const SubforumItem = ({
  index,
  id,
  name,
  description,
  icon,
  lastPost,
  totalPosts,
  totalThreads,
  nsfwFilterEnabled,
}) => {
  const threadIsNsfw =
    nsfwFilterEnabled && lastPost?.thread && filterNsfwThreads([lastPost.thread]).length === 0;
  const threadTitle = lastPost?.thread.title || 'No thread';

  const totalPostsAriaLabel = `Total posts in ${name}`;
  const totalThreadsAriaLabel = `Total threads in ${name}`;

  const totalPostsLocalised = Number(totalPosts).toLocaleString();
  const totalThreadsLocalised = Number(totalThreads).toLocaleString();

  return (
    <SubforumItemWrapper icon={icon} gradient={defaultColor(index)}>
      <Link to={`/subforum/${id}`} className="title-stats" title={description}>
        <p className="sf-name">{name}</p>

        <div className="stats">
          <span aria-label={totalThreadsAriaLabel} title="Total threads">
            <i className="fas fa-comment-dots" />
            <span> </span>
            {totalThreadsLocalised}
          </span>
          <span aria-label={totalPostsAriaLabel} title="Total posts">
            <i className="fas fa-reply-all" />
            <span> </span>
            {totalPostsLocalised}
          </span>
        </div>
      </Link>
      <div className="thread-info">
        {lastPost && buildLastPost(lastPost, threadTitle, id, threadIsNsfw)}
      </div>
    </SubforumItemWrapper>
  );
};

SubforumItem.propTypes = {
  description: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  lastPost: PropTypes.objectOf(PropTypes.any).isRequired,
  name: PropTypes.string.isRequired,
  nsfwFilterEnabled: PropTypes.bool.isRequired,
  totalPosts: PropTypes.number.isRequired,
  totalThreads: PropTypes.number.isRequired,
};

export default SubforumItem;

const SubforumItemWrapper = styled.div`
  display: grid;
  grid-template-rows: auto auto;

  height: 110px;
  background: ${ThemeBackgroundLighter};

  .title-stats p.sf-name,
  .thread-info p.thread-title {
    margin: 0 0 calc(${ThemeVerticalPadding} / 2) 0;
  }

  .title-stats {
    align-items: flex-start;
    display: flex;
    flex-direction: column;
    justify-content: center;
    position: relative;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    font-weight: bold;

    background-image: url(${(props) => props.icon});
    background-repeat: no-repeat;
    background-position: calc(100% - ${ThemeHorizontalPadding}) center;
    background-size: auto 40px;

    &:after {
      content: '';
      display: block;
      position: absolute;
      left: 0;
      right: 0;
      bottom: 0;
      width: 100%;
      height: 1px;
      background: ${(props) => `
        rgba(0, 0, 0, 0) linear-gradient(270deg, ${props.gradient[0]} 0%, ${props.gradient[1]} 100%) repeat scroll 0% 0%
      `};
    }

    p {
      padding: 0 0 2px 0;
    }

    p,
    a,
    div {
      width: calc(100% - 60px);
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    &:hover {
      cursor: pointer;
      filter: brightness(1.05);

      p {
        text-decoration: underline;
      }
    }

    .stats {
      font-size: ${ThemeFontSizeSmall};
      font-weight: normal;
      opacity: 0.5;
      overflow: hidden;

      > span:not(:first-child) {
        padding-left: ${ThemeHorizontalPadding};
      }
    }
  }

  .thread-info {
    background: ${ThemeBackgroundDarker};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    position: relative;
    display: grid;

    a {
      overflow: hidden;
    }

    p {
      margin: 0;
      width: 100%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;

      &.thread-title {
        line-height: 1.3em; /* Fixes "overflow: hidden" cutting the top and bottoms of certain tall characters */
      }

      &:last-child {
        font-size: ${ThemeFontSizeSmall};

        span.dim {
          opacity: 0.5;
        }
      }
    }

    p.second-row {
      position: absolute;
      bottom: ${ThemeVerticalPadding};
      left: 0;
      height: calc(${ThemeFontSizeSmall} + 2px);
      padding: 0 ${ThemeHorizontalPadding};
      box-sizing: border-box;
    }
    .go-to-page {
      position: absolute;
      right: ${ThemeHorizontalPadding};
      opacity: 0.5;
    }

    &:hover {
      filter: brightness(1.05);
      text-decoration: underline;
      cursor: pointer;
    }
  }
`;
