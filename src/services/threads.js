import axios from 'axios';

import config from '../../config';

import { authPost, authGet, authPut } from './common';
import { pushSmartNotification } from '../utils/notification';

export const getThreadList = async () => {
  const res = await axios.get(`${config.apiHost}/thread`);

  const { data } = res;

  return data;
};

export const getThread = async (threadId) => {
  const res = await axios.get(`${config.apiHost}/thread/${threadId}`, {
    headers: { 'Cache-Control': 'private, no-store, max-age=0' },
  });

  const { data } = res;

  return data;
};

export const getThreadWithPosts = async (threadId, page = 1) => {
  const user = localStorage.getItem('currentUser');

  // if logged in, get the subforum with auth so
  // subscribed threads are fetched
  if (user) {
    const res = await authGet({
      url: `/thread/${threadId}/${page}`,
      headers: { 'Cache-Control': 'private, no-store, max-age=0' },
    });

    const { data } = res;

    return data;
  }
  try {
    const res = await axios.get(`${config.apiHost}/thread/${threadId}/${page}`);

    const { data } = res;

    if (res.status !== 200) {
      return {
        title: 'Thread not found',
      };
    }

    return data;
  } catch (err) {
    return {
      title: 'Thread not found',
      posts: [],
      subforumId: 1,
    };
  }
};

export const createNewThread = async (data) => {
  try {
    if (data.title.length < 3) {
      pushSmartNotification({ error: 'Title too short.' });
      return new Error('Title too short.');
    }
    if (data.title.length > 140) {
      pushSmartNotification({ error: 'Title too long.' });
      return new Error('Title too long.');
    }
    if (data.title.length > 140) {
      pushSmartNotification({ error: 'Title too long.' });
      throw new Error('Title too long.');
    }

    const contentSerialized = data.content;
    const contentLength = contentSerialized.trim().length;

    const contentStringified = data.content;

    if (contentLength > 8000) {
      pushSmartNotification({ error: `Post body too long. ${contentLength}/4000` });
      throw new Error('Post body too long.');
    }
    if (contentLength < 1) {
      pushSmartNotification({ error: `Post body too short. ${contentLength}/10` });
      throw new Error('Post body too short.');
    }

    const thread = await authPost({
      url: '/thread',
      data: { ...data, content: contentStringified, postContent: data.content },
    });

    return thread.data;
  } catch (error) {
    console.log(error);
    return {};
  }
};

export const updateThread = async (data) => {
  // add to this so only the fields we want can be submitted
  if (!data.title && !data.subforum_id && data.backgroundUrl == null) {
    pushSmartNotification({ error: 'No valid update data provided.' });
    throw new Error({ error: 'No valid update data provided.' });
  }

  if (data.title && (data.title.length < 3 || data.title.length > 140 || data.title === '')) {
    pushSmartNotification({ error: 'Invalid title.' });
    throw new Error({ error: 'Invalid title.' });
  }

  const thread = await authPut({ url: '/thread', data });

  return thread.data;
};
