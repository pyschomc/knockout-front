/* eslint-disable jsx-a11y/label-has-for */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import OptionsStorageInput from './OptionsStorageInput';
import ServerSideInput from './ServerSideInput';
import { loadOptions, saveValue, widthKey } from '../../../utils/postOptionsStorage';
import Tooltip from '../../../componentsNew/Tooltip';

import { getProfileRatingsDisplay, updateProfileRatingsDisplay } from '../../../services/user';
import { updateHeader } from '../../../state/style';

export const autoSubscribeKey = 'autoSubscribe';
export const ratingsXrayKey = 'ratingsXray';
export const stickyHeaderKey = 'stickyHeader';
export const punchyLabsKey = 'punchyLabs';
export const displayCountryInfoKey = 'displayCountryInfo';
export const nsfwFilterKey = 'nsfwFilter';

const defaultOptions = loadOptions();

const ThemeInputContainer = (props) => {
  const { label } = props;
  const { value } = props;
  const { options } = props;
  const { tooltip } = props;
  return (
    <Tooltip text={tooltip}>
      <div className="input-wrapper">
        <div className="dropdown">
          <label>{label}</label>
          <select onChange={(e) => props.update(e.target.value)} defaultValue={value}>
            {options.map((option) => {
              return (
                <option key={option.key} value={option.key}>
                  {option.label}
                </option>
              );
            })}
          </select>
        </div>
      </div>
    </Tooltip>
  );
};

ThemeInputContainer.propTypes = {
  update: PropTypes.func,
  value: PropTypes.string,
  label: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.any),
  tooltip: PropTypes.string,
};

ThemeInputContainer.defaultProps = {
  update: null,
  value: '',
  label: '',
  options: null,
  tooltip: '',
};

const UserExperienceContainer = () => {
  const [width, setWidth] = useState(defaultOptions.width);

  const dispatch = useDispatch();

  const updateWidth = (widthValue) => {
    setWidth(widthValue);
    saveValue(widthKey, widthValue);
    window.location.reload();
  };

  return (
    <div className="panel">
      <h2 title="Because you're important to us <3">User Experience</h2>
      <div className="options-wrapper">
        <OptionsStorageInput
          tooltip="Subscribe to threads on reply / on creation"
          label="AutoSub™"
          storageKey={autoSubscribeKey}
          defaultValue={false}
        />

        <OptionsStorageInput
          tooltip="Sticky header that's always with you as you scroll"
          label="StickyHeader™"
          storageKey={stickyHeaderKey}
          defaultValue
          onChange={(value) => dispatch(updateHeader(value))}
        />

        <OptionsStorageInput
          tooltip="Show country on posts"
          label="FlagPunchy™"
          storageKey={displayCountryInfoKey}
          defaultValue
        />

        <OptionsStorageInput
          tooltip="Experimental and buggy settings"
          label="PunchyLabs™"
          storageKey={punchyLabsKey}
          defaultValue={false}
          onChange={() => window.location.reload()}
        />

        <OptionsStorageInput
          tooltip="Hides NSFW threads"
          label="WorkSafe™"
          storageKey={nsfwFilterKey}
          defaultValue
        />

        <OptionsStorageInput
          tooltip="See who left that *dumb* rating"
          label="Ratings X-ray™"
          storageKey={ratingsXrayKey}
          defaultValue
        />

        <ServerSideInput
          tooltip="Hide ratings on your profile"
          label="BoxHide™"
          setValue={updateProfileRatingsDisplay}
          getValue={getProfileRatingsDisplay}
        />

        <ThemeInputContainer
          label="Width"
          tooltip="Width of the middle section"
          update={updateWidth}
          options={[
            { key: 'full', label: 'Full' },
            { key: 'verywide', label: 'Very Wide' },
            { key: 'wide', label: 'Wide' },
            { key: 'medium', label: 'Medium' },
            { key: 'narrow', label: 'Narrow' },
          ]}
          value={width}
        />
      </div>
    </div>
  );
};
export default UserExperienceContainer;
