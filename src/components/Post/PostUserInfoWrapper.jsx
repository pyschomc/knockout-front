/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';

import config from '../../../config';

import UserRoleWrapper from '../UserRoleWrapper';
import UserAvatar from '../UserAvatar';

import {
  UserInfoWrapper,
  UserInfo,
  UserName,
  UserJoinDate,
  UserInfoBackgroundImage,
  BackgroundTopOverlay,
  BackgroundBottomOverlay
} from './style';

const PostUserInfoWrapper = ({
  isUnread,
  byCurrentUser,
  user,
  userJoinDateLong,
  userJoinDateShort,
  isLinkedPost,
  userCakeDay
}) => (
  <UserInfoWrapper isUnread={isUnread} byCurrentUser={byCurrentUser} isLinkedPost={isLinkedPost}>
    <UserInfo to={`/user/${user.id}`}>
      <UserAvatar user={user} />

      <UserRoleWrapper user={user}>
        <UserName>{user.username}</UserName>
      </UserRoleWrapper>

      <UserJoinDate title={`Joined ${userJoinDateLong}`}>
        {userJoinDateShort}

        {userCakeDay && ' 🍰'}
      </UserJoinDate>
    </UserInfo>
    <UserInfoBackgroundImage backgroundUrl={`${config.cdnHost}/image/${user.backgroundUrl}`}>
      <BackgroundTopOverlay />
      <BackgroundBottomOverlay />
    </UserInfoBackgroundImage>
  </UserInfoWrapper>
);

PostUserInfoWrapper.propTypes = {
  byCurrentUser: PropTypes.bool,
  isUnread: PropTypes.bool,
  user: PropTypes.object.isRequired,
  userJoinDateLong: PropTypes.string.isRequired,
  userJoinDateShort: PropTypes.string.isRequired,
  isLinkedPost: PropTypes.bool
};
PostUserInfoWrapper.defaultProps = {
  byCurrentUser: false,
  isUnread: false,
  isLinkedPost: false
};

export default PostUserInfoWrapper;
