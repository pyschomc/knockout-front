const adsList = [
  {
    query: 'intitle:"3D Printer Thread"',
    image: '3dprinter.jpg',
  },
  {
    query: 'intitle:"Linux Discussion"',
    image: 'linux.png',
  },
  {
    query: 'intitle:"Fast Anime"',
    image: 'fastanime.png',
  },
  {
    query: 'intitle:"Jabroni Brawl 3"',
    image: 'jabronibrawl.png',
  },
  {
    query: 'intitle:"LMAO Pics"',
    image: 'lmaopics.jpg',
  },
  {
    query: 'intitle:"LMAO Pics"',
    image: 'lmaopics2.png',
  },
  {
    query: 'intitle:"Red Letter Media"',
    image: 'rlm.png',
  },
  {
    query: 'intitle:"Webcomics General"',
    image: 'webcomics.png',
  },
  {
    query: 'intitle:"ULTRAKILL "',
    image: 'ultrakill.png',
  },
  {
    query: 'intitle:"Knockout Anonymous Confessional"',
    image: 'anonconfess.png',
  },
  {
    query: 'intitle:"Election Toxx thread"',
    image: 'toxx.png',
  },
  {
    query: 'intitle:"General 3D Thread"',
    image: '3dg.png',
  },
  {
    query: 'intitle:"Retro FPS Games Thread"',
    image: 'retrofps.png',
  },
  {
    query: 'intitle:"What about YOUR videos?"',
    image: 'postyourvideos.png',
  },
  {
    query: 'intitle:"Titanfall "',
    image: 'titanfall.png',
  },
  {
    query: 'intitle:"Half-Life General"',
    image: 'halflife.png',
  },
  {
    query: 'intitle:"Team Fortress 2 Megathread"',
    image: 'tf2.png',
  },

  {
    query: 'intitle:"Automotive Thread "',
    image: 'autothread.png',
  },
  {
    query: 'intitle:"Birb Megathread"',
    image: 'birb.gif',
  },
  {
    query: 'intitle:"Black Mesa"',
    image: 'blackmesageneral.png',
  },
  {
    query: 'intitle:"Command & Conquer"',
    image: 'cncgeneral.png',
  },
  {
    query: 'intitle:"Let\'s Draw Each other!"',
    image: 'draweachother.gif',
  },
  {
    query: 'intitle:"Draw The Cutest Character You Can"',
    image: 'drawthecutest.gif',
  },
  {
    query: 'intitle:"Firearms General"',
    image: 'firearmsgeneral.gif',
  },
  {
    query: 'intitle:"Gardening Hobby Thread"',
    image: 'gardening.gif',
  },
  {
    query: 'intitle:"Creepy as hell gmod video"',
    image: 'gmodvideos.png',
  },
  {
    query: 'intitle:"Halo"',
    image: 'halogeneral.gif',
  },
  {
    query: 'intitle:"Investments and Stocks Discussion Thread"',
    image: 'investmentstocks.png',
  },
  {
    query: 'intitle:"Knockout Folding@home"',
    image: 'kofah.png',
  },
  {
    query: 'intitle:"Neato Obscure Websites"',
    image: 'neatoobscurewebsites.gif',
  },
  {
    query: 'intitle:"Patch and Pin General"',
    image: 'patchnpins.png',
  },
  {
    query: 'intitle:"Persona Series General"',
    image: 'personageneral.gif',
  },
  {
    query: 'intitle:"Physical Media Thread"',
    image: 'physicalmedia.png',
  },
  {
    query: 'intitle:"Rocket League"',
    image: 'rocketleaguegeneral.jpg',
  },
  {
    query: 'intitle:"CIPWTTKT&GC"',
    image: 'sip.png',
  },
  {
    query: 'intitle:"Smash Bros megathread"',
    image: 'ssbgeneral.png',
  },
  {
    query: 'intitle:"Star Wars Gaming Megathread"',
    image: 'starwarsgeneral.png',
  },
  {
    query: 'intitle:"Terraria Thread"',
    image: 'terrariageneral.gif',
  },
  {
    query: 'intitle:"Unpopular Opinion Thread"',
    image: 'unpopularopinions.png',
  },
  {
    query: 'intitle:"Virtual Reality Megathread"',
    image: 'virtualrealitygeneral.png',
  },
  {
    query: 'intitle:"Webcomics General"',
    image: 'webcomics2.png',
  },
  {
    query: 'intitle:"WAYT are you Thinking"',
    image: 'whatareyouthinking.gif',
  },
  {
    query: 'intitle:"Woodworking Hobby Thread"',
    image: 'woodworkinghobby.png',
  },
  {
    query: 'intitle:"XCOM Thread"',
    image: 'xcomgeneral.gif',
  },
  {
    query: 'intitle:"Yakuza Megathread"',
    image: 'yakuzageneral.gif',
  },
];

const getAdItem = () => adsList[Math.floor(Math.random() * adsList.length)];

export default getAdItem;
