import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '../Tooltip';
import submitReport from '../../services/reports';
import { PostReplyToButton, PostReplyToButtonIcon } from './style';

const handleReport = async (postId) => {
  const reportReason = window.prompt('Why are you reporting this post? (describe rules broken)');
  if (!reportReason || reportReason.length < 3) return;

  await submitReport({ postId, reportReason });
};

const ReportButton = ({ postId }) => (
  <Tooltip text="Report" top={false}>
    <PostReplyToButton square onClick={() => handleReport(postId)} title="Report">
      <PostReplyToButtonIcon className="fas fa-exclamation-triangle" />
    </PostReplyToButton>
  </Tooltip>
);

ReportButton.propTypes = {
  postId: PropTypes.number.isRequired,
};

export default ReportButton;
