/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';

import { createNewThread } from '../../services/threads';

import IconGrid from '../../components/IconGrid';

import {
  SubHeader,
  SubHeaderBackAndTitle,
  SubHeaderTitle,
  SubHeaderBack
} from '../../components/SubHeader/style';
import ThreadCreationNotice from './components/ThreadCreationNotice';

import {
  StyledThreadCreation,
  BackgroundSizeOptionsWrapper,
  StyledBgSizeLabel
} from './components/style';
import Tooltip from '../../components/Tooltip';
import UserGroupRestricted from '../../components/UserGroupRestricted';
import ThreadCreationEditorBB from './components/ThreadCreationEditorBB';

const ThreadCreationComponent = ({
  threadTitle,
  handleTitleChange,
  icons,
  handleBackgroundChange,
  handleBackgroundTypeChange,
  backgroundType,
  backgroundUrl,
  refreshPosts,
  getEditor,
  iconId,
  content,
  handleIconChange,
  tags,
  handleTagAdd,
  handleTagRemove,
  currentTag,
  selectedTags,
  getTagName
}) => (
  <div>
    <SubHeader>
      <SubHeaderBackAndTitle>
        <SubHeaderBack to="/">&#8249;</SubHeaderBack>
        <SubHeaderTitle>Create new Thread</SubHeaderTitle>
      </SubHeaderBackAndTitle>
    </SubHeader>
    <ThreadCreationNotice />
    <StyledThreadCreation>
      <div className="input tall">
        <label className="inputLabel">Icon</label>
        <IconGrid icons={icons} selectedId={iconId} onChange={handleIconChange} />
      </div>
      <div className="input">
        <label className="inputLabel">Title</label>
        <input type="text" name="title" placeholder="Internet Drama" onChange={handleTitleChange} />
      </div>

      <UserGroupRestricted userGroupIds={[2, 3, 4, 5]}>
        <div className="line-option-wrapper">
          <div className="input tall">
            <Tooltip fullWidth text="You will get absolutely BTFO if you abuse this!">
              <div className="tooltip-content">
                <label className="inputLabel">Thread background URL (optional)</label>
                <input
                  type="text"
                  name="title"
                  placeholder="https://onlinewebimages.com/i/epic_image.png"
                  onChange={handleBackgroundChange}
                  value={backgroundUrl}
                />
              </div>
            </Tooltip>
          </div>

          <div className="input">
            <label className="inputLabel">Thread background type</label>
            <BackgroundSizeOptionsWrapper>
              <input
                type="radio"
                value="cover"
                id="cover"
                checked={backgroundType === 'cover'}
                onChange={handleBackgroundTypeChange}
                hidden
              />
              <Tooltip text="Cover (fills the whole page in the background)">
                <StyledBgSizeLabel active={backgroundType === 'cover'} htmlFor="cover">
                  <i className="fas fa-expand-arrows-alt" />
                </StyledBgSizeLabel>
              </Tooltip>
              <input
                type="radio"
                value="tiled"
                id="tiled"
                checked={backgroundType === 'tiled'}
                onChange={handleBackgroundTypeChange}
                hidden
              />
              <Tooltip text="Tiled (best for small images, gets repeated in the background)">
                <StyledBgSizeLabel active={backgroundType === 'tiled'} htmlFor="tiled">
                  <i className="fas fa-puzzle-piece" />
                </StyledBgSizeLabel>
              </Tooltip>
            </BackgroundSizeOptionsWrapper>
          </div>
        </div>
      </UserGroupRestricted>

      <div className="line-option-wrapper">
        <div className="input tall tags-list">
          <label className="inputLabel" htmlFor="tags">
            Tags
          </label>

          <select id="tag-list" onChange={handleTagAdd} value={currentTag}>
            <option value="default">Select a tag...</option>
            {tags.map(tag => (
              <option
                key={tag}
                value={`${tag.id}|${tag.name}`}
                disabled={selectedTags.includes(`${tag.id}|${tag.name}`)}
              >
                {tag.name}
              </option>
            ))}
          </select>
        </div>
        <div className="input">
          <label className="inputLabel">Selected Tags</label>
          <div className="selected-tags">
            {selectedTags.map((tag, index) => {
              return (
                <button
                  key={tag}
                  type="button"
                  className="tag-item"
                  onClick={() => handleTagRemove(index)}
                >
                  <i className="fas fa-times-circle" />
                  &nbsp;
                  {getTagName(tag)}
                </button>
              );
            })}
          </div>
        </div>
      </div>

      <div className="editor">
        <ThreadCreationEditorBB
          callbackFn={refreshPosts}
          getEditor={getEditor}
          buttonLabel="Reply"
          createNewThread={createNewThread}
          threadTitle={threadTitle}
          iconId={iconId}
          content={content}
          backgroundType={backgroundType}
          backgroundUrl={backgroundUrl}
          selectedTags={selectedTags}
          editable
        />
      </div>
    </StyledThreadCreation>
  </div>
);
ThreadCreationComponent.propTypes = {
  threadTitle: PropTypes.string.isRequired,
  backgroundType: PropTypes.string.isRequired,
  iconId: PropTypes.number.isRequired,
  content: PropTypes.string.isRequired,
  icons: PropTypes.array,
  backgroundUrl: PropTypes.string,
  getEditor: PropTypes.func,
  refreshPosts: PropTypes.func,
  handleTitleChange: PropTypes.func.isRequired,
  handleBackgroundChange: PropTypes.func.isRequired,
  handleBackgroundTypeChange: PropTypes.func.isRequired,
  handleIconChange: PropTypes.func.isRequired
};
ThreadCreationComponent.defaultProps = {
  icons: undefined,
  backgroundUrl: undefined,
  getEditor: undefined,
  refreshPosts: undefined
};

export default ThreadCreationComponent;
