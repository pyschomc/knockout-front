/* eslint-disable react/forbid-prop-types */
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import config from '../../../config';
import PostEditor from '../PostEditor';
import { getPost } from '../../services/posts';
import { loadRatingsXrayFromStorageBoolean } from '../../services/theme';
import humanizeDuration from '../../utils/humanizeDuration';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHighlightStronger,
  ThemeHighlightWeaker,
  ThemeHorizontalPadding,
  ThemeKnockoutRed,
  ThemeVerticalPadding,
  ThemePostLineHeight,
} from '../../utils/ThemeNew';
import RatingBar from '../Rating/RatingBar';
import UserRoleWrapper from '../UserRoleWrapper';
import PostControls from './components/PostControls';
import ParseBB from '../KnockoutBB/Parser';
import ErrorBoundary from '../ErrorBoundary';
import { DesktopMediaQuery } from '../../components/SharedStyles';

dayjs.extend(relativeTime);

const Post = ({
  userJoinDate,
  postBody,
  postDate,
  postId,
  user,
  byCurrentUser,
  bans,
  threadId,
  threadLocked,
  postPage,
  isUnread,
  hideUserWrapper,
  hideControls,
  subforumName,
  countryName,
  isLinkedPost,
  ratings,
  threadPage,
  subforumId,
  handleReplyClick,
  postSubmitFn,
}) => {
  const ratingsXray = loadRatingsXrayFromStorageBoolean();

  const [showBBCode, setShowBBCode] = useState(false);
  const [ratingsState, setRatingsState] = useState(ratings);
  const [editing, setEditing] = useState(false);

  const refreshPost = async (id) => {
    const updatedPost = (await getPost(id)).data;

    setRatingsState(updatedPost.ratings);
    setEditing(false);
  };

  const toggleShowBBCode = () => setShowBBCode(!showBBCode);

  const replyToPost = () => {
    let quotePyramidsRemovedString = `${postBody}`;

    const extractQuotes = () => {
      quotePyramidsRemovedString = quotePyramidsRemovedString.replace(
        /\[quote.*[\s\S]*\[\/quote]/gi,
        ''
      );

      if (/\[quote.*[\s\S]*\[\/quote]/gi.test(quotePyramidsRemovedString)) {
        extractQuotes(quotePyramidsRemovedString);
      }
    };

    try {
      extractQuotes();
    } catch (error) {
      console.error('Could not remove quotes.');

      quotePyramidsRemovedString = postBody;
    }
    const content = `[quote mentionsUser="${
      user.id
    }" postId="${postId}" threadPage="${threadPage}" threadId="${threadId}" username="${
      user.username
    }"]${quotePyramidsRemovedString.trim()}[/quote]\n\n`;

    handleReplyClick(content);

    window.scrollTo(0, document.body.scrollHeight);
  };

  const userJoinDateShort = dayjs(userJoinDate).format('MMM YYYY');
  const userJoinDateLong = dayjs(userJoinDate).format('DD/MM/YYYY');
  const userCakeDay = dayjs(userJoinDate).format('DD/MM') === dayjs(new Date()).format('DD/MM');
  const postDateHuman = dayjs().isBefore(dayjs(postDate)) ? 'Now' : dayjs().to(dayjs(postDate));
  const { isBanned, avatar_url: avatarUrl, backgroundUrl, username } = user;
  const hasAvatar = avatarUrl && avatarUrl.length !== 0 && !avatarUrl.includes('none.webp');
  const bgUrl = backgroundUrl ? `${config.cdnHost}/image/${backgroundUrl}` : undefined;
  let url = `${config.cdnHost}/image/${avatarUrl}`;
  let title = `${username}'s avatar`;

  const postIndicatorIcons = (() => {
    return (
      <>
        {byCurrentUser && (
          <span className="post-indicator is-self">
            <i className="fas fa-box-open" />
            &nbsp;You
          </span>
        )}
        {isLinkedPost && (
          <span className="post-indicator is-linked">
            <i className="fas fa-hashtag" />
            &nbsp;Linked
          </span>
        )}
        {isUnread && (
          <span className="post-indicator is-unread">
            <i className="fas fa-asterisk" />
            &nbsp;New
          </span>
        )}
      </>
    );
  })();

  if (!hasAvatar) {
    url = `${config.cdnHost}/image/none.webp`;
  }
  if (isBanned) {
    url = 'https://img.icons8.com/color/80/000000/minus.png';
    title = `${username} is banned!`;
  }

  return (
    <StyledPost id={`post-${postId}`} hasAvatar={hasAvatar} canRate={subforumName !== 'Politics'}>
      {!hideUserWrapper && (
        <Link title={`${user.username}'s profile`} to={`/user/${user.id}`} className="user-wrapper">
          <img className="user-avatar" alt={title} src={url} />
          <UserRoleWrapper user={user}>{username}</UserRoleWrapper>
          {bgUrl && <img className="user-background" alt="User Background" src={bgUrl} />}
          <span className="user-join-date" title={userJoinDateLong}>
            {userCakeDay && `🍰 `}
            {userJoinDateShort}
            {userCakeDay && ` 🎉`}
          </span>
        </Link>
      )}

      <div className="post-body">
        <div className="post-toolbar">
          <Link
            className="post-details"
            to={`${threadId ? `/thread/${threadId}/${postPage}` : ''}#post-${postId}`}
            title={new Intl.DateTimeFormat(undefined, {
              year: 'numeric',
              month: 'long',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
            }).format(new Date(postDate))}
          >
            {postIndicatorIcons}
            {postDateHuman}
            {countryName && ` from ${countryName}`}
          </Link>

          <PostControls
            postId={postId}
            user={user}
            toggleEditable={() => setEditing((value) => !value)}
            showBBCode={toggleShowBBCode}
            replyToPost={replyToPost}
            threadLocked={threadLocked}
            byCurrentUser={byCurrentUser}
            hideControls={hideControls}
          />
        </div>

        <div className="post-content">
          {editing ? (
            <PostEditor
              subforumId={subforumId}
              threadId={threadId}
              initialContent={postBody}
              type="edit"
              postId={postId}
              postSubmitFn={postSubmitFn}
            />
          ) : (
            <ErrorBoundary errorMessage="Invalid KnockoutBB code.">
              <ParseBB content={postBody} />
            </ErrorBoundary>
          )}
        </div>

        {subforumName !== 'Politics' && (
          <RatingBar
            postId={postId}
            ratings={ratingsState}
            userId={user.id}
            refreshPost={refreshPost}
            xrayEnabled={ratingsXray}
            postByCurrentUser={byCurrentUser}
            subforumName={subforumName}
          />
        )}

        {bans &&
          bans.length > 0 &&
          bans.map((ban) => {
            const { banBannedBy, banReason, banCreatedAt, banExpiresAt } = ban;
            const banLength = humanizeDuration(banCreatedAt, banExpiresAt);

            return (
              <div key={banReason + banCreatedAt} className="ban-item">
                <img src="/static/icons/police-badge.png" alt="Police Badge" />
                <span>
                  {`User was banned for this post by ${banBannedBy}`}
                  &nbsp;with reason &quot;
                  <strong>{banReason}</strong>
                  &quot; for&nbsp;
                  {banLength}
                </span>
              </div>
            );
          })}
      </div>
    </StyledPost>
  );
};

Post.propTypes = {
  userJoinDate: PropTypes.string.isRequired,
  postBody: PropTypes.string.isRequired,
  postDate: PropTypes.string.isRequired,
  postId: PropTypes.number.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    isBanned: PropTypes.bool.isRequired,
    avatar_url: PropTypes.string.isRequired,
    backgroundUrl: PropTypes.string,
  }).isRequired,
  byCurrentUser: PropTypes.bool,
  bans: PropTypes.array,
  threadId: PropTypes.number.isRequired,
  threadLocked: PropTypes.bool,
  postPage: PropTypes.number,
  isUnread: PropTypes.bool,
  hideUserWrapper: PropTypes.bool,
  hideControls: PropTypes.bool,
  subforumName: PropTypes.string.isRequired,
  countryName: PropTypes.string,
  isLinkedPost: PropTypes.bool,
  ratings: PropTypes.array,
  threadPage: PropTypes.number,
  subforumId: PropTypes.number,
  handleReplyClick: PropTypes.func.isRequired,
  postSubmitFn: PropTypes.func.isRequired,
};

Post.defaultProps = {
  byCurrentUser: false,
  bans: [],
  threadLocked: false,
  postPage: 1,
  isUnread: false,
  hideUserWrapper: false,
  hideControls: false,
  countryName: '',
  isLinkedPost: false,
  ratings: [],
  threadPage: 1,
  subforumId: undefined,
};

export default Post;

const StyledPost = styled.div`
  display: grid;
  grid-template-columns: 230px 1fr;
  min-height: 250px;
  overflow: hidden;
  margin-bottom: ${ThemeVerticalPadding};
  font-size: ${ThemeFontSizeMedium};
  ${(props) => props.anchor && `outline: 1px solid ${ThemeHighlightWeaker};`}

  .user-wrapper {
    display: flex;
    justify-content: start;
    align-items: center;
    flex-direction: column;
    position: relative;
    min-height: 230px;
    backdrop-filter: blur(3px);
    padding-top: 45px;

    transition: min-height 250ms ease-in-out;
    transition-delay: 550ms;

    ${DesktopMediaQuery} {
      &:hover {
        min-height: 440px;

        .user-background {
          filter: none;
        }

        .user-avatar,
        .user-join-date,
        .user-role-wrapper-component {
          opacity: 0;
        }
      }
    }

    .user-role-wrapper-component {
      font-size: ${ThemeFontSizeMedium};
      z-index: 3;
      top: 175px;
      left: 50%;
      font-weight: bold;
      height: 20px;
      transition: opacity ease-in-out 500ms;
    }

    .user-join-date {
      font-size: ${ThemeFontSizeSmall};
      z-index: 3;
      top: 195px;
      left: 50%;
      text-shadow: 1px 1px 1px black;
      transition: opacity ease-in-out 500ms;
    }
  }

  .user-avatar {
    z-index: 2;
    margin-bottom: ${ThemeVerticalPadding};
    transition: opacity ease-in-out 500ms;
  }

  .user-background {
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    content: '';
    filter: brightness(0.5) contrast(0.925);
    mask-image: linear-gradient(rgb(255, 255, 255) 0%, rgb(255, 255, 255) 85%, transparent 100%);
    transition: filter ease-in-out 500ms;
  }

  .post-body {
    display: grid;
    grid-template-rows: 30px 1fr;
    background-color: ${ThemeBackgroundDarker};
    ${(props) => !props.canRate && `padding-bottom: calc(${ThemeVerticalPadding(props)} * 3);`}
    white-space: break-spaces;
    .bb-post {
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      line-height: ${ThemePostLineHeight}%;
    }
  }

  .post-toolbar {
    background: ${ThemeBackgroundLighter};
    line-height: 30px;
    padding: 0 ${ThemeHorizontalPadding};
    padding-right: 0;
    font-size: ${ThemeFontSizeMedium};
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .post-details {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    min-width: 0;
    width: 0;
    flex: 1;
  }

  .post-content {
    background: ${ThemeBackgroundDarker};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    font-size: ${ThemeFontSizeMedium};
    line-height: 1.5;
    white-space: pre-wrap;
    overflow: hidden;
    overflow-wrap: break-word;
    min-height: 160px;
  }

  .ban-item {
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background: ${ThemeKnockoutRed};
    display: flex;
    align-items: center;

    img {
      margin-right: ${ThemeHorizontalPadding};
    }
  }

  .post-indicator {
    font-size: ${ThemeFontSizeSmall};
    text-transform: uppercase;
    margin-right: ${ThemeHorizontalPadding};
    padding: 0.1875rem ${ThemeHorizontalPadding} calc(0.1875rem - 1px); /*i hate css*/
    opacity: 0.7;
    transition: opacity 150ms ease-in-out;
    font-weight: bold;

    &:hover {
      opacity: 1;
    }
  }
  .is-linked {
    background: ${ThemeKnockoutRed};
  }
  .is-self {
    background: ${ThemeHighlightStronger};
  }
  .is-unread {
    background: ${ThemeHighlightWeaker};
  }

  @media (max-width: 900px) {
    grid-template-columns: unset;
    grid-template-rows: 5rem 1fr;

    .post-toolbar {
      a {
        font-size: ${ThemeFontSizeSmall};
      }
    }

    .user-wrapper {
      overflow: hidden;
      min-height: unset;
      height: 5rem;
      align-items: left;
      justify-content: left;
      padding: unset;
      flex-direction: row;
      padding-left: ${ThemeHorizontalPadding};

      .user-role-wrapper-component {
        position: initial;
        transform: none;
      }
      .user-avatar {
        position: initial;
        width: auto;
        transform: none;
        max-width: 70px;
        max-height: 70px;
        margin: 0 ${ThemeHorizontalPadding} 0 0;
        ${(props) => !props.hasAvatar && `display: none;`}
      }
      .user-join-date {
        display: none;
      }
      .user-background {
        position: absolute;
        width: 100%;
        top: 50%;
        transform: translateY(-50%);
      }
    }
  }
`;
