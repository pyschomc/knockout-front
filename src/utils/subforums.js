const MIN_ACCT_AGE_SUBFORUM_IDS = [5, 6]; // subforumIds where acct has to be at least 3 days old to post

// eslint-disable-next-line import/prefer-default-export
export function subforumHasAccountAgeCheck(subforumId) {
  return MIN_ACCT_AGE_SUBFORUM_IDS.includes(subforumId);
}
