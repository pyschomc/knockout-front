import React from 'react';
import { Helmet } from 'react-helmet';
import getEventsList from '../../services/events';

import EventsComponent from './components/EventsComponent';

class EventsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      eventsLoaded: false,
    };
  }

  componentDidMount = async () => {
    const events = await getEventsList();
    this.setState({ events, eventsLoaded: true });
  };

  render() {
    const { events, eventsLoaded } = this.state;
    return (
      <>
        <Helmet>
          <title>Event Log - Knockout!</title>
        </Helmet>
        <EventsComponent events={events} eventsLoaded={eventsLoaded} />
      </>
    );
  }
}

export default EventsPage;
