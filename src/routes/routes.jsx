import React from 'react';
import loadable from 'loadable-components';

import Loading from '../components/Loading';

const LoadingComponent = () => <Loading />;

// refactored routes
export const HomePageNew = loadable(() => import('../views/HomePage'), { LoadingComponent });
export const SubforumPageNew = loadable(() => import('../views/SubforumPage'), {
  LoadingComponent,
});
export const ThreadPageNew = loadable(() => import('../views/ThreadPage'), { LoadingComponent });
export const ThreadCreationPageNew = loadable(() => import('../views/ThreadCreationPage'), {
  LoadingComponent,
});
export const EventsPageNew = loadable(() => import('../views/EventsPage'), { LoadingComponent });
export const UserSettingsPageNew = loadable(() => import('../views/UserSettingsPage'), {
  LoadingComponent,
});
export const AlertsListNew = loadable(() => import('../views/AlertsList'), { LoadingComponent });
// legacy routes
export const SubforumPage = loadable(() => import('../pages/SubforumPage'), { LoadingComponent });
export const ThreadPage = loadable(() => import('../pages/ThreadPage'), { LoadingComponent });
export const HomePage = loadable(() => import('../pages/HomePage'), { LoadingComponent });
export const UserSetup = loadable(() => import('../pages/UserSetup'), { LoadingComponent });
export const UserSetupNew = loadable(() => import('../views/UserSetupPage'), { LoadingComponent });
export const Rules = loadable(() => import('../pages/RulesPage'), { LoadingComponent });
export const RulesNew = loadable(() => import('../views/RulesPage'), { LoadingComponent });
export const AlertsList = loadable(() => import('../pages/AlertsList'), { LoadingComponent });
export const KnockoutBBSyntaxPage = loadable(() => import('../pages/KnockoutBBSyntaxPage'), {
  LoadingComponent,
});
export const LoginPage = loadable(() => import('../pages/LoginPage'), { LoadingComponent });
export const PrivacyPolicyPage = loadable(() => import('../pages/PrivacyPolicy'), {
  LoadingComponent,
});
export const ThreadCreationPage = loadable(() => import('../pages/ThreadCreation'), {
  LoadingComponent,
});
export const UserSettings = loadable(() => import('../pages/UserSettings'), {
  LoadingComponent,
});
export const UserProfile = loadable(() => import('../pages/UserProfile/index'), {
  LoadingComponent,
});
export const EventsPage = loadable(() => import('../pages/EventsPage'), {
  LoadingComponent,
});
export const Logout = loadable(() => import('../pages/Logout'), {
  LoadingComponent,
});
export const TempThemeSettings = loadable(() => import('../views/TempThemeSettings'), {
  LoadingComponent,
});

// moderation:
export const ModerateDashboard = loadable(
  () => import('../pages/Moderation/components/ModerateDashboard'),
  {
    LoadingComponent,
  }
);
export const ModerateUserLookup = loadable(
  () => import('../pages/Moderation/components/ModerateUserLookup'),
  {
    LoadingComponent,
  }
);
export const ModerateReports = loadable(() => import('../pages/ModerateReports/index'), {
  LoadingComponent,
});
export const ModerateIpLookup = loadable(
  () => import('../pages/Moderation/components/ModerateIpLookup'),
  {
    LoadingComponent,
  }
);
export const ModerateSubforums = loadable(
  () => import('../pages/Moderation/components/ModerateSubforums'),
  {
    LoadingComponent,
  }
);
export const ModerateUsers = loadable(
  () => import('../pages/Moderation/components/ModerateUserLookup'),
  {
    LoadingComponent,
  }
);
export const ModerateTags = loadable(() => import('../pages/Moderation/components/ModerateTags'), {
  LoadingComponent,
});

// search
export const SearchPage = loadable(() => import('../views/SearchPage'), {
  LoadingComponent,
});

// easter eggs:
export const LoveHer = loadable(() => import('../pages/EasterEggs/lovelain'), {
  LoadingComponent,
});

export const Review2019 = loadable(() => import('../pages/2019Review'), {
  LoadingComponent,
});
