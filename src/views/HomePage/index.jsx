import React, { useEffect, useState } from 'react';
import { getSubforumList } from '../../services/subforums';
import SubforumItem from './components/SubforumItem';
import { HomePageSubforumContainer, HomePageWrapper } from './components/HomePageLayout';
import LatestAndPopular from './components/LatestAndPopular';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../utils/postOptionsStorage';

const HomePage = () => {
  const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();
  const [subforums, setSubforums] = useState([]);
  /* state = {
    subforums: [],
    popularThreads: [],
    newThreads: []
  }; */

  useEffect(() => {
    const getSubforums = async () => {
      const subforumsList = await getSubforumList();
      setSubforums(subforumsList.list);
    };

    getSubforums();
  }, []);

  if (subforums.length === 0) return 'Loading';

  return (
    <HomePageWrapper>
      <LatestAndPopular />

      <HomePageSubforumContainer>
        {subforums.map((subforum, i) => (
          <SubforumItem
            key={subforum.id}
            index={i}
            createdAt={subforum.createdAt}
            description={subforum.description}
            icon={subforum.icon}
            iconId={subforum.iconId}
            id={subforum.id}
            lastPostId={subforum.lastPostId}
            lastPost={subforum.lastPost}
            name={subforum.name}
            totalPosts={subforum.totalPosts}
            totalThreads={subforum.totalThreads}
            updatedAt={subforum.updatedAt}
            nsfwFilterEnabled={nsfwFilterEnabled}
          />
        ))}
      </HomePageSubforumContainer>
    </HomePageWrapper>
  );
};

export default HomePage;
