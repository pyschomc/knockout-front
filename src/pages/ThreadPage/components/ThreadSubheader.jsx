/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';

import LoggedInOnly from '../../../components/LoggedInOnly';
import Pagination from '../../../components/Pagination/Pagination';
import {
  SubHeader,
  SubHeaderBack,
  SubHeaderBackAndTitle,
  SubHeaderBackText,
  SubHeaderDropdown,
  SubHeaderDropdownArrow,
  SubHeaderDropdownList,
  SubHeaderDropdownListItem,
  SubHeaderPagination,
  SubHeaderPaginationAndButtons,
  SubHeaderRealButton,
} from '../../../components/SubHeader/style';
import EditableBackground from './EditableBackground';
import EditableTitle from './EditableTitle';
import { ThreadStatusAction } from './style';
import Tooltip from '../../../components/Tooltip';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import { scrollToTop } from '../../../utils/pageScroll';
import MarkUnreadButton from './MarkUnreadButton';
import TagButton from './ChangeTags';

const ThreadSubheader = ({
  thread,
  params,
  currentPage,
  togglePinned,
  toggleDeleted,
  toggleLocked,
  currentUserId,
  showMoveModal,
  createAlert,
  deleteAlert,
}) => (
  <SubHeader>
    <SubHeaderBackAndTitle>
      <SubHeaderBack to={`/subforum/${thread.subforumId}`} onClick={scrollToTop}>
        &#8249;
        <SubHeaderBackText to={`/subforum/${thread.subforumId}`}>
          {thread.subforumName || 'Subforum'}
        </SubHeaderBackText>
      </SubHeaderBack>
    </SubHeaderBackAndTitle>
    <SubHeaderPaginationAndButtons>
      <SubHeaderPagination>
        <Pagination
          totalPosts={thread.totalPosts}
          pagePath={`/thread/${params.id}/`}
          currentPage={currentPage}
          showNext
        />
      </SubHeaderPagination>
      <LoggedInOnly>
        <SubHeaderDropdown>
          <SubHeaderDropdownArrow className="fa fa-angle-down" />
          <SubHeaderDropdownList>
            <UserGroupRestricted userGroupIds={[3, 4, 5]}>
              <SubHeaderDropdownListItem>
                <Tooltip text={thread.pinned ? 'Unpin Thread' : 'Pin Thread'}>
                  <ThreadStatusAction active={thread.pinned} onClick={togglePinned}>
                    {thread.pinned ? (
                      <i className="fas fa-sticky-note" />
                    ) : (
                      <i className="far fa-sticky-note" />
                    )}
                    <span>{thread.pinned ? 'Unpin Thread' : 'Pin Thread'}</span>
                  </ThreadStatusAction>
                </Tooltip>
              </SubHeaderDropdownListItem>
              <SubHeaderDropdownListItem>
                <Tooltip text={thread.locked ? 'Unlock Thread' : 'Lock Thread'}>
                  <ThreadStatusAction active={thread.locked} onClick={toggleLocked}>
                    {thread.locked ? (
                      <i className="fas fa-lock-open" />
                    ) : (
                      <i className="fas fa-lock" />
                    )}
                    <span>{thread.locked ? 'Unlock Thread' : 'Lock Thread'}</span>
                  </ThreadStatusAction>
                </Tooltip>
              </SubHeaderDropdownListItem>
              <SubHeaderDropdownListItem>
                <Tooltip text={thread.deleted ? 'Restore Thread' : 'Delete Thread'}>
                  <ThreadStatusAction active={thread.deleted} onClick={toggleDeleted}>
                    {thread.deleted ? (
                      <i className="fas fa-trash-restore" />
                    ) : (
                      <i className="fas fa-trash" />
                    )}
                    <span>{thread.deleted ? 'Restore Thread' : 'Delete Thread'}</span>
                  </ThreadStatusAction>
                </Tooltip>
              </SubHeaderDropdownListItem>
              <SubHeaderDropdownListItem>
                <Tooltip text="Move thread">
                  <ThreadStatusAction active={thread.locked} onClick={showMoveModal}>
                    <i className="fas fa-people-carry" />
                    <span>Move thread</span>
                  </ThreadStatusAction>
                </Tooltip>
              </SubHeaderDropdownListItem>
            </UserGroupRestricted>
            <EditableBackground
              backgroundUrl={thread.threadBackgroundUrl}
              byCurrentUser={currentUserId === thread.userId}
              threadId={thread.id}
            />
            <EditableTitle
              title={thread.title}
              byCurrentUser={currentUserId === thread.userId}
              threadId={thread.id}
            />
            <MarkUnreadButton threadId={thread.id} />
            <UserGroupRestricted userGroupIds={[3, 4, 5]}>
              <TagButton thread={thread} />
            </UserGroupRestricted>
            <div>
              {thread.isSubscribedTo ? (
                <SubHeaderDropdownListItem>
                  <Tooltip text="Unsubscribe">
                    <SubHeaderRealButton active={thread.isSubscribedTo} onClick={deleteAlert}>
                      <i className="fas fa-eye-slash" />
                      <span>Unsubscribe</span>
                    </SubHeaderRealButton>
                  </Tooltip>
                </SubHeaderDropdownListItem>
              ) : (
                <SubHeaderDropdownListItem>
                  <Tooltip text="Subscribe">
                    <SubHeaderRealButton active={thread.isSubscribedTo} onClick={createAlert}>
                      <i className="fas fa-eye" />
                      <span>Subscribe</span>
                    </SubHeaderRealButton>
                  </Tooltip>
                </SubHeaderDropdownListItem>
              )}
            </div>
          </SubHeaderDropdownList>
        </SubHeaderDropdown>
      </LoggedInOnly>
    </SubHeaderPaginationAndButtons>
  </SubHeader>
);
ThreadSubheader.propTypes = {
  thread: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  currentPage: PropTypes.number.isRequired,
  togglePinned: PropTypes.func.isRequired,
  toggleLocked: PropTypes.func.isRequired,
  toggleDeleted: PropTypes.func.isRequired,
  showMoveModal: PropTypes.func.isRequired,
  deleteAlert: PropTypes.func.isRequired,
  createAlert: PropTypes.func.isRequired,
  currentUserId: PropTypes.number.isRequired,
};

export default ThreadSubheader;
