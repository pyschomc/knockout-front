import { connect } from 'react-redux';
import store from '../../state/configureStore';

export const usergroupCheck = (userGroupIds = []) => {
  const state = store.getState();
  if (state && state.user && state.user.usergroup) {
    return userGroupIds.includes(state.user.usergroup);
  }
  return false;
};

const UserGroupRestricted = ({ children, userGroupIds = [], userState }) => {
  if (userGroupIds.includes(userState.usergroup)) {
    return children;
  }
  return null;
};

const mapStateToProps = ({ user }) => ({
  userState: user
});

export default connect(mapStateToProps)(UserGroupRestricted);
