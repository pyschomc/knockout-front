export const HEADER_UPDATE = 'HEADER_UPDATE';

export function updateHeader(value) {
  return {
    type: HEADER_UPDATE,
    value,
  };
}
