import styled from 'styled-components';
import { ThemePrimaryBackgroundColor, ThemePrimaryTextColor } from '../../Theme';

const StyledEmbedWarning = styled.div`
  width: 100%;
  background: ${ThemePrimaryBackgroundColor};
  color: ${ThemePrimaryTextColor};
  text-align: center;
  max-width: 450px;
  padding: 15px;
  box-sizing: border-box;
  border-radius: 5px;
  margin-bottom: 15px;

  button {
    border: none;
    background: #ffc107;
    padding: 10px;
    border-radius: 3px;

    &:hover {
      filter: brightness(1.1);
    }
  }
`;

export default StyledEmbedWarning;
