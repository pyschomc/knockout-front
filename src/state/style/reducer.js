import { HEADER_UPDATE } from './actions';
import { loadStickyHeaderFromStorageBoolean } from '../../services/theme';

const initialState = {
  stickyHeader: loadStickyHeaderFromStorageBoolean(),
};

export default function (state = initialState, action) {
  switch (action.type) {
    case HEADER_UPDATE:
      return { ...state, stickyHeader: action.value };
    default:
      break;
  }
  return state;
}
