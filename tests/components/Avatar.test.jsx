import React from 'react';
import renderer from 'react-test-renderer';
import UserAvatar from '../../src/componentsNew/Avatar';

jest.mock('../../config', () => {
  return require('../../appConfig'); // eslint-disable-line global-require
});

test('UserAvatar', () => {
  const userAvatar = renderer.create(<UserAvatar src="image.png" alt="CDN Avatar" />);

  const userAvatarTree = userAvatar.toJSON();
  expect(userAvatarTree).toMatchSnapshot();
});

test('UserAvatar with no alt property specified', () => {
  const userAvatar = renderer.create(<UserAvatar src="image.png" />);

  const userAvatarTree = userAvatar.toJSON();
  expect(userAvatarTree).toMatchSnapshot();
});
