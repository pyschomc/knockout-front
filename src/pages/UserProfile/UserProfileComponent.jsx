/* eslint-disable react/prop-types */
/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';

import { DefaultBlueHollowButton } from '../../components/SharedStyles';
import UserProfileInfo from './components/UserProfileInfo';
import UserProfileHeading from './components/UserProfileHeading';
import UserProfileSwitch from './components/UserProfileSwitch';
import { StyledProfileWrapper, StyledProfileContent } from './components/style';
import UserProfileEditor from './components/UserProfileEditor';

const UserProfileComponent = ({
  user,
  bans,
  userProfile,
  topRatings,
  canEditProfile,
  showProfileEditor,
  hideProfileEditor,
  editingProfile,
  profileSwitch
}) => (
  <StyledProfileWrapper>
    <UserProfileInfo user={user} topRatings={topRatings} />
    <StyledProfileContent>
      <UserProfileHeading {...userProfile} />

      {canEditProfile && (
        <DefaultBlueHollowButton onClick={showProfileEditor}>Edit profile</DefaultBlueHollowButton>
      )}

      {editingProfile && <UserProfileEditor closeFn={hideProfileEditor} />}
      <UserProfileSwitch
        ref={profileSwitch}
        userId={user.id}
        bans={bans}
        user={user}
        ratings={topRatings}
      />
    </StyledProfileContent>
  </StyledProfileWrapper>
);

UserProfileComponent.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    posts: PropTypes.number.isRequired,
    threads: PropTypes.number.isRequired
  }).isRequired,
  bans: PropTypes.array.isRequired,
  ratings: PropTypes.array
};
UserProfileComponent.defaultProps = {
  ratings: []
};

export default UserProfileComponent;
