import React, { useState } from 'react';
import {
  deleteCustomThemeFromStorage,
  setCustomThemeToStorage,
  loadCustomThemeFromStorage,
} from '../../services/theme';

let customTheme = {
  backgroundLighter: '#1f2c39',
  backgroundDarker: '#161d24',
  mainBackgroundColor: '#0b0e11',
  bodyBackgroundColor: '#0d1013',
  knockoutBrandColor: '#ec3737',
  knockoutLogo: '/static/logo_dark.png',
  textColor: '#fff',
  textColorInverted: '#000',
  themeFontSizeSmall: '11px',
  themeFontSizeMedium: '14px',
  themeFontSizeLarge: '16px',
  themeFontSizeHuge: '24px',
  themeGoldMemberColor: 'linear-gradient(375deg, #fcbe20, #fcbe20, #ffe770, #fcbe20, #fcbe20)',
  themeGoldMemberGlow: 'drop-shadow(0px 0px 2px #ffcc4a)',
  themeModeratorColor: '#41ff74',
  themeBannedUserColor: `red`,
};

try {
  const storageTheme = loadCustomThemeFromStorage();
  if (storageTheme) {
    customTheme = {
      ...customTheme,
      ...JSON.parse(storageTheme),
    };
  }
} catch (error) {
  console.log('Could not load custom theme from local storage. ', error);
}

const ThemeSettingsPage = () => {
  const [themeValues, setThemeValues] = useState(customTheme);
  const [exportThemeString, setExportThemeString] = useState(JSON.stringify(themeValues));
  const saveTheme = () => {
    setCustomThemeToStorage(JSON.stringify(themeValues));
  };
  const handleChange = ({ target: { name, value } }) => {
    setThemeValues({
      ...themeValues,
      [name]: value,
    });
  };

  const handleExportThemeStringChange = ({ target: { value } }) => {
    setExportThemeString(value);
  };
  const loadThemeFromString = () => {
    try {
      const theme = JSON.parse(exportThemeString);
      setThemeValues(theme);
    } catch (error) {
      alert('Could not read theme from string.');
    }
  };
  const exportThemeToString = () => {
    setExportThemeString(JSON.stringify(themeValues));
  };

  return (
    <div>
      owo whats this
      <div>
        <label htmlFor="backgroundLighter">backgroundLighter: </label>
        <input
          type="text"
          id="backgroundLighter"
          name="backgroundLighter"
          value={themeValues.backgroundLighter}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="backgroundDarker">backgroundDarker: </label>
        <input
          type="text"
          id="backgroundDarker"
          name="backgroundDarker"
          value={themeValues.backgroundDarker}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="mainBackgroundColor">mainBackgroundColor: </label>
        <input
          type="text"
          id="mainBackgroundColor"
          name="mainBackgroundColor"
          value={themeValues.mainBackgroundColor}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="bodyBackgroundColor">bodyBackgroundColor: </label>
        <input
          type="text"
          id="bodyBackgroundColor"
          name="bodyBackgroundColor"
          value={themeValues.bodyBackgroundColor}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="knockoutBrandColor">knockoutBrandColor: </label>
        <input
          type="text"
          id="knockoutBrandColor"
          name="knockoutBrandColor"
          value={themeValues.knockoutBrandColor}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="knockoutLogo">knockoutLogo: </label>
        <input
          type="text"
          id="knockoutLogo"
          name="knockoutLogo"
          value={themeValues.knockoutLogo}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="textColor">textColor: </label>
        <input
          type="text"
          id="textColor"
          name="textColor"
          value={themeValues.textColor}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="textColorInverted">textColorInverted: </label>
        <input
          type="text"
          id="textColorInverted"
          name="textColorInverted"
          value={themeValues.textColorInverted}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="themeFontSizeSmall">themeFontSizeSmall: </label>
        <input
          type="text"
          id="themeFontSizeSmall"
          name="themeFontSizeSmall"
          value={themeValues.themeFontSizeSmall}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="themeFontSizeMedium">themeFontSizeMedium: </label>
        <input
          type="text"
          id="themeFontSizeMedium"
          name="themeFontSizeMedium"
          value={themeValues.themeFontSizeMedium}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="themeFontSizeLarge">themeFontSizeLarge: </label>
        <input
          type="text"
          id="themeFontSizeLarge"
          name="themeFontSizeLarge"
          value={themeValues.themeFontSizeLarge}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="themeFontSizeHuge">themeFontSizeHuge: </label>
        <input
          type="text"
          id="themeFontSizeHuge"
          name="themeFontSizeHuge"
          value={themeValues.themeFontSizeHuge}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="themeGoldMemberColor">themeGoldMemberColor: </label>
        <input
          type="text"
          id="themeGoldMemberColor"
          name="themeGoldMemberColor"
          value={themeValues.themeGoldMemberColor}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="themeGoldMemberGlow">themeGoldMemberGlow: </label>
        <input
          type="text"
          id="themeGoldMemberGlow"
          name="themeGoldMemberGlow"
          value={themeValues.themeGoldMemberGlow}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="themeModeratorColor">themeModeratorColor: </label>
        <input
          type="text"
          id="themeModeratorColor"
          name="themeModeratorColor"
          value={themeValues.themeModeratorColor}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="themeBannedUserColor">themeBannedUserColor: </label>
        <input
          type="text"
          id="themeBannedUserColor"
          name="themeBannedUserColor"
          value={themeValues.themeBannedUserColor}
          onChange={handleChange}
        />
      </div>
      <button type="button" onClick={saveTheme}>
        save!!!
      </button>
      <div>
        <input type="text" value={exportThemeString} onChange={handleExportThemeStringChange} />

        <button type="button" onClick={loadThemeFromString}>
          load from input
        </button>

        <button type="button" onClick={exportThemeToString}>
          export current theme to input for sharing
        </button>
      </div>
      <div>
        <button  type="button" onClick={() => deleteCustomThemeFromStorage()}>YEET THEME COMPLETELY</button>
      </div>

      <p>
        reload page to see changes
      </p>
    </div>
  );
};

export default ThemeSettingsPage;
