import React from 'react';

const SearchBar = (value, setValue) => <input type="text" value={value} onChange={setValue} />;

export default SearchBar;
