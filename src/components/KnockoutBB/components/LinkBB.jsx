/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import dayjs from 'dayjs';

import { StyledAnchor } from '../style';
import Tooltip from '../../Tooltip';
import { buttonHoverGray } from '../../SharedStyles';
import getOpenGraphData from '../../../services/openGraph';
import {
  ThemeQuaternaryBackgroundColor,
  ThemeVerticalPadding,
  ThemeHorizontalPadding
} from '../../../Theme';

const StyledToggleContainer = styled.div`
  display: inline-block;
`;

class LinkBB extends React.Component {
  state = {
    isSmartLink: this.props.isSmart,
    openGraphData: null
  };

  componentDidMount() {
    if (this.state.isSmartLink) {
      this.getOgData();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.smart !== this.props.smart && nextProps.smart === 'true') {
      this.getOgData();
    }
  }

  setData = (e, newStatus) => {
    e.preventDefault();

    const href = this.props.node.data.get('href');

    this.props.editor.setNodeByKey(this.props.node.key, {
      data: { isSmartLink: newStatus, href },
      openGraphData: null
    });
    this.setState({ isSmartLink: newStatus });

    this.getOgData();
  };

  getOgData = async () => {
    if (!this.props.href) {
      return;
    }
    const ogData = await getOpenGraphData(this.props.href);

    if (ogData) {
      this.setState({
        openGraphData: {
          ...ogData
        }
      });
    }
  };

  render() {
    const { props } = this;

    const childrenString = typeof props.children === 'string' ? props.children : 'Link';

    const url = props.href ? props.href : childrenString;
    const children = props.children && props.children.length > 0 ? childrenString : `${props.href}`;

    return (
      <>
        {this.state.isSmartLink && this.state.openGraphData ? (
          <SmartLink linkUrl={props.href || childrenString} {...this.state.openGraphData} />
        ) : (
          <>
            <StyledAnchor {...props} href={url} target="_blank" rel="noopener noreferrer">
              {children}
            </StyledAnchor>

            {this.state.isSmartLink && (
              <StyledToggleContainer>
                <Tooltip text="Attempting to fetch link information...">
                  &nbsp;
                  <i className="fas fa-cloud-download-alt" />
                </Tooltip>
              </StyledToggleContainer>
            )}
          </>
        )}
      </>
    );
  }
}

LinkBB.propTypes = {
  editable: PropTypes.bool.isRequired,
  href: PropTypes.string,
  smart: PropTypes.bool,
  editor: PropTypes.any.isRequired,
  node: PropTypes.any.isRequired
};
LinkBB.defaultProps = {
  href: undefined,
  isSmartLink: undefined
};

export default LinkBB;

const SmartLinkWrapper = styled.a`
  width: 100%;
  max-width: 450px;
  max-height: 150px;
  display: flex;
  box-sizing: border-box;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  background: ${ThemeQuaternaryBackgroundColor};
  border-radius: 5px;
  overflow: hidden;
  cursor: pointer;
  position: relative;

  ${buttonHoverGray}
`;
const SmartLinkImageWrapper = styled.div`
  display: block;
  max-width: 100px;
  flex-shrink: 0;
  margin-right: 10px;
  z-index: 1;
`;
const SmartLinkImage = styled.img`
  display: block;
  max-width: 100%;
  height: auto;
  flex-shrink: 0;
`;
const SmartLinkInfo = styled.div`
  display: flex;
  flex-direction: column;
  width: 75%;
  /* height: 100%; */
  justify-content: space-between;
  z-index: 1;
`;
const SmartLinkTitle = styled.b`
  font-weight: bold;
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  font-size: 14px;
`;
const SmartLinkDescription = styled.div`
  width: 100%;
  font-size: 11px;
  margin-bottom: 5px;

  overflow: hidden;
  text-overflow: ellipsis;

  @supports not (-webkit-line-clamp: 3) {
    white-space: nowrap;
  }

  @supports (-webkit-line-clamp: 3) {
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  }
  /* max-height: 20px; */
`;
const SmartLinkAuthor = styled.div`
  font-size: 10px;
  opacity: 0.9;
`;
const SmartLinkSmallInfo = styled.div`
  display: flex;
  justify-content: space-between;
  opacity: 0.8;
`;
const SmartLinkUrl = styled.div`
  font-size: 9px;
  display: inline-block;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  max-width: 65%;
  color: #3facff;
`;
const SmartLinkDate = styled.div`
  font-size: 9px;
  display: inline-block;
`;
const SmartLinkBackground = styled.div`
  position: absolute;
  font-size: 35px;
  opacity: 0.03;
  z-index: 0;
  right: 5px;
  bottom: 5px;
`;

const SmartLink = ({ author, date, image, logo, title, url, description, linkUrl }) => (
  <SmartLinkWrapper href={linkUrl || url} target="_blank" rel="noopener noreferrer">
    {(image || logo) && (
      <SmartLinkImageWrapper>
        <SmartLinkImage src={image || logo} />
      </SmartLinkImageWrapper>
    )}
    <SmartLinkInfo>
      {title && <SmartLinkTitle>{title}</SmartLinkTitle>}
      {description && <SmartLinkDescription>{description}</SmartLinkDescription>}
      {author && <SmartLinkAuthor>{author}</SmartLinkAuthor>}
      {(url || date) && (
        <SmartLinkSmallInfo>
          {url && <SmartLinkUrl>{url}</SmartLinkUrl>}
          {date && <SmartLinkDate>{dayjs(date).format('DD/MM/YYYY HH:mm')}</SmartLinkDate>}
        </SmartLinkSmallInfo>
      )}
    </SmartLinkInfo>
    <SmartLinkBackground className="fas fa-external-link-alt" />
  </SmartLinkWrapper>
);

SmartLink.propTypes = {
  linkUrl: PropTypes.string,
  author: PropTypes.string,
  date: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  logo: PropTypes.string,
  title: PropTypes.string,
  url: PropTypes.string
};
SmartLink.defaultProps = {
  linkUrl: undefined,
  author: undefined,
  date: undefined,
  description: undefined,
  image: undefined,
  logo: undefined,
  title: undefined,
  url: undefined
};
