import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  ThemeMediumTextSize,
  ThemeSmallTextSize,
  ThemeSecondaryTextColor,
  ThemeTertiaryTextColor
} from '../../../Theme';

const getThreadCount = subforum => {
  if (subforum.totalThreads > 999) {
    return `${Math.floor(subforum.totalThreads / 1000)}k`;
  }
  return subforum.totalThreads;
};

const getPostCount = subforum => {
  if (subforum.totalPosts > 999) {
    return `${Math.floor(subforum.totalPosts / 1000)}k`;
  }
  return subforum.totalPosts;
};

const SubforumStats = ({ subforum }) => {
  return (
    <StyledSubforumStats>
      <span>
        <span className="stat-value">{subforum.totalThreads ? getThreadCount(subforum) : 0}</span>
        &nbsp;
        <span className="stat-label">
          <i className="fas fa-comment-dots" />
        </span>
      </span>
      <span>
        <span className="stat-value">{subforum.totalPosts ? getPostCount(subforum) : 0}</span>
        &nbsp;
        <span className="stat-label">
          <i className="fas fa-reply-all" />
        </span>
      </span>
    </StyledSubforumStats>
  );
};

SubforumStats.propTypes = {
  subforum: PropTypes.shape({
    lastPost: PropTypes.shape({
      user: PropTypes.shape({
        username: PropTypes.string.isRequired,
        usergroup: PropTypes.number.isRequired
      }).isRequired
    }),
    name: PropTypes.string.isRequired,
    totalThreads: PropTypes.number.isRequired,
    totalPosts: PropTypes.number.isRequired
  }).isRequired
};

export default SubforumStats;

const StyledSubforumStats = styled.div`
  > span:first-child {
    margin-right: 5px;
  }

  opacity: 0.75;

  .stat-value {
    line-height: 1.4;
    padding: 0;
    color: ${ThemeTertiaryTextColor};
    font-size: ${ThemeMediumTextSize};
    text-align: center;
  }

  .stat-label {
    line-height: 1.4;
    font-size: ${ThemeSmallTextSize};
    color: ${ThemeSecondaryTextColor};
    text-align: center;
  }
`;
