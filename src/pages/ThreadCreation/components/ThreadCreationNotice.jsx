import React from 'react';
import styled, { keyframes } from 'styled-components';
import { ThemeVerticalPadding, ThemeHorizontalPadding } from '../../../Theme';

const StyledNoticeWrapper = styled.div`
  position: relative;
  padding: 15px;
  z-index: 2;
  text-align: center;
  background: #f44336;

  h2 {
    margin: 0;
    margin-bottom: 10px;
  }

  h4 {
    line-height: 1.3;
    max-width: 480px;
    margin: 0 auto;
  }

  &:after {
    content: '';
    width: 100%;
    height: 100%;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    position: absolute;
    mix-blend-mode: overlay;
    opacity: 0.05;
  }
`;

const Spookyframes = keyframes`
  0% {
      transform: translate(-50px, 50px) rotate(-45deg) scale(1);
  }
  100% {
    transform: translate(-50px, -50px) rotate(45deg) scale(2);
  }
`;
const SpookyframesAlt = keyframes`
  0% {
      transform: translate(50px, -50px) rotate(45deg) scale(2);
  }
  100% {
    transform: translate(50px, 50px) rotate(-45deg) scale(1);
  }
`;

const SkullBoi = styled.img`
  width: 5%;
  height: auto;

  position: absolute;
  top: 120px;
  left: 20%;

  animation: ${Spookyframes} 1s ease-in-out alternate infinite;

  &:nth-child(2n) {
    left: initial;
    right: 20%;
    transform: translate(50px);

    animation: ${SpookyframesAlt} 1s ease-in-out alternate infinite;
  }
`;

const Spookmaster = keyframes`
  0% { 
    transform: perspective(300px) rotate3d(1, 0, 0, 25deg) scale(1);
  }
  100% {
    transform: perspective(300px) rotate3d(1, 0, 0, -25deg) scale(1.1);
  }
`;

const BigSpooker = styled.img`
  animation: ${Spookmaster} 3s ease-in-out alternate infinite;
`;

const bgAnim = keyframes`
  0% {
    transform: rotate(0deg) scale(4);
  }
  100% {
    transform: rotate(360deg) scale(4);
  }
`;

const StyledNoticeWrapperBg = styled.div`
  position: absolute;
  background: #ff0036;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  overflow: hidden;

  &:after {
    content: '';
    width: 100%;
    height: 100%;
    background: conic-gradient(
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00,
      #ff0,
      #f00
    );
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    position: absolute;
    animation: ${bgAnim} 4s linear infinite;
  }
`;

const StyledNoticeMain = styled.div`
  position: relative;
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  padding: 10px;
`;

const ThreadCreationNotice = () => (
  <StyledNoticeMain>
    <StyledNoticeWrapper>
      <h2>You&apos;re about to get banned!</h2>

      <BigSpooker
        src="https://img.icons8.com/color/128/000000/thriller.png"
        alt="Depiction of a skull, depicting your certain future if you dont read the rules"
      />

      <SkullBoi
        src="https://img.icons8.com/color/96/000000/poison.png"
        alt="Depiction of a skull, depicting your certain future if you dont read the rules"
      />
      <SkullBoi
        src="https://img.icons8.com/color/96/000000/poison.png"
        alt="Depiction of a skull, depicting your certain future if you dont read the rules"
      />

      <h4>Watch out! Looks like you&apos;re going to create a thread.</h4>
      <h4>
        Make sure you&apos;ve read and understood the rules and privacy policy before making a
        mistake you&apos;ll regret forever. Bans on Knockout are warnings, and you&nbsp;
        <em>will</em>
        &nbsp;be banned if you break rules or don&apos;t adhere to the standards of the community.
      </h4>
    </StyledNoticeWrapper>

    <StyledNoticeWrapperBg />
  </StyledNoticeMain>
);

export default ThreadCreationNotice;
