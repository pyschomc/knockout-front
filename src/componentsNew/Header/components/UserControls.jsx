import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import HeaderUserLoggedIn from './HeaderUserLoggedIn';
import { loadUserFromStorage } from '../../../services/user';
import { DefaultBlueHollowButton } from '../../../components/SharedStyles';

const LoginButtonStyle = {
  margin: 'auto',
};

class UserControls extends React.Component {
  tagline = '';

  render() {
    const user = loadUserFromStorage();
    const userId = user ? user.id : null;
    const avatarUrl = user ? user.avatar_url : null;
    const username = user ? user.username : null;
    const usergroup = user ? user.usergroup : null;
    const { unreadSubscriptions } = this.props;

    if (user == null) {
      return (
        <Link to="/login">
          <DefaultBlueHollowButton style={LoginButtonStyle}>Log in</DefaultBlueHollowButton>
        </Link>
      );
    }
    if (username == null) {
      return (
        <div>
          <Link to="/usersetup">
            <DefaultBlueHollowButton>User Setup</DefaultBlueHollowButton>
          </Link>
          <Link to="/logout">
            <DefaultBlueHollowButton>Log out</DefaultBlueHollowButton>
          </Link>
        </div>
      );
    }
    return (
      <HeaderUserLoggedIn
        userId={userId}
        unreadSubscriptions={unreadSubscriptions}
        avatarUrl={avatarUrl}
        username={username}
        usergroup={usergroup}
      />
    );
  }
}

UserControls.propTypes = {
  unreadSubscriptions: PropTypes.number.isRequired,
};

export default UserControls;
