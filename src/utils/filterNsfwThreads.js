export const filterNsfwThreads = (threads) => {
  const nsfwTagIndex = 1;

  return threads.filter((thread) => {
    if (!thread.tags || thread.tags.length === 0) {
      return thread;
    }
    
    return thread.tags.findIndex((tag) => {
      console.log(tag[nsfwTagIndex])
      return !tag[nsfwTagIndex];
    }) !== -1
      ? thread
      : null;
  });
};
