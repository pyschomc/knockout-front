import styled, { css } from 'styled-components';
import {
  ThemeHoverFilter,
  ThemeHoverShadow,
  ThemeVerticalHalfPadding,
  ThemeHorizontalHalfPadding,
  ThemePrimaryTextColor,
} from '../../Theme';

export const PageWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: stretch;
  padding: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  @media (max-width: 960px) {
    flex-wrap: wrap;
  }
`;

export const PageWrapperColumn = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;

export const buttonHover = css`
  cursor: pointer;
  transition: background 85ms ease-in-out, box-shadow 85ms ease-in-out;

  &:hover {
    background: #ec3737;
    box-shadow: inset 0px 0px 10px 0px #ff5959;
  }
`;

export const buttonHoverGray = css`
  cursor: pointer;
  transition: filter 85ms ease-in-out, box-shadow 85ms ease-in-out;

  &:hover {
    filter: ${ThemeHoverFilter};
    box-shadow: ${ThemeHoverShadow};
  }
`;

export const buttonHoverBrightness = css`
  cursor: pointer;
  transition: filter 85ms ease-in-out, box-shadow 85ms ease-in-out;

  &:hover,
  &:focus {
    filter: ${ThemeHoverFilter};
    box-shadow: ${ThemeHoverShadow};
  }
`;

export const TextInput = styled.input`
  width: 200px;
  height: 30px;
  padding: 5px;
  background: transparent;
  border: 1px solid gray;
  color: white;
  font-size: 16px;
  border-radius: 5px;
  margin-right: 15px;
  margin-bottom: 15px;
  font-family: 'Open Sans', sans-serif;
  box-sizing: border-box;
  vertical-align: middle;
`;

export const DefaultBlueHollowButton = styled.button`
  padding: 5px;
  background: transparent;
  border: 1px solid gray;
  color: ${ThemePrimaryTextColor};
  font-size: 16px;
  border-radius: 5px;
  margin-right: 15px;
  margin-bottom: 15px;
  font-family: 'Open Sans', sans-serif;
  box-sizing: border-box;

  &:hover {
    color: white;
  }

  ${buttonHover}
`;

export const TitleLarge = styled.h2`
  font-weight: 100;
  font-size: 28px;
  margin-top: 45px;
  margin-bottom: 10px;
  line-height: 1.5;

  &:nth-child(1) {
    margin-top: 0;
  }
`;

export const InformationalTextBody = styled.div`
  margin-bottom: 15px;
  line-height: 1.5;
`;

export const AbsoluteBlackout = styled.div`
  position: fixed;
  pointer-events: ${(props) => (props.openAnim ? 'all' : 'none')};
  background: rgba(0, 0, 0, 0.8);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 50;
  opacity: ${(props) => (props.openAnim ? 1 : 0)};
  transition: opacity ${(props) => props.transition / 1000}s;
`;

AbsoluteBlackout.defaultProps = {
  openAnim: true,
};

export const MobileMediaQuery = `@media (max-width: 700px)`;
export const TabletMediaQuery = `@media (max-width: 900px)`;
export const DesktopMediaQuery = `@media (min-width: 1200px)`;
export const SmallDesktopMediaQuery = `@media (max-width: 1200px)`;
