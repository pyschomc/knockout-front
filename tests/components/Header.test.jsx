/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import { customRender, screen, fireEvent } from '../custom_renderer';
import '@testing-library/jest-dom/extend-expect';

import Header from '../../src/componentsNew/Header';
import * as UserUtils from '../../src/utils/user';

describe('Header component', () => {
  const defaultState = { mentions: { mentions: [] } };

  beforeEach(() => {
    localStorage.clear();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('as a non-logged in user', () => {
    it('displays the knockout logo and name', () => {
      const { getByText, getByAltText } = customRender(<Header />, {
        initialState: defaultState,
      });
      expect(getByText('knockout!')).toBeInTheDocument();
      expect(getByAltText('knockout!')).toBeInTheDocument();
    });

    it('provides links to important pages such as Rules, Discord, login', () => {
      const { getByText } = customRender(<Header />, {
        initialState: defaultState,
      });
      expect(getByText('Rules').closest('a')).toHaveAttribute('href', '/rules');
      expect(getByText('Discord').closest('a')).toHaveAttribute(
        'href',
        'https://discord.gg/wjWpapC'
      );
      expect(getByText('Log in').closest('a')).toHaveAttribute('href', '/login');
    });

    it('does not have links to logged in user features, such as subscriptions and events', () => {
      customRender(<Header />, {
        initialState: defaultState,
      });

      expect(screen.queryByText('Subscriptions')).toBeNull();
      expect(screen.queryByText('Events')).toBeNull();
    });
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      ...defaultState,
      user: {
        loggedIn: true,
        username: 'TestUser',
      },
    };

    const userLocalStorageDetails = {
      id: 123,
      username: 'TestUser',
      usergroup: 1,
      avatar_url: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
    });

    it('displays links for logged in user features', () => {
      const { getByText } = customRender(<Header />, {
        initialState: loggedInState,
      });

      expect(getByText('Subscriptions').closest('a')).toHaveAttribute('href', '/alerts/list');
      expect(getByText('Events').closest('a')).toHaveAttribute('href', '/events');
    });

    describe('with unread subscriptions', () => {
      let checkLoginStatusMock;

      beforeEach(() => {
        checkLoginStatusMock = jest
          .spyOn(UserUtils, 'checkLoginStatus')
          .mockImplementation((_, updateHeader, __) => {
            updateHeader({
              subscriptions: [{ unreadPosts: 15 }, { unreadPosts: 20 }],
              mentions: [],
            });
          });
      });

      afterEach(() => {
        checkLoginStatusMock.mockRestore();
      });

      it('shows me the number of unread subscriptions I currently have', async () => {
        const { getByText } = customRender(<Header />, {
          initialState: loggedInState,
        });

        expect(getByText('35')).toBeInTheDocument();
      });
    });

    it('shows that I currently have no pending mentions', () => {
      const { getByText } = customRender(<Header />, {
        initialState: loggedInState,
      });

      fireEvent.click(getByText('TestUser'));
      expect(getByText('You have no new mentions!')).toBeInTheDocument();
    });

    describe('with new unread mentions', () => {
      const mentionsState = {
        ...loggedInState,
        mentions: {
          mentions: [{ mentionId: 123, threadId: 456, postId: 789, content: '["A mention!"]' }],
        },
      };

      it('shows me details of the mentions in the UserControls', () => {
        const { getByText } = customRender(<Header />, {
          initialState: mentionsState,
        });

        fireEvent.click(getByText('TestUser'));
        expect(getByText('A mention!')).toBeInTheDocument();
      });
    });

    describe('and the user is banned', () => {
      const banInformation = {
        banMessage: 'YOU ARE BANNED!',
        threadId: 123,
      };

      beforeEach(() => {
        localStorage.__STORE__.banInformation = JSON.stringify(banInformation);
      });

      it('shows me details of my ban', () => {
        const { getByText } = customRender(<Header />, {
          initialState: loggedInState,
        });

        expect(getByText('YOU ARE BANNED!')).toBeInTheDocument();
      });
    });

    describe('who is a moderator', () => {
      const moderatorState = {
        mentions: { mentions: [] },
        user: {
          loggedIn: true,
          username: 'TestUser',
          usergroup: 3,
        },
      };

      it('displays a link to the moderator tools', () => {
        const { getByText } = customRender(<Header />, {
          initialState: moderatorState,
        });

        expect(getByText('Moderation').closest('a')).toHaveAttribute('href', '/moderate');
      });

      describe('with open reports', () => {
        let checkLoginStatusMock;

        beforeEach(() => {
          checkLoginStatusMock = jest
            .spyOn(UserUtils, 'checkLoginStatus')
            .mockImplementation((_, updateHeader, __) => {
              updateHeader({
                subscriptions: [],
                mentions: [],
                reports: 123,
              });
            });
        });

        afterEach(() => {
          checkLoginStatusMock.mockRestore();
        });

        it('shows me the number of unread subscriptions I currently have', async () => {
          const { getByText } = customRender(<Header />, {
            initialState: moderatorState,
          });

          expect(getByText('123')).toBeInTheDocument();
        });
      });
    });
  });
});
