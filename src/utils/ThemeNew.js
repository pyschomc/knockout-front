/* eslint-disable no-console */
import theme from 'styled-theming';

let customTheme = {
  backgroundLighter: '#1f2c39',
  backgroundDarker: '#161d24',
  mainBackgroundColor: '#0b0e11',
  bodyBackgroundColor: '#0d1013d9',
  knockoutBrandColor: '#ec3737',
  knockoutLogo: '/static/logo_dark.png',
  textColor: '#fff',
  textColorInverted: '#000',
  themeFontSizeSmall: '0.688rem',
  themeFontSizeMedium: '0.875rem',
  themeFontSizeLarge: '1.000rem',
  themeFontSizeHuge: '1.500rem',
  themeGoldMemberColor: 'linear-gradient(375deg, #fcbe20, #fcbe20, #ffe770, #fcbe20, #fcbe20)',
  themeGoldMemberGlow: 'drop-shadow(0px 0px 2px #ffcc4a)',
  themeModeratorColor: '#41ff74',
  themeBannedUserColor: `red`,
  highlightStrongerColor: '#f07c00',
  highlightWeakerColor: '#4580bf',
  postLineHeight: 160,
};

try {
  if (localStorage.getItem('customTheme')) {
    customTheme = {
      ...customTheme,
      ...JSON.parse(localStorage.getItem('customTheme')),
    };
  }
} catch (error) {
  console.warn('Could not load custom theme from local storage. ', error);
}

// color
export const ThemeKnockoutRed = customTheme.knockoutBrandColor;
export const ThemeBackgroundLighter = customTheme.backgroundLighter;
export const ThemeBackgroundDarker = customTheme.backgroundDarker;
export const ThemeMainBackgroundColor = customTheme.mainBackgroundColor;
export const ThemeBodyBackgroundColor = customTheme.bodyBackgroundColor;
export const ThemeHighlightStronger = customTheme.highlightStrongerColor;
export const ThemeHighlightWeaker = customTheme.highlightWeakerColor;

export const ThemeMainLogo = customTheme.knockoutLogo;
export const ThemeTextColor = customTheme.textColor;

// member colors
export const ThemeGoldMemberColor = customTheme.themeGoldMemberColor;
export const ThemeGoldMemberGlow = customTheme.themeGoldMemberGlow;
export const ThemeModeratorColor = customTheme.themeModeratorColor;
export const ThemeBannedUserColor = customTheme.themeBannedUserColor;

// typography
export const ThemeFontSizeSmall = customTheme.themeFontSizeSmall;
export const ThemeFontSizeMedium = customTheme.themeFontSizeMedium;
export const ThemeFontSizeLarge = customTheme.themeFontSizeLarge;
export const ThemeFontSizeHuge = customTheme.themeFontSizeHuge;

// size
export const ThemeVerticalPadding = theme('scale', {
  large: '9px',
  medium: '8px',
  small: '5px',
});
export const ThemeHorizontalPadding = theme('scale', {
  large: '9px',
  medium: '8px',
  small: '5px',
});

// width
export const ThemeBodyWidth = theme('width', {
  full: 'auto',
  verywide: '1920px',
  wide: '1440px',
  medium: '1280px',
  narrow: '960px',
});

export const ThemePostLineHeight = customTheme.postLineHeight;
