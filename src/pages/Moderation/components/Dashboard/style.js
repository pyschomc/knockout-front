import styled from 'styled-components';
import {
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalHalfPadding,
  ThemePrimaryTextColor
} from '../../../../Theme';
import { MobileMediaQuery } from '../../../../components/SharedStyles';

export const Grid = styled.div`
  position: relative;
  display: flex;
  max-width: 1280px;
  margin: 0 auto;
  flex-wrap: wrap;
  flex-basis: 3;
  justify-content: space-evenly;

  ${MobileMediaQuery} {
    flex-basis: 2;
  }
`;

export const Tile = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  flex-basis: 30%;
  height: 200px;
  background-color: #888;
  background-image: linear-gradient(135deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.5) 100%);
  box-shadow: inset 0 1px 1px rgba(255, 255, 255, 0.2), inset 0 -1px 1px rgba(0, 0, 0, 0.5);
  border-radius: 5px;
  overflow: hidden;
  margin: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};

  ${MobileMediaQuery} {
    flex-basis: 40%;
  }

  &:nth-child(1) {
    background-color: #ff9688;
  }

  &:nth-child(2) {
    background-color: #d69787;
  }

  &:nth-child(3) {
    background-color: #b39488;
  }

  &:nth-child(4) {
    background-color: #8c807d;
  }

  &:nth-child(5) {
    background-color: #5c5a61;
  }
`;

export const TileHeader = styled.div`
  position: relative;
  text-align: center;
  color: ${ThemePrimaryTextColor};
  background: rgba(0, 0, 0, 0.15);
  box-shadow: inset 0 -5px 10px rgba(0, 0, 0, 0.05);
  padding: ${ThemeVerticalPadding} 0;
`;

export const TileBody = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-grow: 1;
`;

export const TileBodyCount = styled.span`
  position: relative;
  font-size: 64px;
  text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.5);
  color: white;
`;
