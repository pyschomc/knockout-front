import styled from 'styled-components';
import {
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeVerticalPadding,
  ThemeHoverFilter,
  ThemeHorizontalPadding
} from '../../../Theme';

export const UserProfileTabContainer = styled.div`
  width: 100;
  border-radius: 5px;
  overflow: hidden;
  display: flex;
  margin-bottom: ${ThemeVerticalPadding};
`;

export const UserProfileTab = styled.button`
  border: none;
  outline: none;
  flex: 1;

  cursor: pointer;

  padding: ${ThemeVerticalPadding} 0;

  background: ${ThemeQuaternaryBackgroundColor};
  color: ${ThemePrimaryTextColor};

  ${props =>
    props.isSelected &&
    `
    filter: brightness(1.1);
    `}

  &:hover {
    filter: ${ThemeHoverFilter};
  }

  &:nth-child(2) {
    margin: 0 1px;
  }
`;

export const UserProfileTopRatingWrapper = styled.div`
  margin: 0 0 0 5px;
  @media (min-width: 900px) {
    margin: 15px 0 0 0;
  }
`;

export const StyledProfileWrapper = styled.div`
  display: flex;
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  @media (max-width: 960px) {
    flex-direction: column;
  }
`;
export const StyledProfileContent = styled.div`
  flex: 1;
  padding-left: ${ThemeHorizontalPadding};

  @media (max-width: 960px) {
    padding-top: ${ThemeVerticalPadding};
    padding-left: 0;
  }
`;