import styled from 'styled-components';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeVerticalHalfPadding,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
  ThemePrimaryTextColor
} from '../../../Theme';
import { TabletMediaQuery, MobileMediaQuery } from '../../../components/SharedStyles';

export const SubforumListWrapper = styled.section`
  display: flex;
  flex-direction: row-reverse;
  width: 100%;
  margin-top: 5px;

  ${TabletMediaQuery} {
    flex-direction: column;
  }
`;
export const SubforumItemWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex: 1 1 auto;

  margin-bottom: ${ThemeVerticalPadding};
`;

export const SubforumItem = styled.div`
  width: calc(50% - ${ThemeHorizontalPadding});
  position: relative;
  overflow: hidden;
  display: flex;
  flex-direction: column;

  color: ${ThemePrimaryTextColor};

  background: ${ThemeBackgroundLighter};

  box-sizing: border-box;
  margin-bottom: ${ThemeVerticalHalfPadding};

  &:nth-child(odd) {
    margin-right: ${ThemeVerticalHalfPadding};
    border-radius: 6px 0 0 6px;
  }
  &:nth-child(even) {
    margin-left: 0;
    border-radius: 0 6px 6px 0;
  }

  .sfitem-header {
    display: block;
    position: relative;
    padding: calc(${ThemeVerticalPadding} + ${ThemeVerticalPadding}) ${ThemeHorizontalPadding};
    background: ${ThemeBackgroundLighter};
    transition: background 300ms ease-in-out;

    &:hover {
      box-shadow: 0px 0px 13px rgba(255, 255, 255, 0.05) inset;
      .sfitem-name {
        text-decoration: underline;
        cursor: pointer;
      }

      &:before {
        filter: saturate(1.1) brightness(1.1);
      }
    }

    .sfitem-name {
      padding: 0;
      margin: 0 0 ${ThemeVerticalHalfPadding} 0;
      font-family: 'Fira Sans', 'Open Sans', sans-serif;
      font-size: 15px;
    }

    .sfitem-description {
      margin: 0;
      width: 80%;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      font-size: 14px;
      height: 16px;
      margin-bottom: ${ThemeVerticalHalfPadding};
    }

    &:before {
      content: '';
      display: block;
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      height: 2px;
      background: linear-gradient(
        270deg,
        ${props => props.color[0]} 0%,
        ${props => props.color[1]} 100%
      );
    }
    &:after {
      content: '';
      display: block;
      position: absolute;
      right: ${ThemeHorizontalPadding};
      width: 65px;
      height: 65px;
      background: url(${props => props.icon});
      background-size: cover;
      background-repeat: no-repeat;
      z-index: 0;

      transition: transform 300ms ease-in-out;

      top: 50%;
      transform: translateY(-50%) scale(1);
    }
  }

  > summary {
    box-sizing: border-box;
    flex: 1;
    background: ${ThemeBackgroundDarker};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    min-height: calc(${ThemeVerticalPadding} * 7);
  }

  ${MobileMediaQuery} {
    width: calc(100% - ${ThemeHorizontalPadding} - ${ThemeHorizontalPadding});

    &:nth-child(odd),
    &:nth-child(even) {
      border-radius: 5px;
      margin: 0 auto;
      margin-bottom: ${ThemeVerticalPadding};
    }

    .sfitem-description {
      width: calc(100% - 80px);
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      height: 16px;
    }

    > summary {
      > a {
        margin: 0;
      }
    }
  }
`;
