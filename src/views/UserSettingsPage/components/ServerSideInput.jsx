/* eslint-disable jsx-a11y/label-has-for */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Tooltip from '../../../componentsNew/Tooltip';

const ServerSideInput = (props) => {
  const [value, setValue] = useState(false);

  useEffect(() => {
    const fetchValue = async () => {
      const serverSideValue = await props.getValue();
      setValue(serverSideValue);
    };
    fetchValue();
  }, []);

  const updateValue = (newValue) => {
    const setValueAsync = async () => {
      await props.setValue(newValue);
      setValue(newValue);
    };
    setValueAsync();
  };
  const { tooltip } = props;
  const { label } = props;

  return (
    <Tooltip text={tooltip}>
      <div className="input-wrapper">
        <input
          type="checkbox"
          onChange={(e) => {
            updateValue(e.target.checked);
          }}
          checked={value}
        />
        <label>{label}</label>
      </div>
    </Tooltip>
  );
};

ServerSideInput.propTypes = {
  label: PropTypes.string.isRequired,
  tooltip: PropTypes.string.isRequired,
  setValue: PropTypes.func,
  getValue: PropTypes.func,
};

ServerSideInput.defaultProps = {
  setValue: null,
  getValue: null,
};

export default ServerSideInput;
