/* eslint-disable react/no-array-index-key */

import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  ThemeBackgroundDarker,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeKnockoutRed,
} from '../../utils/ThemeNew';
import postsPerPage from '../../utils/postsPerPage';

const scrollToTop = () => {
  document.documentElement.scrollTop = 0;
};

const getUnfocusedRange = (totalPages, maxSize) => {
  const output = [];
  const center = Math.ceil(maxSize / 2);

  // count up from page 1 to the center
  for (let i = 1; i <= center; i += 1) {
    output.push(i);
  }

  // include a deliniator
  output.push(null);

  // count down from the last page to the center
  for (let i = center; i > 0; i -= 1) {
    output.push(totalPages - (i - 1));
  }

  return output;
};

const getFocusedRange = (totalPages, maxSize, currentPage) => {
  const output = [];
  const center = Math.ceil(maxSize / 2);

  // if the user is on a page near the start of the thread
  if (currentPage <= center) {
    for (let i = 1; i <= maxSize; i += 1) {
      output.push(i);
    }
    output[maxSize - 2] = null;
    output[maxSize - 1] = totalPages;
  }

  // if the user is on a page near the middle of the thread
  if (currentPage > center && currentPage <= totalPages - center) {
    for (let i = center - 1; i > 0; i -= 1) {
      output.push(currentPage - i);
    }
    for (let i = 0; i < center; i += 1) {
      output.push(parseInt(currentPage, 10) + i);
    }
    output[0] = 1;
    output[1] = null;
    output[maxSize - 2] = null;
    output[maxSize - 1] = totalPages;
  }

  // if the user is on a page near the end of the thread
  if (currentPage > totalPages - center) {
    for (let i = maxSize; i > 0; i -= 1) {
      output.push(totalPages - (i - 1));
    }
    output[0] = 1;
    output[1] = null;
  }

  return output;
};

const getFullRange = (totalPages) => {
  const output = [];

  // count up to the max Size
  for (let i = 1; i <= totalPages; i += 1) {
    output.push(i);
  }

  return output;
};

const Pagination = ({
  pagePath,
  totalPosts,
  currentPage,
  pageSize = postsPerPage,
  small = false,
  showNext = false,
  className,
}) => {
  const totalPages = Math.ceil(totalPosts / pageSize);
  const maxSize = small ? 4 : 9;
  const onLastPage = parseInt(currentPage, 10) === parseInt(totalPages, 10);
  const nextPage = parseInt(currentPage, 10) + 1;
  const smallClassName = small ? 'small' : '';

  let pages = [];
  if (totalPages > maxSize) {
    // more pages than we have space to show, so lets be a bit more relevent
    if (typeof currentPage === 'undefined') {
      // we dont have a current page set, so lets just show the extremes
      pages = getUnfocusedRange(totalPages, maxSize);
    } else {
      // we do have a current page set, so lets show around it and the extremes
      pages = getFocusedRange(totalPages, maxSize, currentPage);
    }
  } else {
    // less pages than we have space to show, so lets show everything
    pages = getFullRange(totalPages, maxSize, currentPage);
  }

  return (
    <PaginationWrapper className={className} small={small}>
      {pages.map((page, index) => {
        if (page == null) {
          return (
            <div
              className={`pagination-spacer ${smallClassName}`}
              small={small || undefined}
              key={`page-null-${index}`}
            >
              ...
            </div>
          );
        }
        if (page === parseInt(currentPage, 10)) {
          return (
            <Link
              className={`pagination-item active ${smallClassName}`}
              small={small || undefined}
              key={`page-${page}-active`}
              to={`${pagePath}${page}`}
              onClick={scrollToTop}
            >
              {page}
              <i className="fas fa-caret-up" />
            </Link>
          );
        }
        return (
          <Link
            className={`pagination-item ${smallClassName}`}
            small={small || undefined}
            key={`page-${page}`}
            to={`${pagePath}${page}`}
            onClick={scrollToTop}
          >
            {page}
          </Link>
        );
      })}
      {showNext && !onLastPage && pages[0] && (
        <Link
          className={`pagination-item ${smallClassName}`}
          small={small || undefined}
          to={`${pagePath}${nextPage}`}
          onClick={scrollToTop}
        >
          Next&nbsp;
          <i className="fas fa-angle-double-right" />
        </Link>
      )}
    </PaginationWrapper>
  );
};

export default Pagination;

Pagination.propTypes = {
  pagePath: PropTypes.string.isRequired,
  totalPosts: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  pageSize: PropTypes.number,
  small: PropTypes.bool,
  showNext: PropTypes.bool,
  className: PropTypes.string,
};

Pagination.defaultProps = {
  pageSize: postsPerPage,
  small: false,
  showNext: false,
  className: '',
};
const PaginationWrapper = styled.div`
  display: inline-block;

  a,
  .pagination-spacer {
    display: inline-block;
  }

  .pagination-item,
  .pagination-spacer {
    background: ${ThemeBackgroundDarker};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    margin: 0 calc(${ThemeHorizontalPadding} / 2);
    height: 30px;
    box-sizing: border-box;

    &:last-child {
      margin-right: 0;
    }

    &:hover {
      filter: brightness(1.2);
      text-decoration: underline;
    }

    &.small {
      box-sizing: border-box;
      padding: calc(${ThemeVerticalPadding} / 2) calc(${ThemeHorizontalPadding} / 2);
    }

    &.active {
      position: relative;
      background: ${ThemeBackgroundLighter};

      i {
        color: ${ThemeKnockoutRed};
        position: absolute;
        left: 50%;
        bottom: -5px;
        transform: translateX(-50%);
      }
    }
  }
`;
