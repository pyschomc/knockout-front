import React from 'react';
import { Redirect } from 'react-router-dom';
import { loadUserFromStorage } from '../services/user';
import SubforumlistV2 from './SubforumListV2';

const HomePage = () => {
  const user = loadUserFromStorage();

  if (user && !user.username) {
    // wait, we still need to do the user setup
    return <Redirect to="/usersetup" />;
  }

  return <SubforumlistV2 />;
};

export default HomePage;
