import { authPost } from './common';
import { pushSmartNotification } from '../utils/notification';

const submitReport = async ({ postId, reportReason }) => {
  if (!postId || !reportReason || reportReason.length < 3) {
    return { error: 'Missing required info.' };
  }

  const requestBody = {
    postId,
    reportReason,
  };

  const response = await authPost({ url: '/report', data: requestBody });

  return pushSmartNotification(response.data);
};

export default submitReport;
