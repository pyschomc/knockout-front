import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useHistory, Link } from 'react-router-dom';

import { StyledHeader, HeaderLink } from './components/style';
import UserControls from './components/UserControls';
import BannedHeaderMessage from './components/BannedHeaderMessage';
import UserGroupRestricted from '../UserGroupRestricted';
import { scrollToTop } from '../../utils/pageScroll';
import LoggedInOnly from '../LoggedInOnly';

import { updateMentions } from '../../state/mentions';
import { loadBannedMessageFromStorage } from '../../utils/bannedStorage';
import { checkLoginStatus, isLoggedIn } from '../../utils/user';
import { setSubscriptions } from '../../state/subscriptions';

const Header = () => {
  const [bannedInformation, setBannedInformation] = useState(undefined);
  const [openReports, setOpenReports] = useState(0);

  const dispatch = useDispatch();
  const unreadSubscriptions = useSelector((state) => state.subscriptions.count);

  const history = useHistory();
  const location = useLocation();

  const stickyHeader = useSelector((state) => state.style.stickyHeader);

  const updateHeader = ({ mentions, subscriptions, reports }) => {
    if (subscriptions && subscriptions[0]) {
      const subscriptionState = { threads: {}, count: 0 };
      subscriptions.forEach((thread) => {
        if (thread.unreadPosts) {
          subscriptionState.threads[thread.threadId] = thread.unreadPosts;
          subscriptionState.count += thread.unreadPosts;
        }
      });
      dispatch(setSubscriptions(subscriptionState));
    }

    if (reports) {
      setOpenReports(reports || 0);
    }

    if (mentions.length > 0) {
      dispatch(updateMentions(mentions));
    }
  };

  useEffect(() => {
    checkLoginStatus(location.pathname, (user) => updateHeader({ ...user }), history);

    if (!isLoggedIn()) {
      setBannedInformation(loadBannedMessageFromStorage());
    }
  }, []);

  return (
    <>
      {bannedInformation && (
        <BannedHeaderMessage
          banMessage={bannedInformation.banMessage}
          threadId={bannedInformation.threadId}
        />
      )}

      <StyledHeader id="knockout-header" stickyHeader={stickyHeader}>
        <div id="header-content">
          <div className="branding">
            <Link to="/" className="brand">
              <img src="static/logo_new_dark.svg" alt="knockout!" className="header-logo" />
              <div className="title">knockout!</div>
            </Link>

            <div id="nav-items">
              <Link to="/rules" className="link">
                <i className="fas fa-atlas" />
                <span>Rules</span>
              </Link>

              <a
                href="https://discord.gg/wjWpapC"
                className="link"
                target="_blank"
                rel="noopener noreferrer"
              >
                <i className="fab fa-discord" />
                <span>Discord</span>
              </a>

              <LoggedInOnly>
                <HeaderLink
                  to="/alerts/list"
                  className="link"
                  onClick={() => {
                    scrollToTop();
                  }}
                >
                  <i className="fas fa-newspaper" />
                  <span>Subscriptions</span>
                  {unreadSubscriptions > 0 && (
                    <div className="link-notification">{unreadSubscriptions}</div>
                  )}
                </HeaderLink>

                <Link to="/events" className="link" onClick={scrollToTop}>
                  <i className="fas fa-bullhorn" />
                  <span>Events</span>
                </Link>

                <UserGroupRestricted userGroupIds={[3, 4, 5]}>
                  <HeaderLink
                    to="/moderate"
                    className="link"
                    onClick={() => {
                      setOpenReports(0);
                      scrollToTop();
                    }}
                  >
                    <i className="fas fa-shield-alt" />
                    <span>Moderation</span>
                    {openReports > 0 && <div className="link-notification">{openReports}</div>}
                  </HeaderLink>
                </UserGroupRestricted>
              </LoggedInOnly>
            </div>
          </div>

          <UserControls unreadSubscriptions={unreadSubscriptions} />

          <div className="beta-tag">LABS</div>
        </div>

        <div className="backdrop-filter" />
      </StyledHeader>
    </>
  );
};

export default Header;
