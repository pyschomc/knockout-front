import styled from 'styled-components';

import { MobileMediaQuery } from '../SharedStyles';

import {
  ThemeSmallTextSize,
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeFooterTextColor
} from '../../Theme';

const FooterWrapper = styled.footer`
  position: relative;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;

  padding-top: ${ThemeVerticalPadding};
  padding-bottom: ${ThemeVerticalPadding};
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  margin-top: 80px;
  font-size: ${ThemeSmallTextSize};
  box-sizing: border-box;

  .linkContainer {
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    justify-content: space-evenly;
  }

  .linkContainer a,
  .linkContainer span {
    display: inline-block;
    color: ${ThemeFooterTextColor};
    text-decoration: none;
    opacity: 0.75;
    transition: opacity 100ms ease-in-out;
    margin-left: 30px;
    cursor: pointer;
    &:hover {
      opacity: 1;
    }
  }

  i {
    padding-right: 3px;
  }

  ${MobileMediaQuery} {
    margin-top: 0px;
    padding-bottom: 15px;
    flex-direction: column-reverse;
    justify-content: space-evenly;

    .linkContainer {
      width: 100%;
      margin-bottom: 15px;
      justify-content: center;

      a,
      span {
        margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      }
    }

    a,
    span {
      margin: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
    }
  }
`;

export default FooterWrapper;
