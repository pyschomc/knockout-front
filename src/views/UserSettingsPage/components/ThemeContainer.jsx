/* eslint-disable jsx-a11y/label-has-for */ // not working properly
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';

import { pushNotification } from '../../../utils/notification';

import {
  deleteCustomThemeFromStorage,
  setCustomThemeToStorage,
  loadCustomThemeFromStorage,
} from '../../../services/theme';

import ThemePreview from './ThemePreview';

let customTheme = {
  backgroundLighter: '#1f2c39',
  backgroundDarker: '#161d24',
  mainBackgroundColor: '#0b0e11',
  bodyBackgroundColor: '#0d1013',
  knockoutBrandColor: '#ec3737',
  knockoutLogo: '/static/logo_dark.png',
  textColor: '#ffffff',
  themeFontSizeSmall: '11px',
  themeFontSizeMedium: '14px',
  themeFontSizeLarge: '16px',
  themeFontSizeHuge: '24px',
  themeGoldMemberColor: 'linear-gradient(375deg, #fcbe20, #fcbe20, #ffe770, #fcbe20, #fcbe20)',
  themeGoldMemberGlow: 'drop-shadow(0px 0px 2px #ffcc4a)',
  themeModeratorColor: '#41ff74',
  themeBannedUserColor: '#ff0000',
  postLineHeight: 160,
};

try {
  const storageTheme = loadCustomThemeFromStorage();

  if (storageTheme) {
    customTheme = {
      ...customTheme,
      ...JSON.parse(storageTheme),
    };
  }
} catch (error) {
  console.log('Could not load custom theme from local storage. ', error);
}

const ThemeInput = ({ name, value, onChange, type, label }) => (
  <div className="input-row">
    <label htmlFor={name}>{`${label}: `}</label>
    <div className="input-wrap">
      <input type={type} id={name} name={name} value={value} onChange={onChange} />
      {type === 'color' && (
        <input type="text" id={name} name={name} value={value} onChange={onChange} />
      )}
    </div>
  </div>
);

ThemeInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

const ThemeConfiguratorContainer = () => {
  const [themeValues, setThemeValues] = useState(customTheme);
  const [rerenderPreviewValues, setRerenderPreviewValues] = useState(customTheme);
  const changeTimer = useRef(0);

  const saveTheme = () => {
    setCustomThemeToStorage(JSON.stringify(themeValues));
    pushNotification({ message: 'Theme saved!', type: 'success' });
  };

  const resetTheme = () => {
    deleteCustomThemeFromStorage();
    pushNotification({ message: 'Theme reset to default.', type: 'success' });
  };

  const handleChange = ({ target: { name, value } }) => {
    const newValues = {
      ...themeValues,
      [name]: value,
    };

    setThemeValues(newValues);
    clearTimeout(changeTimer.current);

    changeTimer.current = setTimeout(() => {
      setRerenderPreviewValues(newValues);
    }, 400);
  };

  const copyThemeFromClipboard = () => {
    // Check if the browser supports clipboard reading and if we have permission to do so
    navigator.permissions
      .query({ name: 'clipboard-read' })
      .then((result) => {
        // Permissions denied
        if (result.state === 'denied') {
          pushNotification({
            message: 'Unable to read theme. Clipboard permissions denied by browser.',
            type: 'error',
          });
        }

        // Permissions granted or we haven't asked yet
        else if (result.state === 'granted' || result.state === 'prompt') {
          navigator.clipboard
            .readText()
            .then((theme) => JSON.parse(theme))
            .then((theme) => {
              setThemeValues(theme);
              pushNotification({ message: 'Theme applied', type: 'success' });
            })
            .catch(() => {
              pushNotification({
                message: 'Unable to read theme. Please make sure it is valid JSON.',
                type: 'error',
              });
            });
        }
      })
      .catch(() => {
        // Browsers like firefox do not implement clipboard reading publicly, causing an error when asking for it.
        // https://developer.mozilla.org/en-US/docs/Web/API/Clipboard/read
        // This should be later replaced with a fallback input method for theme JSON.
        pushNotification({
          message: 'Unable to read theme. Browser does not support clipboard reading.',
          type: 'error',
        });
      });
  };

  const copyThemeToClipboard = () => {
    navigator.clipboard
      .writeText(JSON.stringify(themeValues))
      .then(() => {
        pushNotification({ message: 'Theme copied to clipboard', type: 'success' });
      })
      .catch(() => {
        pushNotification({ message: 'Unable to copy theme', type: 'error' });
      });
  };

  const {
    backgroundLighter,
    backgroundDarker,
    mainBackgroundColor,
    bodyBackgroundColor,
    knockoutBrandColor,
    // knockoutLogo,
    textColor,
    themeFontSizeSmall,
    themeFontSizeMedium,
    themeFontSizeLarge,
    themeFontSizeHuge,
    themeGoldMemberColor,
    themeGoldMemberGlow,
    themeModeratorColor,
    themeBannedUserColor,
    postLineHeight,
  } = themeValues;

  return (
    <div className="panel">
      <h2 title="Richboi Zone">Theme Configurator</h2>
      <div className="theme-configuration">
        <div className="theme-inputs">
          <ThemeInput
            type="color"
            name="backgroundLighter"
            value={backgroundLighter}
            onChange={handleChange}
            label="Lighter Background Color"
          />
          <ThemeInput
            type="color"
            name="backgroundDarker"
            value={backgroundDarker}
            onChange={handleChange}
            label="Darker Background Color"
          />
          <ThemeInput
            type="color"
            name="mainBackgroundColor"
            value={mainBackgroundColor}
            onChange={handleChange}
            label="Page Background Color"
          />
          <ThemeInput
            type="color"
            name="bodyBackgroundColor"
            value={bodyBackgroundColor}
            onChange={handleChange}
            label="Page Content Background Color"
          />
          <ThemeInput
            type="color"
            name="textColor"
            value={textColor}
            onChange={handleChange}
            label="Text Color"
          />
          <ThemeInput
            type="color"
            name="knockoutBrandColor"
            value={knockoutBrandColor}
            onChange={handleChange}
            label="Knockout Brand Color"
          />
          <ThemeInput
            type="text"
            name="themeGoldMemberColor"
            value={themeGoldMemberColor}
            onChange={handleChange}
            label="Gold Member Color"
          />
          <ThemeInput
            type="text"
            name="themeGoldMemberGlow"
            value={themeGoldMemberGlow}
            onChange={handleChange}
            label="Gold Member Glow"
          />
          <ThemeInput
            type="color"
            name="themeModeratorColor"
            value={themeModeratorColor}
            onChange={handleChange}
            label="Moderator Color"
          />
          <ThemeInput
            type="color"
            name="themeBannedUserColor"
            value={themeBannedUserColor}
            onChange={handleChange}
            label="Banned User Color"
          />
          <ThemeInput
            type="text"
            name="themeFontSizeSmall"
            value={themeFontSizeSmall}
            onChange={handleChange}
            label="Font Size Small"
          />
          <ThemeInput
            type="text"
            name="themeFontSizeMedium"
            value={themeFontSizeMedium}
            onChange={handleChange}
            label="Font Size Medium"
          />
          <ThemeInput
            type="text"
            name="themeFontSizeLarge"
            value={themeFontSizeLarge}
            onChange={handleChange}
            label="Font Size Large"
          />
          <ThemeInput
            type="text"
            name="themeFontSizeHuge"
            value={themeFontSizeHuge}
            onChange={handleChange}
            label="Font Size Huge"
          />
          <ThemeInput
            type="number"
            name="postLineHeight"
            value={postLineHeight}
            onChange={handleChange}
            label="Post content line height (%)"
          />
        </div>
        <div className="theme-preview">
          <ThemePreview theme={rerenderPreviewValues} />
        </div>
        <div className="theme-actions">
          <button type="button" onClick={saveTheme}>
            <i className="fas fa-save" />
            Save
          </button>
          <button type="button" onClick={resetTheme}>
            <i className="fas fa-redo" />
            Reset
          </button>
          <button type="button" onClick={copyThemeFromClipboard}>
            <i className="fas fa-file-import" />
            Copy theme from clipboard
          </button>
          <button type="button" onClick={copyThemeToClipboard}>
            <i className="fas fa-file-export" />
            Copy theme to clipboard
          </button>
          <p>Reload on save to see the changes</p>
        </div>
      </div>
    </div>
  );
};

export default ThemeConfiguratorContainer;
