/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { scrollToTop } from '../utils/pageScroll';

class ScrollOnRouteChange extends React.Component {
  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      scrollToTop();
    }
  }

  render() {
    return this.props.children;
  }
}
ScrollOnRouteChange.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  children: PropTypes.any.isRequired
};

export default withRouter(ScrollOnRouteChange);
