/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import {
  StyledModalWrapper,
  StyledModalTitle,
  StyledSectionDelimiter,
  StyledModalSelect,
  StyledModalIcon,
  StyledModalContent
} from './style';
import { SubHeaderRealButton } from '../SubHeader/style';
import { AbsoluteBlackout } from '../SharedStyles';

class SelectModal extends React.Component {
  state = { selectedOption: null };

  handleChange = e => this.setState({ selectedOption: e.target.value });

  render() {
    const {
      title,
      options,
      cancelText = 'Cancel',
      submitText = 'Submit',
      iconUrl,
      submitFn,
      cancelFn
    } = this.props;

    return (
      <>
        <StyledModalWrapper>
          <StyledSectionDelimiter>
            {iconUrl && <StyledModalIcon src={iconUrl} alt="Background icon" />}
            <StyledModalTitle>{title}</StyledModalTitle>
          </StyledSectionDelimiter>

          <StyledModalContent>
            <StyledModalSelect name="" id="" onChange={this.handleChange}>
              <option>Choose a subforum...</option>
              {options.map(option => (
                <option value={option.value}>{option.text || option.value}</option>
              ))}
            </StyledModalSelect>
          </StyledModalContent>

          <StyledSectionDelimiter>
            <SubHeaderRealButton type="button" onClick={cancelFn}>
              {cancelText}
            </SubHeaderRealButton>

            <SubHeaderRealButton type="button" onClick={() => submitFn(this.state.selectedOption)}>
              {submitText}
            </SubHeaderRealButton>
          </StyledSectionDelimiter>
        </StyledModalWrapper>

        <AbsoluteBlackout />
      </>
    );
  }
}

export default SelectModal;

SelectModal.propTypes = {
  title: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  cancelText: PropTypes.string,
  submitText: PropTypes.string,
  submitFn: PropTypes.func.isRequired,
  cancelFn: PropTypes.func.isRequired,
  iconUrl: PropTypes.string
};
SelectModal.defaultProps = {
  cancelText: 'Cancel',
  submitText: 'Submit',
  iconUrl: undefined
};
