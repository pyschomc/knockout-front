import React from 'react';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import NavbarResponsive from './NavbarResponsive';
import { navItems } from './NavItems';
import { PageWrapper } from './style';

const ModerateSubforums = () => (
  <UserGroupRestricted userGroupIds={[3, 4, 5]}>
    <PageWrapper>
      <NavbarResponsive items={navItems} />
    </PageWrapper>
  </UserGroupRestricted>
);

export default ModerateSubforums;
