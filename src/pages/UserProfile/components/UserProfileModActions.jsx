import React from 'react';
import PropTypes from 'prop-types';

import { removeUserImage, addGoldProduct } from '../../../services/moderation';
import { pushSmartNotification } from '../../../utils/notification';
import { BasicInfoWrapper, BigInfo } from './modActionStyles';
import UserGroupRestricted from '../../../components/UserGroupRestricted';

const removeImage = async ({ avatar, background, userId }) => {
  if (!avatar && !background) {
    return;
  }
  try {
    const imageRemovalResponse = await removeUserImage({ avatar, background, userId });

    pushSmartNotification(imageRemovalResponse);
  } catch (error) {
    console.error(error);
  }
};

const UserProfileModActions = ({ id }) => (
  <>
    <BasicInfoWrapper>
      <BigInfo>Actions</BigInfo>

      <button type="button" onClick={() => removeImage({ avatar: true, userId: id })}>
        Remove avatar
      </button>
      <button type="button" onClick={() => removeImage({ background: true, userId: id })}>
        Remove background
      </button>

      <UserGroupRestricted userGroupIds={[4]}>
        <button type="button" onClick={() => addGoldProduct({ userId: id })}>
          Give gold for a month
        </button>
      </UserGroupRestricted>
    </BasicInfoWrapper>
  </>
);

UserProfileModActions.propTypes = {
  id: PropTypes.number.isRequired
};

export default UserProfileModActions;
