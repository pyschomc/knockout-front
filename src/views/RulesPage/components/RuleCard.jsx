import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { transparentize } from 'polished';
import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
  ThemeFontSizeMedium,
  ThemeFontSizeLarge,
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeHighlightWeaker,
} from '../../../utils/ThemeNew';

const RuleCard = ({ title, children, identifier, selected, interactable }) => {
  const path = `${window.location.origin}${useLocation().pathname}`;
  const cardRef = useRef(null);

  const onClick = () => {
    navigator.clipboard.writeText(`${path}?id=${identifier}`);
  };

  useEffect(() => {
    if (selected && interactable) {
      cardRef.current.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'nearest',
      });
    }
  });

  return (
    <StyledRuleCard
      id={identifier}
      className={`rule-card ${selected && interactable ? 'selected' : ''}`}
      onClick={interactable ? onClick : () => {}}
      ref={cardRef}
      tabIndex="-1"
      label="ruleCard"
    >
      <h3>{title}</h3>
      <p>{children}</p>
      {interactable && (
        <p className="copied-text" aria-hidden>
          Copied To Clipboard
        </p>
      )}
    </StyledRuleCard>
  );
};

RuleCard.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  identifier: PropTypes.string.isRequired,
  selected: PropTypes.bool,
  interactable: PropTypes.bool,
};

RuleCard.defaultProps = {
  selected: false,
  interactable: true,
};

const numberBoxSize = '35px';

const StyledRuleCard = styled.li`
  @keyframes copiedTextPop {
    0% {
      opacity: 0;
    }
    30% {
      opacity: 1;
    }
    80% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }
  &.rule-card {
    display: list-item;
    position: relative;
    z-index: 3;
    box-sizing: border-box;
    width: 49%;
    max-width: 49%;
    padding: calc(${ThemeHorizontalPadding} * 2);
    padding-left: calc(${numberBoxSize} + calc(${ThemeHorizontalPadding} * 2));
    margin-bottom: calc(${ThemeVerticalPadding} * 2);
    background-color: ${ThemeBackgroundDarker};
    counter-increment: counter;
    cursor: pointer;
    transition: filter 200ms ease-in-out, transform 50ms;
    overflow: hidden;
    .copied-text {
      box-sizing: border-box;
      position: absolute;
      z-index: 1;
      bottom: 0;
      left: 0;
      opacity: 0;
      padding: 10px;
      background-color: ${ThemeBackgroundDarker};
      font-size: ${ThemeFontSizeSmall};
    }
    &:before {
      content: counter(counter);
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      width: ${numberBoxSize};
      height: ${numberBoxSize};
      line-height: ${numberBoxSize};
      text-align: center;
      align-self: center;
      font-size: ${ThemeFontSizeLarge};
      background-color: ${transparentize(0.3, ThemeHighlightWeaker)};
    }
    &.selected {
      background-color: ${ThemeBackgroundLighter};
      filter: drop-shadow(0px 0px 3px ${ThemeHighlightWeaker});
    }
    h3 {
      letter-spacing: 0.1em;
      margin-top: 0;
      padding: 0;
      position: relative;
      display: inline-block;
      text-transform: capitalize;
      font-size: ${ThemeFontSizeLarge};
      &:after {
        content: ' ';
        position: absolute;
        bottom: -10px;
        left: 0;
        right: 50%;
        height: 2px;
        background-color: ${ThemeHighlightWeaker};
        transition: right 200ms ease-in-out;
      }
    }
    p {
      font-size: ${ThemeFontSizeMedium};
      margin-bottom: 0;
      line-height: 1.5em;
    }
    a {
      color: ${ThemeHighlightWeaker};
      font-weight: bold;
      &:hover {
        filter: brightness(150%);
      }
    }
    &:focus {
      filter: drop-shadow(0px 0px 3px ${ThemeHighlightWeaker});
      outline: none;
      .copied-text {
        z-index: 1;
        animation-name: copiedTextPop;
        animation-duration: 2s;
      }
    }
    &:active {
      transform: scale(0.98);
    }
    &:hover {
      h3::after {
        right: 0;
      }
    }
  }
  @media (max-width: 700px) {
    &.rule-card {
      width: 100%;
      max-width: 100%;
    }
  }
`;

export default RuleCard;
