import React from 'react';
import UserGroupRestricted from '../../../components/UserGroupRestricted';
import NavbarResponsive from './NavbarResponsive';
import { Panels } from './UserLookup/style';
import UserLookupPanel from './UserLookup/UserLookupPanel';
import { navItems } from './NavItems';
import { PageWrapper } from './style';

const ModerateUserLookup = () => (
  <UserGroupRestricted userGroupIds={[3, 4, 5]}>
    <PageWrapper>
      <NavbarResponsive items={navItems} />
      <Panels>
        <UserLookupPanel />
      </Panels>
    </PageWrapper>
  </UserGroupRestricted>
);

export default ModerateUserLookup;
